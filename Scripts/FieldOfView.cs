﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour
{
    /// <summary>  
    ///  Support for buyer spotting distractions
    ///  Author: Oluwabiyi Akinsara
    ///  Contributor: 
    /// </summary>  

    public enum eTargeting
    { 
        SINGLE,
        MULTIPLE
    }

    public float viewRadius;
    [Range(0, 360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    public eTargeting targetingType;

    [HideInInspector]
    public Transform target; 

    public List<Transform> targets; 

    void Start()
    {
        targets = new List<Transform>();
        target = null;
    }

    void Update()
    {
        switch (targetingType)
        {
            case eTargeting.SINGLE:
                FindVisibleTarget();
                break;
            case eTargeting.MULTIPLE:
                FindVisibleTargets();
                break;
            default:
                break;
        }
    }

    void FindVisibleTarget()
    {
        target = null;
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform tempTarget = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (tempTarget.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, tempTarget.position);

                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    target = tempTarget;
                    break;
                }
            }
        }
    }

    void FindVisibleTargets()
    {
        targets.Clear(); ;
        Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            Transform tempTarget = targetsInViewRadius[i].transform;
            Vector3 dirToTarget = (tempTarget.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, tempTarget.position);

                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask))
                {
                    targets.Add(tempTarget);
                }
            }
        }
    }


    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    } 
}
