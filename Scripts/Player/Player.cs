﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script is responsible for serving as the central hub for all of the player's input checking and passing
 * that input off to AvariceCC's Move method.
 * 
 * Additionally, the Player script is also responsible for managing the player's abilities as follows:
 *  - This script maintains a list of every ability the player has innately as well as a reference to the current capture ability (if one exists)
 *  - When the script initializes, the ability list is sorted with respect to each ability's priority number in descending order.
 *  - In Update, the list of abilities as well as the current capture ability are updated, and if any are activated, that ability becomes the "current active ability"
 *  - The "current active ability" then becomes the only ability that is updated until it's finished, allowing for the inputs of other abilities to follow.
 *  
 * Finally, this script contains special case methods for resetting the state of every ability or even the player's animator for the few cases that might require breaking away
 * from the previously mentioned priority system.
 */

[RequireComponent(typeof(Damageable))]
[RequireComponent(typeof(AnimationListener))]
public class Player : AvariceEntity
{
    [Header("Player Properties")]

    [Tooltip("Make this value the same as the one for fade duration on the death screen")]
    public float respawnDelay = 2.0f;

    [Range(0.0f, 2.0f), Tooltip("The deceleration by which the player's velocity lowers towards gravity when falling")]
    public float fallDeceleration = 0.2f;

    [HideInInspector] public Captureable.CaptureAbilityStats currentCapture = null;
    public List<AbilityBase> abilities = new List<AbilityBase>();

    [HideInInspector] public Animator animator;
    [HideInInspector] public Animator bagAnimator;
    [HideInInspector] public Damageable playerHealth;
    [HideInInspector] public AnimationListener animationListener;
    [HideInInspector] public ParticleSystem propelCooldownEffect = null;
    protected TrailRenderer[] scarabStrikeTrails = null;
    protected PlayerCapture playerCapture;
    protected Damager playerDamager;

    private bool isCaptureAbilityActive = false;
    private AbilityBase currentActiveAbility = null;

    private string lastActivatedAbility = string.Empty;
    public string LastActivatedAbility
    {
        get
        {
            return lastActivatedAbility;
        }
    }

    private bool bDied = false;
    private float respawnTimer = 0.0f;

    public void SetScarabStrikeTrailsActive(bool isActive)
    {
        foreach (var trail in scarabStrikeTrails)
        {
            trail.enabled = isActive;
        }
    }

    protected void Awake()
    {
        animationListener = GetComponent<AnimationListener>();
        playerCapture = GetComponentInChildren<PlayerCapture>();
        playerDamager = GetComponentInChildren<Damager>();
        propelCooldownEffect = GetComponentInChildren<ParticleSystem>();

        scarabStrikeTrails = GetComponentsInChildren<TrailRenderer>();
        SetScarabStrikeTrailsActive(false);
    }

    protected override void Start()
    {
        base.Start();
        playerHealth = GetComponent<Damageable>();

        Animator[] animators = GetComponentsInChildren<Animator>();
        Debug.Assert(animators.Length == 2, "Error: The player prefab is expected to have two animators; one for Avary, the other for the bag");
        animator = animators[0];
        bagAnimator = animators[1];

        // Sort all abilities in descending order so that those with higher priority all always updated first
        abilities.Sort();
        abilities.Reverse();

        foreach (AbilityBase ability in abilities)
        {
            if (ability.isEnabled)
            {
                ability.Initialize(gameObject);
            }
        }

        if (currentCapture != null && currentCapture.captureAbility != null) { currentCapture.captureAbility.Initialize(gameObject); }
    }

    protected override void Update()
    {
        if (GameController.GameIsPaused) { return; }
        base.Update();

        if (playerHealth.health <= 0)
        {
            if (!bDied)
            {
                velocity.x = 0;
                animator.SetTrigger("Death");
                bagAnimator.SetTrigger("Death");
                bDied = true;
                GameController.Instance.Death();
            }
            else if (bDied)
            {
                StartCoroutine(RespawnWithDelay(respawnDelay));
            }
            return;
        }

        moveAxis = (isMovementFrozen) ? Vector2.zero : new Vector2(InputHandler.GetAxisRaw("Horizontal"), InputHandler.GetAxisRaw("Vertical"));
        if (moveAxis.x > 0.0f)
        {
            moveAxis.x = 1.0f;
        }
        else if (moveAxis.x < 0.0f)
        {
            moveAxis.x = -1.0f;
        }
        else
        {
            moveAxis.x = 0.0f;
        }

        if (moveAxis.y > 0.0f)
        {
            moveAxis.y = 1.0f;
        }
        else if (moveAxis.y < 0.0f)
        {
            moveAxis.y = -1.0f;
        }
        else
        {
            moveAxis.y = 0.0f;
        }

        targetVelocityX = moveAxis.x * moveSpeed;

        if (!controller.IsGrounded && !isGravityFrozen)
        {
            velocity.y += gravity * Time.deltaTime;
        }
        else if (controller.IsGrounded && !controller.IsDescendingSlope && currentActiveAbility == null)
        {
            // Essentially, force the y-velocity to be a small negative value when grounded.
            // This keeps y-velocity in line when falling from platforms, for example.
            //velocity.y = -0.001f;
            velocity.y = -5.0f;
        }

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.IsGrounded) ? accelerationTimeGround : accelerationTimeAir);

        isCaptureAbilityActive = false;
        if (currentActiveAbility != null)
        {
            currentActiveAbility.OnUpdate();
            if (currentActiveAbility != null && !currentActiveAbility.IsActivated)
            {
                currentActiveAbility = null;
            }
        }
        else if (currentCapture != null && currentCapture.captureAbility != null)
        {
            if (!currentCapture.captureAbility.IsActivated)
            {
                if (currentCapture.uses > 0)
                {
                    lastActivatedAbility = currentCapture.captureAbility.abilityName;
                    currentCapture.captureAbility.OnUpdate();
                    isCaptureAbilityActive = currentCapture.captureAbility.IsActivated;

                    if (isCaptureAbilityActive)
                    {
                        --currentCapture.uses;
                    }
                }
                else
                {
                    currentCapture = null;
                }
            }
            else
            {
                currentCapture.captureAbility.OnUpdate();
                isCaptureAbilityActive = currentCapture.captureAbility.IsActivated;
            }

        }

        if (!isCaptureAbilityActive && currentActiveAbility == null && !playerCapture.IsCaptureAnimActive)
        {
            foreach (AbilityBase ability in abilities)
            {
                if (ability.isEnabled)
                {
                    ability.OnUpdate();
                    if (ability.IsActivated)
                    {
                        lastActivatedAbility = ability.abilityName;

                        if (ability != currentActiveAbility && ability.priority > 1)
                        {
                            currentActiveAbility = ability;
                            break;
                        }
                    }
                }
            }
        }

        if (controller.IsCeilingHit)
        {
            velocity.y = 0.0f;
        }

        animator.SetBool("IsGrounded", controller.IsGrounded);
        animator.SetFloat("Speed", Mathf.Abs(velocity.x));

        bagAnimator.SetBool("IsGrounded", controller.IsGrounded);
        bagAnimator.SetFloat("Speed", Mathf.Abs(velocity.x));
    }

    protected override void FixedUpdate()
    {
        if (GameController.GameIsPaused) { return; }
        base.FixedUpdate();

        if (currentCapture != null && currentCapture.captureAbility != null) { currentCapture.captureAbility.OnFixedUpdate(); }

        foreach (AbilityBase ability in abilities)
        {
            if (ability.isEnabled)
            {
                ability.OnFixedUpdate();
            }
        }

        controller.Move(velocity * Time.deltaTime);
    }

    public void ResetPendingAnimationTriggers()
    {
        if (animator != null)
        {
            foreach (AnimatorControllerParameter animationParam in animator.parameters)
            {
                if (animationParam.type == AnimatorControllerParameterType.Trigger)
                {
                    animator.ResetTrigger(animationParam.nameHash);
                }
            }
        }

        if (bagAnimator != null)
        {
            foreach (AnimatorControllerParameter animationParam in bagAnimator.parameters)
            {
                if (animationParam.type == AnimatorControllerParameterType.Trigger)
                {
                    bagAnimator.ResetTrigger(animationParam.nameHash);
                }
            }
        }
    }

    public void ResetAnimatorAndAbilities(AbilityBase exceptFor = null)
    {
        ResetAbilities(exceptFor);

        animator.Rebind();
        bagAnimator.Rebind();
        blendshapeReference?.ResetAll();
        playerDamager?.ResetDamagerState();

        isMovementFrozen = false;
        currentActiveAbility = null;

        playerCapture?.Reset();
    }

    public void ResetAbilities(AbilityBase exceptFor = null)
    {
        foreach (AbilityBase ability in abilities)
        {
            if (exceptFor != null && ability.name == exceptFor.name)
            {
                continue;
            }
            else
            {
                ability.Reset();
            }
        }

        if (currentCapture?.captureAbility)
        {
            currentCapture.captureAbility.Reset();
        }
    }

    public void InterruptAbilities(List<AbilityBase> abilities)
    {
        foreach (AbilityBase ability in abilities)
        {
            ability.Reset();
        }
    }

    // TODO Determine what needs to be done when respawning
    private void Respawn()
    {
        bDied = false;
        ResetAnimatorAndAbilities();
        velocity.y = -5;
        playerHealth.health = playerHealth.maxHealth;
        transform.position = PlayerManager.Instance.spawnPoint;
        lastActivatedAbility = string.Empty;
    }

    IEnumerator RespawnWithDelay(float delay)
    {
        InputHandler.bInputFrozen = true;

        CameraController.Instance?.ResetCameraPosition();
        yield return new WaitForSeconds(delay);
        Respawn();

        yield return new WaitForSeconds(delay);
        GameController.Instance.Respawn();
        InputHandler.bInputFrozen = false;
    }
}
