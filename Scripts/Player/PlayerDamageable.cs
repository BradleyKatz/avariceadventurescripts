﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script is a simple override of the damageable script used exclusively by the player. By resetting the player's animator and ability state, this prevents the player from getting stuck
 * in an unwanted animation loop.
 */

public class PlayerDamageable : Damageable
{
    protected Player player = null;
    protected float hitReactionBlend = 0.5f;

    protected override void Start()
    {
        base.Start();
        player = GetComponent<Player>();
    }

    public override void OnDamageTaken(int damage)
    {
        base.OnDamageTaken(damage);

        if (player != null)
        {
            player.ResetAnimatorAndAbilities();
            player.animator?.SetFloat("HitReactionBlend", hitReactionBlend);
            StartCoroutine(ResetHitReactionBlend());
        }
    }

    protected IEnumerator ResetHitReactionBlend()
    {
        yield return new WaitForSeconds(0.4f);
        player.animator?.SetFloat("HitReactionBlend", 0.0f);
    }
}
