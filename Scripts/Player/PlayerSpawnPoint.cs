﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Sets the spawn point of the player after the scene has finished loading
/// </summary>
public class PlayerSpawnPoint : MonoBehaviour
{
    public string levelTheme;

    private void OnEnable()
    {
        //SceneManager.activeSceneChanged += OnLevelFinishedLoading;
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //SceneManager.activeSceneChanged -= OnLevelFinishedLoading;
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        //AudioManager.Instance.StopSound(levelTheme);
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (PlayerManager.Instance.GetPlayer() == null && SceneManager.GetActiveScene().buildIndex > -1)
        {
            PlayerManager.Instance.spawnPoint = transform.position;
        }
        else if (PlayerManager.Instance.GetPlayer() != null)
        {
            if (PlayerManager.Instance.prevScene == SceneLoader.Instance.getCurrentLoadedScene())
            {
                PlayerManager.Instance.spawnPoint = transform.position;
            }
            PlayerManager.Instance.GetPlayer().transform.position = PlayerManager.Instance.spawnPoint;
        }

        PlayerManager.Instance.bSpawn = true;
        InputHandler.bInputFrozen = false;
        //CameraController.Instance?.ResetCameraPosition();

        AudioManager.Instance.SetLevelTheme(levelTheme);
        AudioManager.Instance.PlaySong(levelTheme);
    }
}
