﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: William Barry
 * Modified by:
 */

public interface IAnimationCompleted
{
    void AnimationCompleted(int shortHashName);
}
