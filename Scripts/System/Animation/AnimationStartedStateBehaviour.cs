﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This state behaviour searches for an AnimationStarted callback on the attached gameobject and invokes it if found.
 */

public class AnimationStartedStateBehaviour : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        IAnimationStarted callback = animator.gameObject.GetComponentInParent(typeof(IAnimationStarted)) as IAnimationStarted;
        if (callback != null)
        {
            callback.AnimationStarted(stateInfo.shortNameHash);
        }
    }
}
