﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script is used to model the size of the collision window for an attack animation, providing float sliders for when to start and end the collision window.
 * Additionally, this also contains properties for how the attack attached to this animation should affect the player's velocity and gravity during the animation.
 */

public class AttackAnimCollisionWindowStateBehaviour : StateMachineBehaviour
{
    public CombatAbilityBase combatAbility = null;

    [Header("Attack Momentum Properties")]
    [Tooltip("The velocity, if any, to by which to propel the character during this attack animation.")]
    public Vector2 attackPushVelocity = Vector2.zero;
    [Tooltip("Whether or not to freeze the entity's movement at the start of the animation")]
    public bool freezeMovement = true;
    [Tooltip("Whether or not to unfreeze the entity's movement at the end of the animation (useful for attack combos)")]
    public bool unfreezeMovementAtEnd = true;
    [Tooltip("Whether or not to freeze gravity when this attack animation starts")]
    public bool freezeGravity = false;
    [Tooltip("Whether or not to unfreeze the entity's gravity at the end of this animation")]
    public bool unfreezeGravityAtEnd = false;

    [Header("Collision Window Properties")]
    [Range(0, 1), Tooltip("The percentage of the way through the animation where collision should start being considered")]
    public float collisionWindowStart;
    [Range(0, 1), Tooltip("The percentage of the way through the animation where collision should stop being considered")]
    public float collisionWindowEnd;

    protected AvariceEntity entity = null;
    protected Damager entityDamager = null;
    protected AnimationListener animationListener = null;

    private void OnAttackAnimStart(int shortHashName)
    {
        if (freezeMovement)
        {
            entity.isMovementFrozen = true;
        }

        if (freezeGravity)
        {
            entity.isGravityFrozen = true;
        }

        if (attackPushVelocity.x != 0.0f)
        {
            entity.velocity.x = entity.GetHorizontalFacingDirection() * attackPushVelocity.x;
        }
    }

    private void OnAttackAnimEnd(int shortHashName)
    {
        if (unfreezeMovementAtEnd)
        {
            entity.isMovementFrozen = false;
        }

        if (unfreezeGravityAtEnd)
        {
            entity.isGravityFrozen = false;
        }

        if (entityDamager != null && animationListener != null)
        {
            entityDamager.ResetDamagerState();
            animationListener.RemoveAnimationCompletedListener(shortHashName, OnAttackAnimEnd);
        }
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        entity = animator.gameObject.GetComponentInParent<AvariceEntity>();
        entityDamager = animator.gameObject.transform.root.GetComponentInChildren<Damager>();
        
        animationListener = animator.gameObject.GetComponentInParent<AnimationListener>();
        if (entityDamager != null && animationListener != null)
        {
            OnAttackAnimStart(stateInfo.shortNameHash);
            animationListener.AddAnimationCompletedListener(stateInfo.shortNameHash, OnAttackAnimEnd);

            if (combatAbility != null && entity != null)
            {
                entityDamager.SetAttackCollider(combatAbility.abilityName);

                entityDamager.knockbackStrength = combatAbility.knockbackStrength;
                entityDamager.knockbackDirection = new Vector2(combatAbility.knockbackDirection.x, combatAbility.knockbackDirection.y);

                entityDamager.shakeDuration = combatAbility.shakeDuration;
                entityDamager.shakeAmplitude = combatAbility.shakeAmplitude;
                entityDamager.shakeFrequency = combatAbility.shakeFrequency;
            }
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (entityDamager != null)
        {
            float currentAnimPercent = System.Convert.ToSingle(stateInfo.normalizedTime - System.Math.Truncate(stateInfo.normalizedTime));
            if (currentAnimPercent >= collisionWindowStart && currentAnimPercent <= collisionWindowEnd)
            {
                entityDamager.isDamageCollisionEnabled = true;
            }
            else
            {
                entityDamager.isDamageCollisionEnabled = false;
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (entityDamager != null)
        {
            entityDamager.isDamageCollisionEnabled = false;
            entityDamager.shakeDuration = 0.0f;
            entityDamager.shakeAmplitude = 0.0f;
            entityDamager.shakeFrequency = 0.0f;
        }
    }
}
