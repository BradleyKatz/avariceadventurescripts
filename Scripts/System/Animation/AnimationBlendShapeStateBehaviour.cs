﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBlendShapeStateBehaviour : StateMachineBehaviour
{
    [Tooltip("The names of the blend shapes to influence as the animation progresses")]
    public string[] blendShapeNames;
    [Range(0.0f, 100.0f), Tooltip("The speed at which the blendshape takes over")]
    public float blendSpeed = 2.5f;
    [Tooltip("Temporary position value to assign the entity while blend shapes are active")]
    public Vector3 entityTransformInfluence = Vector3.zero;
    [Tooltip("Temporary rotation value to assign the entity while blend shapes are active")]
    public Vector3 entityRotationInfluence = Vector3.zero;
    [Tooltip("Whether or not the blendshapes should be reset when this animation state exits")]
    public bool resetBlendShapesOnExit = false;

    [Header("Reverse Properties")]
    [Tooltip("Whether or not the blend shapes should start reversing at a specified point of the animation")]
    public bool shouldReverseMidway = false;
    [Range(0.0f, 1.0f), Tooltip("The percentage of the way through the animation that the blend shapes should start reversing, if applicable")]
    public float reverseAnimPercent = 0.0f;
    [Range(0.0f, 100.0f), Tooltip("The speed at which the blendshape reverses")]
    public float reverseBlendSpeed = 2.5f;

    private BlendShapeReference blendshapeReference = null;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        blendshapeReference = animator.gameObject.GetComponent<BlendShapeReference>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (blendshapeReference)
        {
            if (entityTransformInfluence != Vector3.zero)
            {
                animator.gameObject.transform.localPosition = entityTransformInfluence;
            }
            
            if (entityRotationInfluence != Vector3.zero)
            {
                animator.gameObject.transform.localRotation = Quaternion.Euler(entityRotationInfluence);
            }

            float currentAnimPercent = System.Convert.ToSingle(stateInfo.normalizedTime - System.Math.Truncate(stateInfo.normalizedTime));
            foreach (Tuple<int, string, SkinnedMeshRenderer> reference in blendshapeReference.blendshapes)
            {
                foreach (string blendshapeName in blendShapeNames)
                {
                    if (reference.Item2.Equals(blendshapeName))
                    {
                        if (shouldReverseMidway && currentAnimPercent >= reverseAnimPercent)
                        {
                            // Lerp each blendshape down to 0 relative to the speed of the animation
                            float currentBlendshapeWeight = reference.Item3.GetBlendShapeWeight(reference.Item1);
                            currentBlendshapeWeight = Mathf.Lerp(currentBlendshapeWeight, 0.0f, reverseBlendSpeed * Time.deltaTime);
                            reference.Item3.SetBlendShapeWeight(reference.Item1, currentBlendshapeWeight);

                            //if (currentBlendshapeWeight == 0.0f)
                            //{
                                animator.gameObject.transform.localPosition = blendshapeReference.originalLocalPosition;
                                animator.gameObject.transform.localRotation = blendshapeReference.originalLocalRotation;
                            //}
                        }
                        else
                        {
                            // Lerp each blendshape up to 100 relative to the speed of the animation
                            float currentBlendshapeWeight = reference.Item3.GetBlendShapeWeight(reference.Item1);
                            currentBlendshapeWeight = Mathf.Lerp(currentBlendshapeWeight, 100.0f, blendSpeed * Time.deltaTime);
                            reference.Item3.SetBlendShapeWeight(reference.Item1, currentBlendshapeWeight);
                        }

                        break;
                    }
                }
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (resetBlendShapesOnExit && blendshapeReference)
        {
            animator.gameObject.transform.localPosition = blendshapeReference.originalLocalPosition;
            animator.gameObject.transform.localRotation = blendshapeReference.originalLocalRotation;

            // Snap each blendshape's weight back down to 0 at the end of the animation
            foreach (Tuple<int, string, SkinnedMeshRenderer> reference in blendshapeReference.blendshapes)
            {
                foreach (string blendshapeName in blendShapeNames)
                {
                    if (reference.Item2.Equals(blendshapeName))
                    {
                        reference.Item3.SetBlendShapeWeight(reference.Item1, 0.0f);
                        break;
                    }
                }
            }
        }
    }
}
