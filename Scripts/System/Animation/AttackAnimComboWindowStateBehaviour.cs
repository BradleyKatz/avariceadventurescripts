﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script is used to handle the input logic behind basic attack combos. If another attack input is detected within a reasonable amount of time, the next attack in the combo will trigger. Otherwise, the player is reset.
 */

public class AttackAnimComboWindowStateBehaviour : StateMachineBehaviour
{
    public BasicAttackAbility basicAttackAbility = null;

    [Range(0, 1), Tooltip("The percentage of the way through the animation where the combo can be continued")]
    public float comboWindowStart;
    [Range(0, 1), Tooltip("The percentage of the way through the animation where the combo window ends")]
    public float comboWindowEnd;
    [Tooltip("The name of the next animation to play in the combo sequence")]
    public string nextComboAnimationName = string.Empty;

    protected bool isNextComboAnimValid = false;
    protected bool triggerNextAttack = false;
    protected Player player = null;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        triggerNextAttack = false;
        player = animator.gameObject.GetComponentInParent<Player>();

        if (animator.HasState(layerIndex, Animator.StringToHash(nextComboAnimationName)))
        {
            isNextComboAnimValid = true;
        }
        else
        {
            Debug.LogError("Error: Invalid combo animation name in player melee combo FSM");
        }
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (basicAttackAbility != null && isNextComboAnimValid)
        {
            float currentAnimPercent = System.Convert.ToSingle(stateInfo.normalizedTime - System.Math.Truncate(stateInfo.normalizedTime));
            if (currentAnimPercent >= comboWindowStart && currentAnimPercent <= comboWindowEnd)
            {
                if (!triggerNextAttack && Input.GetButtonDown(basicAttackAbility.attackInputButtonName))
                {
                    triggerNextAttack = true;
                    animator.SetTrigger("Attack");
                    player.bagAnimator.SetTrigger("Attack");
                    //animator.Play(nextComboAnimationName);
                    //player.bagAnimator.Play(nextComboAnimationName);

                    if (!player.controller.IsGrounded)
                    {
                        player.velocity.y = 0.0f;
                    }
                }
            }
            else if (stateInfo.normalizedTime > 1.0f)
            {
                if (player != null && !triggerNextAttack)
                {
                    player.isMovementFrozen = false;
                    player.isGravityFrozen = false;

                    if (player.controller.IsGrounded)
                    {
                        animator.Play("Locomotion");
                        player.bagAnimator.Play("Locomotion");
                    }
                    else
                    {
                        animator.Play("Falling");
                        player.bagAnimator.Play("Falling");
                        basicAttackAbility.airComboAlreadyPerformed = true;
                    }
                    

                    basicAttackAbility.IsActivated = false;
                }
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }
}
