﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This state behaviour models any and all behaviour that should be carried out when the player reaches the final animation in their attack combo.
 */

public class AttackComboEndStateBehaviour : StateMachineBehaviour
{
    public BasicAttackAbility attackComboAbility = null;

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AvariceEntity attachedEntity = animator.gameObject.GetComponentInParent<AvariceEntity>();
        if (attachedEntity != null && attackComboAbility != null)
        {
            attackComboAbility.IsActivated = false;

            attachedEntity.isMovementFrozen = false;

            if (!attachedEntity.controller.IsGrounded)
            {
                attackComboAbility.airComboAlreadyPerformed = true;
            }
        }
    }
}
