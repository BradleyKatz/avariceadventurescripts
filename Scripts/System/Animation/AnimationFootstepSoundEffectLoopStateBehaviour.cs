﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationFootstepSoundEffectLoopStateBehaviour : AnimationSoundEffectLoopStateBehaviour
{
    [Header("Speed Requirement Properties")]
    [Range(0.0f, 10.0f), Tooltip("The minimal speed value required to trigger footstep SFX")]
    public float requiredSpeed = 1.5f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetFloat("Speed") > requiredSpeed)
        {
            base.OnStateUpdate(animator, stateInfo, layerIndex);
        }
    }
}
