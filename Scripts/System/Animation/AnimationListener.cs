﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * Original Author: William Barry
 * Modified by: Bradley Katz
 */

public class AnimationListener : MonoBehaviour, IAnimationCompleted, IAnimationStarted, IAnimationPartway
{
    #region Animation Move Event
    private UnityEvent onAnimatorMoveEvent = new UnityEvent();

    public void AddAnimatorMoveListener(UnityAction callback)
    {
        onAnimatorMoveEvent.AddListener(callback);
    }
    public void RemoveAnimatorMoveListener(UnityAction callback)
    {
        onAnimatorMoveEvent.RemoveListener(callback);
    }

    private void OnAnimatorMove()
    {
        onAnimatorMoveEvent.Invoke();
    }
    #endregion

    #region Animation Started Event
    [System.Serializable]
    public class AnimationStartedEvent : UnityEvent<int> { }
    private Dictionary<int, AnimationStartedEvent> animationStartedEvents = new Dictionary<int, AnimationStartedEvent>();

    // Add Listener for Animation Started
    public void AddAnimationStartedListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationStartedEvent eventCallback;
        if (animationStartedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.AddListener(callback);
        }
        else
        {
            eventCallback = new AnimationStartedEvent();
            eventCallback.AddListener(callback);
            animationStartedEvents.Add(shortHashName, eventCallback);
        }
    }

    // Remove Listener for Animation Started
    public void RemoveAnimationStartedListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationStartedEvent eventCallback;
        if (animationStartedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.RemoveListener(callback);
        }
    }

    public void AnimationStarted(int shortHashName)
    {
        AnimationStartedEvent eventCallback;
        if (animationStartedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.Invoke(shortHashName);
        }
    }
    #endregion

    #region Animation Partway Event 
    [System.Serializable]
    public class AnimationPartwayEvent : UnityEvent<int> { }
    private Dictionary<int, AnimationPartwayEvent> animationPartwayEvents = new Dictionary<int, AnimationPartwayEvent>();

    // Add Listener for Animation Started 
    public void AddAnimationPartwayListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationPartwayEvent eventCallback;
        if (animationPartwayEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.AddListener(callback);
        }
        else
        {
            eventCallback = new AnimationPartwayEvent();
            eventCallback.AddListener(callback);
            animationPartwayEvents.Add(shortHashName, eventCallback);
        }
    }

    // Remove Listener for Animation Started 
    public void RemoveAnimationPartwayListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationPartwayEvent eventCallback;
        if (animationPartwayEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.RemoveListener(callback);
        }
    }

    public void AnimationPartway(int shortHashName)
    {
        AnimationPartwayEvent eventCallback;
        if (animationPartwayEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.Invoke(shortHashName);
        }
    }
    #endregion

    #region Animation Completed Event

    [System.Serializable]
    public class AnimationCompletedEvent : UnityEvent<int> { }

    private Dictionary<int, AnimationCompletedEvent> animationCompletedEvents = new Dictionary<int, AnimationCompletedEvent>();

    // Add Listener for Animation Completed
    public void AddAnimationCompletedListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationCompletedEvent eventCallback;
        if (animationCompletedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.AddListener(callback);
        }
        else
        {
            eventCallback = new AnimationCompletedEvent();
            eventCallback.AddListener(callback);
            animationCompletedEvents.Add(shortHashName, eventCallback);
        }
    }

    // Remove Listener for Animation Completed
    public void RemoveAnimationCompletedListener(int shortHashName, UnityAction<int> callback)
    {
        AnimationCompletedEvent eventCallback;
        if (animationCompletedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.RemoveListener(callback);
        }
    }

    public void AnimationCompleted(int shortHashName)
    {
        AnimationCompletedEvent eventCallback;
        if (animationCompletedEvents.TryGetValue(shortHashName, out eventCallback))
        {
            eventCallback.Invoke(shortHashName);
        }
    }
    #endregion
}
