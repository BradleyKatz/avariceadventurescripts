﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPartwayStateBehaviour : StateMachineBehaviour
{
    [Range(0, 1), Tooltip("The percentage of the way through the animation where any attached callbacks should be invoked")]
    public float timeForEventTrigger = 0.0f;

    protected bool isEffectTriggeredAlready = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isEffectTriggeredAlready = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!isEffectTriggeredAlready)
        {
            float currentAnimPercent = System.Convert.ToSingle(stateInfo.normalizedTime - System.Math.Truncate(stateInfo.normalizedTime));
            if (currentAnimPercent >= timeForEventTrigger)
            {
                isEffectTriggeredAlready = true;

                IAnimationPartway callback = animator.gameObject.GetComponentInParent(typeof(IAnimationPartway)) as IAnimationPartway;
                if (callback != null)
                {
                    callback.AnimationPartway(stateInfo.shortNameHash);
                }
                else
                {
                    callback = animator.gameObject.GetComponentInChildren(typeof(IAnimationPartway)) as IAnimationPartway;
                    if (callback != null)
                    {
                        callback.AnimationPartway(stateInfo.shortNameHash);
                    }
                }
            }
        }
    }
}
