﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * Simple interface that defines a callback method that should be called when a particular animation has started.
 */

public interface IAnimationStarted
{
    void AnimationStarted(int shortHashName);
}
