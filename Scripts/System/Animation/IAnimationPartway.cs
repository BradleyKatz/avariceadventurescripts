﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 */

public interface IAnimationPartway
{
    void AnimationPartway(int shortHashName);
}