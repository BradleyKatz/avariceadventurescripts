﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSoundEffectLoopStateBehaviour : StateMachineBehaviour
{
    [Header("Sound Effect Properties")]
    public Sound soundEffect = null;

    [Header("Looping Properties")]
    [Tooltip("Whether or not the attached sound effect will play more than once during a single cycle of the animation this is attached to")]
    public bool loopSoundEffect = false;
    [Range(0.0f, 1.0f), Tooltip("The time between playing another repeat of the attached sound effect if it's set to be looping, i.e. play the sound effect every half-second")]
    public float soundEffectRepeatWindow = 0.0f;

    protected float repeatWindowTimeElapsed = 0.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (soundEffect != null)
        {
            AudioManager.Instance?.RegisterSFX(soundEffect);
            AudioManager.Instance?.PlaySoundEffect(soundEffect.name);
        }
        
        repeatWindowTimeElapsed = 0.0f;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (soundEffect != null && loopSoundEffect)
        {
            repeatWindowTimeElapsed += Time.deltaTime;
            if (repeatWindowTimeElapsed >= soundEffectRepeatWindow)
            {
                AudioManager.Instance?.PlaySoundEffect(soundEffect.name);
                repeatWindowTimeElapsed = 0.0f;
            }
        }
    }
}
