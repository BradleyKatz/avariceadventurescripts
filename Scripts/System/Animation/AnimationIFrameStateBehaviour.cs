﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This StateMachineBehaviour serves as a tool for attaching iframes to certain animations for a variable length of said animation. This is currently used to implement the dodge roll iframes,
 * for example.
 */

public class AnimationIFrameStateBehaviour : StateMachineBehaviour
{
    [Header("Collision Window Properties")]
    [Range(0, 3), Tooltip("The amount of time to delay turning off iframes after the animation ends")]
    public float iframeWindowOffset = 0.5f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Damageable entityDamageable = animator.gameObject.GetComponentInParent<Damageable>();
        if (entityDamageable == null)
        {
            Debug.LogWarning("A Damageable could not be found on " + animator.gameObject.name + ", no iFrames will be applied");
        }
        else
        {
            entityDamageable.ActivateIFrames(stateInfo.length + iframeWindowOffset);
        }
    }
}
