﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This class serves as a base for any player ability related to combat by abstracting out the knockback properties here.
 */

public abstract class CombatAbilityBase : AbilityBase
{
    [Header("Knockback Properties")]
    public Vector2 knockbackDirection = Vector2.zero;
    [Range(0.0f, 10.0f)]
    public float knockbackStrength = 0.0f;
}
