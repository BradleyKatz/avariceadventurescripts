﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This class serves as the base of all "abilities" in our game, providing:
 * - A priority number which affects the update order of the player's abilities.
 * - The name of the ability.
 * - A sound to play when the ability is activated.
 * - A toggle for whether or not the ability is enabled.
 * - A list of other abilities that the current one can interrupt.
 * - A base damage number.
 * - Abstract skeletons of Update and FixedUpdate methods.
 * - A base implementation of Initialize that obtains a common reference of Player for every ability.
 * - A public property so the Player script can tell if an ability is currently active.
 */

public abstract class AbilityBase : ScriptableObject, IComparable<AbilityBase>
{
    [Header("Base Ability Properties")]
    [Range(1, 100), Tooltip("The priority of this ability as opposed to others, abilities with a higher priority update first")]
    public int priority = 1;
    public string abilityName = "New Ability";
    public Sound activationSound;
    public bool isEnabled = true;

    public int damage = 0;

    [Header("Interrupt Properties")]
    [Tooltip("Any ability objects referenced here will be interrupted when this ability starts")]
    public List<AbilityBase> abilitiesThisInterrupts = new List<AbilityBase>();

    [Header("Camera Shake Properties")]
    [Range(0.0f, 3.0f), Tooltip("The length of camera shake to apply when used")]
    public float shakeDuration = 0.0f;
    [Range(0.0f, 30.0f), Tooltip("The amplitude of the camera shake when used")]
    public float shakeAmplitude = 0.0f;
    [Range(0.0f, 3.0f), Tooltip("The frequency of the camera shake when used")]
    public float shakeFrequency = 0.0f;

    protected Player player = null;

    protected bool isActivated = false;
    public bool IsActivated
    {
        get
        {
            return isActivated;
        }
        set
        {
            if (isActivated == value) { return; }
            isActivated = value;

            if (isActivated && player != null)
            {
                player.blendshapeReference?.ResetAll();
                player.InterruptAbilities(abilitiesThisInterrupts);
            }
        }
    }

    public static bool operator <(AbilityBase lhs, AbilityBase rhs)
    {
        return lhs.priority < rhs.priority;
    }

    public static bool operator >(AbilityBase lhs, AbilityBase rhs)
    {
        return lhs.priority < rhs.priority;
    }

    public virtual void Initialize(GameObject obj)
    {
        player = obj.GetComponent<Player>();
        if (player == null)
        {
            Debug.LogError("Error: Expected Player script on GO");
        }

        if (activationSound.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(activationSound);
        }

        Reset();
    }

    public abstract void OnUpdate();
    public abstract void OnFixedUpdate();
    public abstract void Reset(int shortHashName = -1); // Incase we want to also use Reset for AnimationCompleted

    public int CompareTo(AbilityBase other)
    {
        int compareResult = 0;

        if (priority == other.priority)
            compareResult = 0;
        else if (priority < other.priority)
            compareResult = -1;
        else if (priority > other.priority)
            compareResult = 1;

        return compareResult;
    }
}
