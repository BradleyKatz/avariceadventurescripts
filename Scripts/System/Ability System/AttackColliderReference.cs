﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script serves as an inspector-based link between a combat ability (attack combo, propel, etc) and a collider. 
 * This is used so that we may define different collision shapes for each ability as appropriate. The player's damager will maintain a mapping of these abilities and colliders
 * so that the appropriate collider may be invoked when checking for collisions during certain attacks.
 */

[RequireComponent(typeof(BoxCollider2D))]
public class AttackColliderReference : MonoBehaviour
{
    public CombatAbilityBase combatAbility = null;
    [HideInInspector] public BoxCollider2D abilityCollider = null;

    private void Awake()
    {
        abilityCollider = GetComponent<BoxCollider2D>();
    }
}
