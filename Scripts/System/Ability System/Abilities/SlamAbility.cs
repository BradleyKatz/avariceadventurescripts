﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Slam ability, including its input checking, velocity calculations, and state handling (grounded, midair, falling, etc).
 */

[CreateAssetMenu(menuName = "Abilities/Slam")]
public class SlamAbility : CombatAbilityBase
{
    protected enum eSlamState
    {
        Inactive,
        Grounded,
        Midair,
        Falling
    }
    protected eSlamState currentSlamState;

    [Header("Input Properties")]
    public string inputButtonName = "Capture";

    [Header("Slam Properties")]
    [Tooltip("Reference to the shockwave prefab that the player will spawn on impact with the ground")]
    public GameObject shockwavePrefab = null;
    [Range(0.0f, 30.0f), Tooltip("How fast both shockwaves will travel once spawned")]
    public float shockwaveMoveSpeed = 4.0f;
    [Range(0.0f, 5.0f), Tooltip("The amount of time each shockwave can exist before being destroyed by default")]
    public float shockwaveDuration = 2.0f;
    [Range(0, 10), Tooltip("The amount of damage each shockwave will deal")]
    public int shockwaveDamage = 2;

    [Header("Midair Charge State Properties")]
    [Range(0.0f, 1.0f), Tooltip("The amount of time the player pauses in midair before entering the falling state.")]
    public float maxTimeMidair = 0.5f;
    protected float midairTimeElapsed = 0.0f;

    [Header("Falling State Properties")]
    [Range(0.0f, 2.0f), Tooltip("The amoutn by which to multiply the player's gravity when falling")]
    public float fallingGravityMultiplier = 0.5f;

    public override void Initialize(GameObject obj)
    {
        AudioManager.Instance.RegisterSFX(activationSound);

        base.Initialize(obj);
        if (player != null)
        {
            Reset();
        }
    }

    public override void OnUpdate()
    {
        switch (currentSlamState)
        {
            case eSlamState.Inactive:
                {
                    IsActivated = !IsActivated && InputHandler.GetButtonDown(inputButtonName);

                    if (IsActivated && !player.controller.IsCeilingHit)
                    {
                        player.animationListener.AddAnimationPartwayListener(Animator.StringToHash("Grounded Slam"), SpawnShockwaves);
                        //player.animationListener.AddAnimationPartwayListener(Animator.StringToHash("Falling"), SpawnShockwaves);

                        player.ResetPendingAnimationTriggers();
                        player.isMovementFrozen = true;

                        player.animator.SetTrigger("Slam");
                        player.bagAnimator.SetTrigger("Slam");
                        currentSlamState = player.controller.IsGrounded ? eSlamState.Grounded : eSlamState.Midair;
                    }
                }
                break;
            case eSlamState.Midair:
                {
                    player.velocity.y = 0.0f;

                    midairTimeElapsed += Time.deltaTime;
                    if (midairTimeElapsed >= maxTimeMidair)
                    {
                        currentSlamState = eSlamState.Falling;
                        player.animator.SetTrigger("SlamFalling");
                        player.bagAnimator.SetTrigger("SlamFalling");
                        player.controller.IsGrounded = false;
                        player.isGravityFrozen = false;

                        midairTimeElapsed = 0.0f;
                        ApplyDownwardVelocity();
                    }
                }
                break;
            case eSlamState.Falling:
                {
                    if (player.controller.IsGrounded)
                    {
                        SpawnShockwaves();
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {

    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        currentSlamState = eSlamState.Inactive;
        midairTimeElapsed = 0.0f;

        player.animationListener.RemoveAnimationPartwayListener(Animator.StringToHash("Grounded Slam"), SpawnShockwaves);
        //player.animationListener.RemoveAnimationPartwayListener(Animator.StringToHash("Falling"), SpawnShockwaves);
    }

    protected void ApplyDownwardVelocity()
    {
        player.velocity.y = player.gravity * fallingGravityMultiplier;
    }

    protected void SpawnShockwaves(int shortHashName = 0)
    {
        AudioManager.Instance.PlaySoundEffect(activationSound.name);

        if (shockwavePrefab != null)
        {
            Vector2 shockwaveSpawnPos = new Vector2(player.transform.position.x + (2.0f * player.GetHorizontalFacingDirection()), player.transform.position.y + 0.4f);

            ProjectileLogic shockwave = Instantiate(shockwavePrefab, shockwaveSpawnPos, Quaternion.identity).GetComponent<ProjectileLogic>();
            shockwave.moveSpeed = shockwaveMoveSpeed;
            shockwave.duration = shockwaveDuration;
            shockwave.damage = shockwaveDamage;
            shockwave.velocity.x = shockwaveMoveSpeed * player.GetHorizontalFacingDirection();
        }

        player.isMovementFrozen = false;
        Reset();
        player.ResetPendingAnimationTriggers();
    }
}
