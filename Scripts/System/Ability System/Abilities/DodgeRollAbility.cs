﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Dodge Roll ability, including its input checking, velocity calculations, and state handling (grounded, rising, falling, etc).
 */

[CreateAssetMenu(menuName = "Abilities/Dodge Roll")]
public class DodgeRollAbility : AbilityBase
{
    protected enum eDodgeRollState
    {
        Inactive,
        Rolling,
    }
    protected eDodgeRollState currentDodgeState;

    [Header("Input Properties")]
    public string dodgeInputButtonName = "DodgeRoll";

    [Header("Animation Properties")]
    [Tooltip("The name of the animator node tied to the dodge animation")]
    public string dodgeAnimationName = "Dodge Roll";

    [Header("Movement Properties")]
    [Range(0.0f, 100.0f), Tooltip("The target distance to travel when rolling")]
    public float xDistance = 5.0f;
    [Range(0.0f, 30.0f), Tooltip("The amount of forward movement to apply to dodge rolls")]
    public float xVelocity = 4.0f;
    [Range(0.0f, 30.0f), Tooltip("The amount of downward movement to apply to dodge rolls in the air")]
    public float yVelocity = 4.0f;

    [Header("Delay Properties")]
    [Range(1, 10), Tooltip("The number of rolls the player is able to perform before this ability is put on cooldown")]
    public int numRollsBeforeDelay = 1;
    [Range(0.0f, 1.0f), Tooltip("The length of time this ability must cool down for after exceeding the maximum number of rolls")]
    public float cooldownTime = 2.0f;

    protected int numRollsPerformed = 0;
    protected float cooldownTimeElapsed = 0.0f;
    protected float startX = 0.0f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        if (player != null && player.animator.HasState(0, Animator.StringToHash(dodgeAnimationName)))
        {
            Reset();
        }
    }

    protected void SetDodgeVelocity()
    {
        player.velocity.x = player.GetHorizontalFacingDirection() * xVelocity;

        if (!player.controller.IsGrounded)
        {
            player.velocity.y = -yVelocity;
        }
    }

    public override void OnUpdate()
    {
        if (player != null)
        {
            switch (currentDodgeState)
            {
                case eDodgeRollState.Inactive:
                    {
                        if (numRollsPerformed >= numRollsBeforeDelay)
                        {
                            cooldownTimeElapsed += Time.deltaTime;
                            if (cooldownTimeElapsed >= cooldownTime)
                            {
                                Reset();
                                cooldownTimeElapsed = 0.0f;
                                numRollsPerformed = 0;
                            }
                        }
                        else
                        {
                            IsActivated = !IsActivated && !player.isMovementFrozen && InputHandler.GetButtonDown(dodgeInputButtonName);
                            if (IsActivated)
                            {

                                ++numRollsPerformed;
                                player.isMovementFrozen = true;

                                player.ResetPendingAnimationTriggers();

                                if (player.controller.IsGrounded)
                                {
                                    player.animator.SetBool("DodgeRoll", true);
                                    player.bagAnimator.SetBool("DodgeRoll", true);
                                }
                                else
                                {
                                    player.animator.Play("Dodge Roll");
                                    player.bagAnimator.Play("Dodge Roll");
                                }

                                AudioManager.Instance.PlaySoundEffect(activationSound.name);
                                currentDodgeState = eDodgeRollState.Rolling;
                                startX = player.transform.position.x;

                                SetDodgeVelocity();
                            }
                        }
                    }
                    break;
                case eDodgeRollState.Rolling:
                    {
                        SetDodgeVelocity();

                        if (player.GetHorizontalFacingDirection() < 0.0f)
                        {
                            if (player.transform.position.x < startX - xDistance || player.controller.IsWallHit)
                            {
                                Reset();
                            }
                        }
                        else
                        {
                            if (player.transform.position.x > startX + xDistance || player.controller.IsWallHit)
                            {
                                Reset();
                            }
                        }
                    }
                    break;
            }

        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        player.isMovementFrozen = false;
        currentDodgeState = eDodgeRollState.Inactive;
        IsActivated = false;
        cooldownTimeElapsed = 0.0f;
        startX = 0.0f;
        player.animator.SetBool("DodgeRoll", false);
        player.bagAnimator.SetBool("DodgeRoll", false);
    }
}
