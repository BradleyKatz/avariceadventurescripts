﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Propel ability, including its input checking, velocity calculations, and state handling (grounded, rising, falling, etc).
 */

[CreateAssetMenu(menuName = "Abilities/Propel")]
public class PropelAbility : CombatAbilityBase
{
    protected enum ePropelState
    {
        Inactive,
        Grounded,
        Rising,
        Midair,
        Cooldown
    }
    protected ePropelState currentPropelState;

    [Header("Input Properties")]
    public string propelInputButtonName = "Jump";

    [Header("Propel Properties")]
    [Range(0.0f, 20.0f), Tooltip("The amount of units above the player to move with Propel")]
    public float propelHeight = 6.0f;
    [Range(0.0f, 20.0f), Tooltip("How much to move the player along the x-axis when using propel")]
    public float xPushVelocity = 2.0f;

    [Header("Grounded State Properties")]
    [Range(0.0f, 2.0f), Tooltip("The amount of time the player remains on the ground after activating Propel and before they begin to rise")]
    public float maxTimeGrounded = 0.0f;
    protected float groundedTimeElapsed = 0.0f;

    [Header("Cooldown State Properties")]
    [Range(0.0f, 5.0f), Tooltip("The amount of cooldown delay applied to Propel after hitting the ground")]
    public float cooldownTime = 0.3f;
    protected float cooldownTimeElapsed = 0.0f;
    protected bool isCooldownParticlesPlaying = false;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        if (player != null)
        {
            Reset();
        }
    }

    public override void OnUpdate()
    {
        switch (currentPropelState)
        {
            case ePropelState.Inactive:
                {
                    IsActivated = !IsActivated && InputHandler.GetButtonDown(propelInputButtonName);

                    if (IsActivated && !player.controller.IsCeilingHit)
                    {
                        player.ResetPendingAnimationTriggers();
                        player.animator.SetTrigger("Propel");
                        player.bagAnimator.SetTrigger("Propel");

                        if (player.controller.IsGrounded)
                        {
                            currentPropelState = ePropelState.Grounded;
                        }
                        else
                        {
                            player.velocity.y = 0.0f;
                            currentPropelState = ePropelState.Rising;
                        }
                    }
                    else
                    {
                        IsActivated = false;
                    }
                }
                break;
            case ePropelState.Grounded:
                {
                    groundedTimeElapsed += Time.deltaTime;
                    if (groundedTimeElapsed >= maxTimeGrounded)
                    {
                        currentPropelState = ePropelState.Rising;
                        groundedTimeElapsed = 0.0f;
                    }
                }
                break;

            case ePropelState.Rising:
                {
                    ApplyPropelVelocity();
                    currentPropelState = ePropelState.Midair;
                }
                break;

            case ePropelState.Midair:
                {
                    if (player.velocity.y <= 0.0f)
                    {
                        IsActivated = false;
                    }

                    if (player.controller.IsCeilingHit)
                    {
                        CameraController.Instance?.ActivateCameraShake(shakeDuration, shakeAmplitude, shakeFrequency);

                        Obstruction obstruction = player.controller.HitColliderAbove?.GetComponent<Obstruction>();
                        if (obstruction)
                        {
                            player.controller.IsCeilingHit = false;
                        }
                        else
                        {
                            player.ResetAnimatorAndAbilities(this);
                        }
                        
                    }

                    if (player.controller.IsGrounded)
                    {
                        currentPropelState = ePropelState.Cooldown;
                        player.controller.isIgnoringPlatformMovement = false;
                    }
                }
                break;
            case ePropelState.Cooldown:
                {
                    if (!isCooldownParticlesPlaying)
                    {
                        cooldownTimeElapsed += Time.deltaTime;
                        if (cooldownTimeElapsed >= cooldownTime)
                        {
                            if (player.propelCooldownEffect != null && player.propelCooldownEffect.isStopped)
                            {
                                player.propelCooldownEffect.Play();
                                isCooldownParticlesPlaying = true;
                            }
                        }
                    }
                    else
                    {
                        if (player.propelCooldownEffect != null && player.propelCooldownEffect.isStopped)
                        {
                            Reset();
                        }
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        isCooldownParticlesPlaying = false;
        currentPropelState = ePropelState.Inactive;
        groundedTimeElapsed = 0.0f;
        cooldownTimeElapsed = 0.0f;
        player.controller.isIgnoringPlatformMovement = false;
    }

    protected void ApplyPropelVelocity()
    {
        player.velocity.y = Mathf.Sqrt(2.0f * -player.gravity * propelHeight);

        if (xPushVelocity > 0.0f)
        {
            player.velocity.x = player.GetHorizontalFacingDirection() * xPushVelocity;
        }

        player.controller.isIgnoringPlatformMovement = true;
    }
}
