﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Used to for the player to spawn a projectile and handles the values of that projectile
/// </summary>
[CreateAssetMenu(menuName = "Abilities/Projectile")]
public class ProjectileAbility : AbilityBase
{
    [Header("InputHandler Properties")]
    public string projectileInputButtonName = "Fire";
    public GameObject projectile;
    public float projectileSpeed = 20;
    public float projectileDuration = 3;
    [Range(1, 100)]
    public int projectileDamage = 35;
    [Range(0, 1), Tooltip("Delay between uses")]
    public float delay = 1f;

    private float timer;
    private const float airCastAnimBlendPercent = 0.65f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
    }

    public override void OnUpdate()
    {
        timer -= Time.deltaTime;
        if (IsActivated)
        {
            IsActivated = false;
        }
        else if (!IsActivated && InputHandler.GetButtonDown(projectileInputButtonName) && timer <= 0)
        {
            player.ResetPendingAnimationTriggers();

            IsActivated = true;
            timer = delay;
            CastSpell();
        }
    }

    public override void OnFixedUpdate()
    {

    }

    public void CastSpell()
    {
        player.animator.SetFloat("AirCastBlend", airCastAnimBlendPercent);
        player.animator.SetTrigger("Cast");
        player.bagAnimator.SetTrigger("Cast");


        Vector3 dir = player.GetHorizontalFacingDirection() == -1 ? new Vector3(0, 180, 0) : new Vector3(0, 0, 0);

        var proj = Instantiate(projectile, player.gameObject.transform.position, new Quaternion(dir.x, dir.y, dir.z, 0));

        var entity = proj.GetComponent<ProjectileLogic>();
        entity.velocity.x = player.GetHorizontalFacingDirection() * projectileSpeed;
        entity.moveSpeed = projectileSpeed;
        entity.duration = projectileDuration;
        entity.damage = (int)projectileDamage;
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        timer = 0.0f;
    }
}
