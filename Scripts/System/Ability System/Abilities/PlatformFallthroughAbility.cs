﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script simply checks for the appropriate input for platform fallthrough, and if applicable, sets the appropriate state to tell the player's AvariceCC to fall through the current platform.
 */

[CreateAssetMenu(menuName = "Abilities/Platform Fallthrough")]
public class PlatformFallthroughAbility : AbilityBase
{
    public string fallthroughInputAxisName = "Vertical";
    public string fallthroughInputButtonName = "Jump";

    protected bool firstActivate = true;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
    }

    public override void OnUpdate()
    {
        IsActivated = player.isFallingThroughPlatform || (player.controller.IsGrounded && InputHandler.GetAxisRaw(fallthroughInputAxisName) == -1.0f && InputHandler.GetButtonDown(fallthroughInputButtonName));
        
        if (IsActivated && firstActivate)
        {
            if (player.controller.HitColliderBelow?.GetComponent<PlatformController>() != null || player.controller.currentRidingPlatform != null)
            {
                firstActivate = false;
                player.isFallingThroughPlatform = true;
                player.controller.IsGrounded = false;
                player.controller.isIgnoringPlatformMovement = true;
            }
        }
        else if (!player.isFallingThroughPlatform && player.controller.IsGrounded)
        {
            Reset();
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        firstActivate = true;
        player.isFallingThroughPlatform = false;
        player.controller.isIgnoringPlatformMovement = false;
    }
}
