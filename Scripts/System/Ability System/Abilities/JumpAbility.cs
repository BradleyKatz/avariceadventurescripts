﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Jump ability, including its input checking, velocity calculations, and state handling (grounded, rising, falling, etc).
 */

[CreateAssetMenu(menuName = "Abilities/Jump")]
public class JumpAbility : AbilityBase
{
    protected enum eJumpState
    {
        Inactive,
        Grounded,
        Midair
    }
    protected eJumpState currentJumpState = eJumpState.Inactive;

    [Header("Input Properties")]
    public string jumpInputAxisName = "Jump";

    [Header("Jump Properties")]
    [Range(0.0f, 10.0f), Tooltip("The maximum jump height")]
    public float jumpHeight = 2.0f;
    [Range(0.0f, 1.0f), Tooltip("The time it takes the player to rise to their jump height")]
    public float jumpTime = 0.4f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        Reset();

        player.gravity = -(2.0f * jumpHeight) / Mathf.Pow(jumpTime, 2.0f);

        player.animationListener.AddAnimationPartwayListener(Animator.StringToHash("Jump Start"), CalculateJumpVelocity);
    }

    private void CalculateJumpVelocity(int shortHashName)
    { 
        player.velocity.y = Mathf.Sqrt(2.0f * -player.gravity * jumpHeight);

        player.controller.IsGrounded = false;
        player.controller.isIgnoringPlatformMovement = true;
        currentJumpState = eJumpState.Midair;
        AudioManager.Instance.PlaySoundEffect(activationSound.name);
    }

    public override void OnUpdate()
    {
        switch (currentJumpState)
        {
            case eJumpState.Inactive:
                {
                    IsActivated = player.controller.IsGrounded && !player.isFallingThroughPlatform && InputHandler.GetButtonDown(jumpInputAxisName);
                    if (IsActivated)
                    {
                        currentJumpState = eJumpState.Grounded;

                        player.ResetPendingAnimationTriggers();
                        player.animator.SetTrigger("JumpStart");
                        player.bagAnimator.SetTrigger("JumpStart");
                        player.gravity = -(2.0f * jumpHeight) / Mathf.Pow(jumpTime, 2.0f);
                    }
                }
                break;
            case eJumpState.Midair:
                {
                    if (player.controller.IsGrounded)
                    {
                        if (shakeDuration > 0.0f)
                        {
                            //CameraController.Instance.ActivateCameraShake(shakeDuration, shakeAmplitude, shakeFrequency);
                        }

                        Reset();
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        currentJumpState = eJumpState.Inactive;
        player.controller.isIgnoringPlatformMovement = false;
    }
}
