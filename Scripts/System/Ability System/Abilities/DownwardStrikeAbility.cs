﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Downward Strike ability, including its input checking, velocity calculations, and state handling (grounded, rising, falling, etc).
 */

[CreateAssetMenu(menuName = "Abilities/Downward Strike")]
public class DownwardStrikeAbility : CombatAbilityBase
{
    protected enum eDownwardStrikeState
    {
        Inactive,
        Midair,
        Falling
    }
    protected eDownwardStrikeState currentDownwardStrikeState;

    [Header("Input Properties")]
    public string inputAxisName = "Vertical";
    public string inputButtonName = "Attack";

    [Header("Midair Charge State Properties")]
    [Range(0.0f, 1.0f), Tooltip("The amount of time the player pauses in midair before entering the falling state.")]
    public float maxTimeMidair = 0.5f;
    protected float midairTimeElapsed = 0.0f;

    [Header("Falling State Properties")]
    [Range(0.0f, 2.0f), Tooltip("The amoutn by which to multiply the player's gravity when falling")]
    public float downwardStrikeGravityMultiplier = 0.5f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        if (player != null)
        {
            Reset();
            player.animationListener.AddAnimationCompletedListener(Animator.StringToHash("Fall"), Reset);
        }
    }

    public override void OnUpdate()
    {
        switch (currentDownwardStrikeState)
        {
            case eDownwardStrikeState.Inactive:
                {
                    IsActivated = !IsActivated && InputHandler.GetAxisRaw(inputAxisName) <= -0.8f && InputHandler.GetButtonDown(inputButtonName);

                    if (IsActivated && !player.controller.IsGrounded)
                    {
                        player.ResetPendingAnimationTriggers();
                        player.isMovementFrozen = false;
                        player.isGravityFrozen = true;

                        player.velocity.y = 0.0f;

                        currentDownwardStrikeState = eDownwardStrikeState.Midair;
                        player.animator.SetTrigger("DownwardStrikeCharge");
                        player.bagAnimator.SetTrigger("DownwardStrikeCharge");
                    }
                    else
                    {
                        IsActivated = false;
                    }
                }
                break;
            case eDownwardStrikeState.Midair:
                {
                    midairTimeElapsed += Time.deltaTime;
                    if (midairTimeElapsed >= maxTimeMidair)
                    {
                        currentDownwardStrikeState = eDownwardStrikeState.Falling;
                        player.animator.SetTrigger("DownwardStrikeFall");
                        player.bagAnimator.SetTrigger("DownwardStrikeFall");
                        player.controller.IsGrounded = false;

                        midairTimeElapsed = 0.0f;
                        ApplyDownwardVelocity();
                    }
                }
                break;
            case eDownwardStrikeState.Falling:
                {
                    if (player.controller.IsGrounded)
                    {
                        //player.isMovementFrozen = false;
                        //Reset();
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    protected void ApplyDownwardVelocity()
    {
        float downwardStrikeVelocity = player.gravity * downwardStrikeGravityMultiplier;

        //player.velocity.x = 0.0f;
        player.velocity.y = downwardStrikeVelocity;
    }

    public override void Reset(int shortHashName = -1)
    {
        midairTimeElapsed = 0.0f;
        currentDownwardStrikeState = eDownwardStrikeState.Inactive;
        IsActivated = false;
        player.isGravityFrozen = false;
        player.isMovementFrozen = false;
    }
}
