﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script essentially kicks off the basic attack combo by setting the appropriate state depending on whether the player is grounded or in the air.
 */

[CreateAssetMenu(menuName = "Abilities/Basic Attack")]
public class BasicAttackAbility : CombatAbilityBase
{
    [Header("Input Properties")]
    public string attackInputButtonName = "Attack";

    [HideInInspector] public bool airComboAlreadyPerformed = false;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        airComboAlreadyPerformed = false;
    }

    public override void OnUpdate()
    {
        if (!IsActivated && !airComboAlreadyPerformed)
        {
            IsActivated = InputHandler.GetButtonDown(attackInputButtonName);

            if (IsActivated)
            {
                if (player.controller.IsGrounded)
                {
                    player.animator.SetTrigger("Attack");
                    player.bagAnimator.SetTrigger("Attack");
                }
                else if (!player.controller.IsGrounded && !airComboAlreadyPerformed)
                {
                    player.velocity.y = 0.0f;
                    player.isGravityFrozen = true;
                    player.animator.SetTrigger("Attack");
                    player.bagAnimator.SetTrigger("Attack");
                }
            }
        }

        if (player.controller.IsGrounded && airComboAlreadyPerformed)
        {
            airComboAlreadyPerformed = false;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        airComboAlreadyPerformed = false;
    }
}
