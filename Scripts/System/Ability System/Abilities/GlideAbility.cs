﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Glide ability, including its input checking, velocity calculations, and state handling.
 */

[CreateAssetMenu(menuName = "Abilities/Glide")]
public class GlideAbility : AbilityBase
{
    protected enum eGlideState
    {
        Inactive,
        Gliding
    }
    protected eGlideState currentGlideState = eGlideState.Inactive;

    [Header("Input Properties")]
    public string glideInputAxisName = "Glide";

    [Header("Glide Properties")]
    [Range(0.0f, 4.0f), Tooltip("The speed at which the player moves along the x-axis while gliding")]
    public float glideVelocityX = 1.5f;

    [Header("Vertical Velocity Properties")]
    [Range(0.0f, 100.0f), Tooltip("The initial speed at which the player moves along the y-axis while gliding")]
    public float glideVelocityYStart = 5.0f;
    [Range(0.0f, 100.0f), Tooltip("The final speed at which the player moves along the y-axis while gliding")]
    public float glideVelocityYEnd = 10.0f;
    [Range(0.0f, 4.0f)]
    public float timeToVelocityChange = 0.2f;
    [Range(0.0f, 15.0f), Tooltip("The time it takes to accelerate towards either y-velocity target")]
    public float glideAccelerationTime = 0.5f;

    protected float glideTimeElapsed = 0.0f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        Reset();
    }

    public override void OnUpdate()
    {
        switch (currentGlideState)
        {
            case eGlideState.Inactive:
                {
                    if (player.LastActivatedAbility == "Jump")
                    {
                        IsActivated = !player.isMovementFrozen && InputHandler.GetButtonDown(glideInputAxisName) && !player.controller.IsGrounded && !player.isFallingThroughPlatform;
                    }
                    else
                    {
                        IsActivated = !player.isMovementFrozen && InputHandler.GetButton(glideInputAxisName) && !player.controller.IsGrounded && !player.isFallingThroughPlatform;
                    }

                    if (IsActivated)
                    {
                        currentGlideState = eGlideState.Gliding;
                    }
                }
                    break;
            case eGlideState.Gliding:
                {
                    if (InputHandler.GetButton(glideInputAxisName) && !player.controller.IsGrounded)
                    {
                        player.animator.SetBool("IsGliding", true);
                        player.bagAnimator.SetBool("IsGliding", true);
                        glideTimeElapsed += Time.deltaTime;
                        float targetVelocityY = (glideTimeElapsed > timeToVelocityChange) ? glideVelocityYEnd : glideVelocityYStart;

                        player.velocity.y = Mathf.SmoothDamp(player.velocity.y, -targetVelocityY, ref player.velocityYSmoothing, -glideAccelerationTime);
                        player.velocity.x = Mathf.SmoothDamp(player.velocity.x, player.targetVelocityX * glideVelocityX, ref player.velocityXSmoothing, player.accelerationTimeGround);
                    }
                    else
                    {
                        Reset();
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        currentGlideState = eGlideState.Inactive;
        glideTimeElapsed = 0.0f;
        player.animator.SetBool("IsGliding", false);
        player.bagAnimator.SetBool("IsGliding", false);
    }
}
