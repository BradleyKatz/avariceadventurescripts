﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models the logic attached to the Scarab Strike ability, including its input checking, velocity calculations, and state handling.
 */

[CreateAssetMenu(menuName = "Abilities/Scarab Strike")]
public class ScarabStrikeAbility : CombatAbilityBase
{
    protected enum eScarabStrikeState
    {
        Inactive,
        Charging,
        Rushing
    }
    protected eScarabStrikeState currentScarabStrikeState;

    [Header("Input Properties")]
    public string inputButtonName = "Capture";

    [Header("Animation Properties")]
    public string chargeTransitionName = "ScarabStrikeCharge";
    public string strikeTransitionName = "ScarabStrikeRush";

    [Header("Charging State Properties")]
    [Range(0.0f, 2.0f), Tooltip("The time that the player pauses to charge before rushing forward, if any")]
    public float chargeTime = 0.3f;
    protected float chargeTimeElapsed = 0.0f;

    [Header("Rush State Properties")]
    [Range(0.0f, 100.0f), Tooltip("The amount by which to move the player along the x-axis when rushing")]
    public float xRushDistance = 50.0f;
    [Range(0.0f, 100.0f), Tooltip("The velocity at which to push the player towards xRushDistance")]
    public float xRushVelocity = 50.0f;
    protected float startX = 0.0f;

    public override void Initialize(GameObject obj)
    {
        base.Initialize(obj);
        if (player != null)
        {
            Reset();
        }
    }

    public override void OnUpdate()
    {
        switch (currentScarabStrikeState)
        {
            case eScarabStrikeState.Inactive:
                {
                    IsActivated = !player.isMovementFrozen && InputHandler.GetButtonDown(inputButtonName);
                    if (IsActivated)
                    {
                        player.ResetPendingAnimationTriggers();
                        player.SetScarabStrikeTrailsActive(true);
                        player.animator.SetTrigger(chargeTransitionName);
                        player.bagAnimator.SetTrigger(chargeTransitionName);

                        currentScarabStrikeState = eScarabStrikeState.Charging;
                        player.playerHealth.ActivateIFrames();
                        player.isGravityFrozen = true;
                        player.isMovementFrozen = true;
                        player.velocity.y = 0.0f;
                        player.velocity.x = 0.000001f * player.GetHorizontalFacingDirection();
                    }
                }
                break;
            case eScarabStrikeState.Charging:
                {
                    chargeTimeElapsed += Time.deltaTime;
                    if (chargeTimeElapsed >= chargeTime)
                    {
                        currentScarabStrikeState = eScarabStrikeState.Rushing;
                        player.animator.SetBool(strikeTransitionName, true);
                        player.bagAnimator.SetBool(strikeTransitionName, true);

                        startX = player.transform.position.x;
                    }
                }
                break;
            case eScarabStrikeState.Rushing:
                {
                    player.velocity.x = Mathf.SmoothDamp(player.velocity.x, player.GetHorizontalFacingDirection() * xRushVelocity, ref player.velocityXSmoothing, player.accelerationTimeGround);

                    if (player.GetHorizontalFacingDirection() == -1.0f)
                    {
                        if ((player.transform.position.x <= startX - xRushDistance) || player.controller.IsWallHit)
                        {
                            player.animator.SetBool(strikeTransitionName, false);
                            player.bagAnimator.SetBool(strikeTransitionName, false);
                            Reset();
                        }
                    }
                    else
                    {
                        if ((player.transform.position.x >= startX + xRushDistance) || player.controller.IsWallHit)
                        {
                            player.animator.SetBool(strikeTransitionName, false);
                            player.bagAnimator.SetBool(strikeTransitionName, false);
                            Reset();
                        }
                    }
                }
                break;
        }
    }

    public override void OnFixedUpdate()
    {
    }

    public override void Reset(int shortHashName = -1)
    {
        IsActivated = false;
        currentScarabStrikeState = eScarabStrikeState.Inactive;
        chargeTimeElapsed = 0.0f;
        startX = 0.0f;
        player.isGravityFrozen = false;
        player.isMovementFrozen = false;
        player.velocity.x = 0.0f;
        player.SetScarabStrikeTrailsActive(false);
    }
}
