﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: William Barry
 * Modified by:
 */

[CreateAssetMenu(fileName = "MenuClassifier", menuName = "Create/Menu Classifier")]
public class MenuClassifier : ScriptableObject
{
    public string menuName;
}
