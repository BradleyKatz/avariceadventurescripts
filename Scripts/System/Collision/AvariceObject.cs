﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * AvariceObject can be considered as the second half of our game's collision. While AvariceCC handles all of the physics based collision using the raycasting system, AvariceObject provides our own implementation of 
 * trigger collision callbacks using the size of an entity's collider for reference. 
 */

public class AvariceObject : MonoBehaviour
{
    [Header("Collision Properties")]
    [Tooltip("The physics layer(s) that define which entities are interactable with this current one")]
    public LayerMask entityCollisionMask;
    public bool isTrigger = false;

    private HashSet<Collider2D> prevIntersectedColliders = new HashSet<Collider2D>();
    private HashSet<Collider2D> intersectedColliders = new HashSet<Collider2D>();

    protected BoxCollider2D box2D;

    protected virtual void Start()
    {
        box2D = GetComponent<BoxCollider2D>();
    }

    protected virtual void Update()
    {
    }

    public void ResetCollisionMap()
    {
        prevIntersectedColliders.Clear();
        intersectedColliders.Clear();
    }

    protected virtual void FixedUpdate()
    {
        // Do nothing if the entity collision mask wasn't set.
        if (entityCollisionMask.value == 0 || box2D == null) { return; }

        prevIntersectedColliders = new HashSet<Collider2D>(intersectedColliders);

        Collider2D[] hits = Physics2D.OverlapBoxAll(new Vector2(transform.position.x + box2D.offset.x * Mathf.Sign(transform.right.x), transform.position.y + box2D.offset.y), box2D.bounds.size, transform.eulerAngles.z, entityCollisionMask);
        if (hits.Length > 0)
        {
            foreach (Collider2D hit in hits)
            {
                bool foundInColliders = intersectedColliders.Contains(hit);
                bool foundInPrevColliders = prevIntersectedColliders.Contains(hit);

                if (!foundInPrevColliders && !foundInColliders)
                {
                    intersectedColliders.Add(hit);

                    if (isTrigger)
                        TriggerEnter(hit);
                    else
                        CollisionEnter(hit);
                }
                else if (foundInColliders && foundInPrevColliders)
                {
                    if (isTrigger)
                        TriggerStay(hit);
                    else
                        CollisionStay(hit);
                }
            }
        }
        else
        {
            foreach (Collider2D collider in intersectedColliders)
            {
                if (isTrigger)
                    TriggerExit(collider);
                else
                    CollisionExit(collider);
            }

            intersectedColliders.Clear();
        }
    }

    #region Collision Callbacks
    protected virtual void CollisionEnter(Collider2D other)
    {
        //Debug.Log("Collision Enter");
    }

    protected virtual void CollisionStay(Collider2D other)
    {
        //Debug.Log("Collision Stay");
    }

    protected virtual void CollisionExit(Collider2D other)
    {
        //Debug.Log("CollisionExit");
    }
    #endregion

    #region Trigger Callbacks
    protected virtual void TriggerEnter(Collider2D other)
    {
        //Debug.Log("Trigger Enter");
    }

    protected virtual void TriggerStay(Collider2D other)
    {
        //Debug.Log("Trigger Stay");
    }

    protected virtual void TriggerExit(Collider2D other)
    {
        //Debug.Log("Trigger Exit");
    }
    #endregion
}
