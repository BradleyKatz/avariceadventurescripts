﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

[System.Serializable]
public struct Item
{
    public GameObject item;
    public float dropChance;
}
