﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CoinEffect : MonoBehaviour
{
    [Header("Coin Properties")]
    public int value;
    public float peakHeight = 5.5f;
    public float riseSpeed = 4.0f;
    public float chaseSpeed = 10.0f;

    private Vector3 startPosition;
    private Vector3 endPosition;
    private GameObject rootObject;

    private float startTime;
    private float journeyLength;

    private bool bPeakReached = false;

    // Start is called before the first frame update
    void Start()
    {
        bPeakReached = false;
        startTime = Time.time;

        //rootObject = transform.parent.gameObject;
        //startPosition = rootObject.transform.position;
        //endPosition = new Vector3(startPosition.x, startPosition.y + peakHeight, startPosition.z);

        journeyLength = Vector3.Distance(startPosition, endPosition);
    }

    // Update is called once per frame
    void Update()
    {
        //FloatUp();
        //FloatToPlayer();
    }


    void FloatUp()
    {
        if (Vector3.Distance(rootObject.transform.position, endPosition) < 0.15f)
        {
            bPeakReached = true;
        }
        if (!bPeakReached)
        {
            float distCovered = (Time.time - startTime) * riseSpeed;
            float fracJourney = distCovered / journeyLength;

            rootObject.transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
        }
    }

    void FloatToPlayer()
    {
        startPosition = rootObject.transform.position;

        float distCovered = (Time.time - startTime) * chaseSpeed;
        float distToPlayer = Vector3.Distance(startPosition, PlayerManager.Instance.GetPlayer().transform.position);
        float fracJourney = distCovered / distToPlayer;

        rootObject.transform.position = Vector3.Lerp(startPosition, PlayerManager.Instance.GetPlayer().transform.position, fracJourney);
    }
}
