﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollectStateBehaviour : StateMachineBehaviour
{
    private CoinEffect coinEffect;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        coinEffect = animator.GetComponent<CoinEffect>();

        PlayerManager.Instance.GetPlayer().GetComponent<PlayerCurrency>().IncCount(coinEffect.value);
        HUDManager.Instance.WiggleCoin();
        Destroy(animator.gameObject);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //} 
}
