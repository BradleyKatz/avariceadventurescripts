﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRisingStateBehavior : StateMachineBehaviour
{
    private Vector3 startPosition;
    private Vector3 endPosition;
    private GameObject rootObject;

    private float startTime;
    private float journeyLength;

    private CoinEffect coinEffect;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        coinEffect = animator.GetComponent<CoinEffect>();

        startTime = Time.time;
        rootObject = animator.gameObject;
        startPosition = rootObject.transform.position;
        endPosition = new Vector3(startPosition.x, startPosition.y + coinEffect.peakHeight, startPosition.z);

        journeyLength = Vector3.Distance(startPosition, endPosition);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(rootObject.transform.position, endPosition) < 0.15f)
        {
            animator.SetTrigger("Chase");
        }

        float distCovered = (Time.time - startTime) * coinEffect.riseSpeed;
        float fracJourney = distCovered / journeyLength;

        rootObject.transform.position = Vector3.Lerp(startPosition, endPosition, fracJourney);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}
}
