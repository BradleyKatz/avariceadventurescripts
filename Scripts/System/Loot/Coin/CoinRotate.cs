﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// Rotates the coin while it is active.
/// </summary>

[RequireComponent(typeof(CurrencyPickup))]
public class CoinRotate : MonoBehaviour
{
    public float rotateSpeed;

    private void Update()
    {
        transform.Rotate(0,0 , rotateSpeed * Time.deltaTime);
    }
}