﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script is attached to the player, 
/// and is used to store the current number
/// of coins collected, and display it to the HUD.
/// </summary>

public class PlayerCurrency : MonoBehaviour
{
    private HUDManager manager;
    private Text uiCount;
    private int currentCoins;

    private void Start()
    {
        manager = HUDManager.Instance;
        uiCount = manager.currencyText;
        currentCoins = 0;
    }

    private void Update()
    {
        if(currentCoins < 0)
        {
            currentCoins = 0;
        }

        if(uiCount != null)
        {
            uiCount.text = currentCoins.ToString();
        }
    }

    public void IncCount(int amount)
    {
        currentCoins += amount;
    }

    public void DecCount(int amount)
    {
        currentCoins -= amount;
    }
}
