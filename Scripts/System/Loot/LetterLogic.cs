﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterLogic : MonoBehaviour
{
    public void ResetWiggle()
    {
        this.GetComponent<Animator>().SetBool("LetterCollected", false);
    }
}
