﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script is attached to letter pickups
/// that are picked up by the player,
/// which trigger a UI animation.
/// </summary>

public class LetterPickup : PickupController
{
    public string letterName;

    private HUDManager manager;

    public override void OnPickupGrabbed(Player player)
    {
        if (player != null)
        {
            AudioManager.Instance.PlaySoundEffect(onPickupSound.name);
            manager.ActivateLetter(letterName);
            Destroy(this.gameObject);
        }
    }

    protected override void Start()
    {
        base.Start();
        manager = HUDManager.Instance;
        transform.Rotate(0, 0, 40.0f * Time.deltaTime);
    }
}
