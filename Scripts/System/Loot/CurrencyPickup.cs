﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script is attached to coins
/// that are picked up by the player,
/// in order to increment the coin count.
/// </summary>

public class CurrencyPickup : PickupController
{
    private Animator anim;
    private bool soundEffectPlayed = false;

    protected override void Start()
    {
        base.Start();
        anim = GetComponent<Animator>();
        soundEffectPlayed = false;
    }

    public override void OnPickupGrabbed(Player player)
    {
        anim.SetTrigger("Rise");

        if (!soundEffectPlayed)
        {
            AudioManager.Instance.PlaySoundEffect(onPickupSound.name);
            soundEffectPlayed = true;
        }
    }
}