﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Used by the EnemyWaveSpawner script to give designers more control over how many 
/// enemies can be spawned and the number of spawnpoints each type of enemy has
/// </summary>

[System.Serializable]
public class EnemyWave
{
    public string waveName;
    public int numberOfScarabs;
    [Tooltip("The vectors in this list are added to the gameobjects position")]
    public List<Vector3> scarabSpawnPoints = new List<Vector3>();

    public int numberOfBrawlers;
    [Tooltip("The vectors in this list are added to the gameobjects position")]
    public List<Vector3> brawlerSpawnPoints = new List<Vector3>();

    public int numberOfWizards;
    [Tooltip("The vectors in this list are added to the gameobjects position")]
    public List<Vector3> wizardSpawnPoints = new List<Vector3>();
}
