﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eEnemyType
{
    Empty,
    Scarab,
    Mummy,
    Wizard
};