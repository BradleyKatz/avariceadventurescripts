﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Data container for saving the player's data
/// </summary>
[System.Serializable]
public class PlayerData
{
    public int level;
    public int health;
    public float[] position;

    public string currentScene;

    public PlayerData(Player player)
    {
        health = player.playerHealth.health;

        position = new float[3];
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;    
    }
}
