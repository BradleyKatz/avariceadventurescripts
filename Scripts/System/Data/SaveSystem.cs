﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Handles the saving and loading of data to a json file
/// </summary>
public static class SaveSystem
{
    public static void SavePlayer(Player player)
    {
        string path = Application.persistentDataPath + "/PlayerSave.json";
        PlayerData data = new PlayerData(player);
        string json = JsonUtility.ToJson(data);
        File.WriteAllText(path, json);
  
        Debug.Log(json);
        Debug.Log("Saved Game to " + path);
    }

    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/PlayerSave.json";
        if (File.Exists(path))
        {
            string dataAsJson = File.ReadAllText(path);
            PlayerData loadedData = JsonUtility.FromJson<PlayerData>(dataAsJson);

            Debug.Log("Loaded Game from " + path);

            return loadedData;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
