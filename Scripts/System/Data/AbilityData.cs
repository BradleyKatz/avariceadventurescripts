﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Was to be used for saving ability data 
/// </summary>
[System.Serializable]
public class AbilityData
{
    public string abilityName;
    public int damage;

    public AbilityData(string _abilityName, int _damage)
    {
        abilityName = _abilityName;
        damage = _damage;
    }
}
