﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * Based off of Sebastian Lague's 2D Character Controller tutorial, this script lies at the very core of our game by providing a means for shooting a series of equally spaced raycasts along the width and height of a character's collider.
 */

[RequireComponent(typeof(BoxCollider2D))]
public class RaycastController : MonoBehaviour
{
    [HideInInspector]
    public struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    private const float distanceBetweenRays = 0.12f;

    [Tooltip("The physics layer(s) that will make up walkbable surfaces")]
    public LayerMask groundCollisionMask;

    public float skinWidth = 0.015f;
    protected int horizontalRayCount = 4;
    protected int verticalRayCount = 4;

    protected float horizontalRaySpacing;
    protected float verticalRaySpacing;

    [HideInInspector] public BoxCollider2D box2D;
    [HideInInspector] public RaycastOrigins raycastOrigins;

    protected virtual void Start()
    {
        box2D = GetComponent<BoxCollider2D>();
        CalculateRaySpacing();
    }

    protected virtual void Update()
    {
        if (box2D == null)
        {
           // box2D = GetComponent<BoxCollider2D>();
        }
    }

    #region Raycast Setup Methods
    protected void CalculateRaySpacing()
    {
        Bounds bounds = box2D.bounds;
        bounds.Expand(skinWidth * -2.0f);

        float boundsWidth = bounds.size.x;
        float boundsHeight = bounds.size.y;

        horizontalRayCount = Mathf.RoundToInt(boundsHeight / distanceBetweenRays) * 2;
        verticalRayCount = Mathf.RoundToInt(boundsWidth / distanceBetweenRays) * 2;

        horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
        verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
    }

    protected void UpdateRaycastOrigins()
    {
        Bounds bounds = box2D.bounds;
        bounds.Expand(skinWidth * -2.0f);

        raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
    }
    #endregion
}
