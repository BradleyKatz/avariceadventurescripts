﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * Based off of Sebastian Lague's 2D platformer character controller tutorial, this script uses a custom raycasting system to smoothly handle the movement and slope handling of entities in our game.
 * Additionally, I've also added quality of life improvements such as public properties to determine in other scripts if an entity is grounded or not, platform fallthrough, character model rotation
 * handling, and physics exceptions (for rolling through vines when iframes are active, for example).
 * 
 */

public class AvariceCC : RaycastController
{
    #region Helper Structs
    [System.Serializable]
    protected struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;

        public bool isClimbingSlope, isDescendingSlope;
        public float slopeAngle, prevSlopeAngle;
        public Vector3 prevVelocity;

        public BoxCollider2D hitAbove, hitBelow, hitLeft, hitRight;

        public void Reset()
        {
            above = below = left = right = false;
            isClimbingSlope = false;
            isDescendingSlope = false;

            prevSlopeAngle = slopeAngle;
            slopeAngle = 0.0f;

            hitAbove = hitBelow = hitLeft = hitRight = null;
        }
    }
    #endregion Helper Structs
    [SerializeField] protected CollisionInfo collisions;
    protected CollisionInfo prevCollisions;

    public bool IsGrounded
    {
        get
        {
            return collisions.below;
        }
        set
        {
            collisions.below = value;
            if (!collisions.below)
            {
                currentRidingPlatform?.RemovePassenger(this);
                currentRidingPlatform = null;
            }
        }
    }

    public bool IsCeilingHit
    {
        get
        {
            return collisions.above;
        }
        set
        {
            collisions.above = value;
        }
    }

    public bool IsWallHit
    {
        get
        {
            return collisions.left || collisions.right;
        }
    }

    public bool IsClimbingSlope
    {
        get
        {
            return collisions.isClimbingSlope;
        }
    }

    public bool IsDescendingSlope
    {
        get
        {
            return collisions.isDescendingSlope;
        }
    }

    public BoxCollider2D HitColliderAbove
    {
        get
        {
            return collisions.hitAbove;
        }
    }

    public BoxCollider2D HitColliderBelow
    {
        get
        {
            return collisions.hitBelow;
        }
    }

    public BoxCollider2D HitColliderLeft
    {
        get
        {
            return collisions.hitLeft;
        }
    }

    public BoxCollider2D HitColliderRight
    {
        get
        {
            return collisions.hitRight;
        }
    }

    public float maxSlopeClimbAngle = 80.0f; // The steepest angle the controller can climb
    public float maxSlopeDescendAngle = 75.0f;

    [Tooltip("If enabled, the entity this Character Controller is attached to can pass through passable platforms")]
    public bool passThroughPlatforms = false;

    private AvariceEntity attachedEntity = null;
    //private Transform entityModelTransform = null;
    //private Vector3 originalModelRotation = Vector3.zero;
    private GameObject prevPassingGO = null;
    [HideInInspector] public PlatformController currentRidingPlatform = null;
    [HideInInspector] public bool isIgnoringPlatformMovement = false;

    protected override void Start()
    {
        base.Start();
        attachedEntity = GetComponent<AvariceEntity>();

        // Assumes the first child of the root transform is the character model (if it exists)
        //if (transform.childCount > 0)
        //{
        //    entityModelTransform = transform.GetChild(0);
        //    if (entityModelTransform)
        //    {
        //        originalModelRotation = entityModelTransform.localEulerAngles;
        //    }
        //}
    }

    #region Public Methods
    public void Move(Vector3 velocity, bool isStandingOnPlatform = false)
    {
        if (velocity.y < 0.0f && IsGrounded)
        {
            DescendSlope(ref velocity);
        }
        else if (velocity.y > 0.0f)
        {
            //if (entityModelTransform != null)
            //{
            //    entityModelTransform.localEulerAngles = originalModelRotation;
            //}
        }

        UpdateRaycastOrigins();
        prevCollisions = collisions;
        collisions.Reset();
        collisions.prevVelocity = velocity;

        if (velocity.x != 0.0f)
        {
            CheckHorizontalCollisions(ref velocity);
        }

        if (velocity.y != 0.0f)
        {
            CheckVerticalCollisions(ref velocity);
        }

        if (isStandingOnPlatform)
        {
            collisions.below = true;

            //if (entityModelTransform != null)
            //{
            //    entityModelTransform.localEulerAngles = originalModelRotation;
            //}
        }

        transform.Translate(velocity, Space.World);
    }
    #endregion

    #region Collision Detection
    private void CheckHorizontalCollisions(ref Vector3 velocity)
    {
        float xDirection = Mathf.Sign(velocity.x);
        float yDirection = Mathf.Sign(velocity.y);

        float rayLength = Mathf.Abs(velocity.x) + skinWidth;

        for (int i = 0; i < horizontalRayCount; ++i)
        {
            // Flip the raycast origin depending on if we are travelling up or down
            Vector2 rayOrigin = (xDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;

            // Horizontal rays should be stacked on top of each other, hence the usage of Vector2.up
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * xDirection, rayLength, groundCollisionMask);
            if (hit.transform != null)
            {
                if (hit.distance == 0) { continue; }

                PlatformController platform = hit.transform.GetComponent<PlatformController>();
                if (platform != null && yDirection != 0.0f)
                {
                    continue;
                }

                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (i == 0 && slopeAngle <= maxSlopeClimbAngle)
                {
                    if (collisions.isDescendingSlope)
                    {
                        collisions.isDescendingSlope = false;
                        velocity = collisions.prevVelocity;
                    }

                    float distanceToSlope = 0.0f;
                    if (slopeAngle != collisions.prevSlopeAngle)
                    {
                        // Snap the character's position to be closer to the slope.
                        distanceToSlope = hit.distance - skinWidth;
                        velocity.x -= distanceToSlope * xDirection;
                    }

                    ClimbSlope(ref velocity, slopeAngle);
                    velocity.x += distanceToSlope * xDirection;
                }

                if (!collisions.isClimbingSlope || slopeAngle > maxSlopeClimbAngle)
                {
                    velocity.x = (hit.distance - skinWidth) * xDirection;
                    rayLength = hit.distance;

                    if (collisions.isClimbingSlope)
                    {
                        velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad * Mathf.Abs(velocity.x));
                    }

                    collisions.left = (xDirection == -1);
                    collisions.right = (xDirection == 1);

                    if (collisions.left) { collisions.hitLeft = hit.collider as BoxCollider2D; }
                    if (collisions.right) { collisions.hitRight = hit.collider as BoxCollider2D; }
                }
            }

            Debug.DrawRay(rayOrigin, Vector2.right * xDirection, Color.red);
        }
    }

    private void CheckVerticalCollisions(ref Vector3 velocity)
    {
        float yDirection = Mathf.Sign(velocity.y);

        float rayLength = Mathf.Abs(velocity.y) + skinWidth;
        if (yDirection == -1.0f) { rayLength *= 3.0f; }

        for (int i = 0; i < verticalRayCount; ++i)
        {
            // Constantly raycast downwards in order to always check for platform related collisions.
            Vector2 rayOriginDown = raycastOrigins.bottomLeft + (Vector2.right * (verticalRaySpacing * i + velocity.x));
            RaycastHit2D hitBelow = Physics2D.Raycast(rayOriginDown, Vector2.down, rayLength, groundCollisionMask);
            if (hitBelow.transform != null)
            {
                PlatformController platform = hitBelow.transform.GetComponent<PlatformController>();
                if (platform != null && attachedEntity.isFallingThroughPlatform && prevPassingGO == null)
                {
                    attachedEntity.velocity.y = -5.0f;
                    prevPassingGO = platform.gameObject;
                    IsGrounded = false;
                    return;
                }
                else if (platform != null && prevPassingGO != null && prevPassingGO != hitBelow.transform.gameObject)
                {
                    velocity.y = -(hitBelow.distance - skinWidth - platform.velocity.y);

                    collisions.below = (yDirection == -1);
                    collisions.above = (yDirection == 1);

                    if (collisions.above) { collisions.hitAbove = hitBelow.collider as BoxCollider2D; }
                    if (collisions.below) { collisions.hitBelow = hitBelow.collider as BoxCollider2D; }

                    attachedEntity.isFallingThroughPlatform = false;
                    prevPassingGO = null;
                }
                else if (platform == null && IsGrounded)
                {
                    attachedEntity.isFallingThroughPlatform = false;
                    prevPassingGO = null;
                }
            }

            Vector2 rayOrigin = (yDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * yDirection, rayLength, groundCollisionMask);
            if (hit.transform != null)
            { 
                // If the character is beneath a passable platform and they are currently rising,
                // ignore the collision detection and let them pass 
                PlatformController platform = hit.transform.GetComponent<PlatformController>();
                if (platform != null && platform.isPassableFromBelow && Mathf.Sign(velocity.y) >= 0.0f)
                {
                    //if (platform.transform.TransformPoint(platform.box2D.bounds.min).y > transform.TransformPoint(box2D.bounds.max).y)
                    //{
                    //    continue;
                    //}
                    //if (platform.transform.TransformPoint(platform.transform.position).y > transform.TransformPoint(transform.position).y)
                    //{
                    //    continue;
                    //}

                    if (platform.box2D.bounds.min.y > box2D.bounds.max.y)
                    {
                        continue;
                    }
                }
                else
                {
                    if (prevPassingGO != null && prevPassingGO == hit.transform.gameObject)
                    {
                        continue;
                    }

                    velocity.y = (hit.distance - skinWidth) * yDirection;

                    // Basically, if one of our rays detects a platform above the ground, we want to consider
                    // all further raycasts relative to that platform, otherwise, we risk falling through the platform
                    // due to another raycast detecting the ground.
                    rayLength = hit.distance;

                    if (collisions.isClimbingSlope)
                    {
                        velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
                    }

                    collisions.below = (yDirection == -1);
                    collisions.above = (yDirection == 1);

                    if (collisions.above) { collisions.hitAbove = hit.collider as BoxCollider2D; }
                    if (collisions.below) { collisions.hitBelow = hit.collider as BoxCollider2D; }

                    //attachedEntity.isFallingThroughPlatform = false;
                    //prevPassingGO = null;
                }
            }

            Debug.DrawRay(rayOrigin, Vector2.up * yDirection, Color.red);
        }

        if (collisions.isClimbingSlope)
        {
            float xDirection = Mathf.Sign(velocity.x);
            rayLength = Mathf.Abs(velocity.x) + skinWidth;
            Vector2 rayOrigin = ((xDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * velocity.y;

            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * xDirection, rayLength, groundCollisionMask);
            if (hit.transform != null)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle != collisions.slopeAngle)
                {
                    // We've collided with a new slope
                    velocity.x = (hit.distance - skinWidth) * xDirection;
                    collisions.slopeAngle = slopeAngle;
                }
            }
        }
    }
    #endregion

    #region Slope Handlers
    private void ClimbSlope(ref Vector3 velocity, float slopeAngle)
    {
        float moveDistance = Mathf.Abs(velocity.x);
        float climbVelocity = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

        // If the current velocity is greater than climbVelocity, we're jumping on the slope.
        if (velocity.y <= climbVelocity)
        {
            // Otherwise do the slope calculations as usual.
            velocity.y = climbVelocity;
            velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);

            // If we're climbing a slope, we can generally assume the player is grounded even if they aren't
            // 100% grounded by Unity's standards (maybe replace this with an isGrounded bool later?)
            collisions.below = true;
            collisions.isClimbingSlope = true;
            collisions.slopeAngle = slopeAngle;

            // Rotate the character model to be in line with the slope's angle
            //if (entityModelTransform != null)
            //{
            //    entityModelTransform.localEulerAngles = new Vector3(slopeAngle * -1.0f, originalModelRotation.y, originalModelRotation.z);
            //}
        }
    }

    private void DescendSlope(ref Vector3 velocity)
    {
        float xDirection = Mathf.Sign(velocity.x);
        Vector2 rayOrigin = (xDirection == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;

        RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, Mathf.Infinity, groundCollisionMask);
        if (hit.transform != null)
        {
            float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
            if (slopeAngle != 0.0 && slopeAngle <= maxSlopeDescendAngle)
            {
                // Rotate the character model to be in line with the slope's angle
                //if (entityModelTransform != null)
                //{
                //    entityModelTransform.localEulerAngles = new Vector3(slopeAngle * -Mathf.Sign(transform.right.x) * Mathf.Sign(hit.transform.localRotation.z) , originalModelRotation.y, originalModelRotation.z);
                //}

                if (Mathf.Sign(hit.normal.x) == xDirection)
                {
                    // Are we close enough to the slope for this to actually take effect?
                    if (hit.distance - skinWidth <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x))
                    {
                        float moveDistance = Mathf.Abs(velocity.x);
                        float descendVelocity = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

                        velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
                        velocity.y -= descendVelocity;

                        collisions.slopeAngle = slopeAngle;
                        collisions.isDescendingSlope = true;
                        collisions.below = true;
                    }
                }
            }
        }
    }
    #endregion
}
