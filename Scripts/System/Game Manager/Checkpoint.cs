﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Sets the player's spawn point to the current position of the checkpoint
/// </summary>
public class Checkpoint : AvariceObject
{
    public float zPosition = 0;
    public Vector3 spawnRotation;

    public ParticleSystem particleEffect;
    public Sound soundEffect;

    protected override void Start()
    {
        base.Start();

        particleEffect.Stop();
        transform.position = new Vector3(transform.position.x, transform.position.y, zPosition);
        transform.rotation = Quaternion.Euler(spawnRotation);

        if (soundEffect.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(soundEffect);
        }
    }

    protected override void TriggerEnter(Collider2D other)
    {
        base.TriggerEnter(other);

        AudioManager.Instance.PlaySoundEffect(soundEffect.name);
        particleEffect.Play();
        PlayerManager.Instance.spawnPoint = transform.position;
    }
}
