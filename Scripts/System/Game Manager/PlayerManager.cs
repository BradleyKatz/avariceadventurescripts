﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Manages the player's spawning and keeping track of the scene they came from
/// </summary>
public class PlayerManager : Singleton<PlayerManager>
{
    [SerializeField] private GameObject gameCameraRoot = null;
    [SerializeField] private GameObject Player;
    [SerializeField] private float initialSpawnDelay;

    [HideInInspector] public Vector3 spawnPoint;
    [HideInInspector] public Vector3 spawnRotation;
    [HideInInspector] public string prevScene = "";
    [HideInInspector] public Vector3 returnPoint;
    [HideInInspector] public bool bSpawn = false;

    private float timer;
    private GameObject PlayerInstance;
    private bool firstStart = true;


    private void Update()
    {
        if(bSpawn)
        {
            SpawnPlayer();
            bSpawn = false;
        }
    }

    public GameObject GetPlayer()
    {
        return PlayerInstance;
    }

    // TODO: Fix the delay not working as intended
    public void SpawnPlayer()
    {       
        if (PlayerInstance == null && SceneManager.GetActiveScene().buildIndex > -1)
        {
            if (gameCameraRoot)
            {
                gameCameraRoot.SetActive(true);
                for (int i = 0; i < gameCameraRoot.transform.childCount; ++i)
                {
                    GameObject childGO = gameCameraRoot.transform.GetChild(i)?.gameObject;
                    if (childGO)
                    {
                        childGO.SetActive(true);
                    }
                }
            }

            if (firstStart)
            {
                while (timer < initialSpawnDelay)
                {
                    timer += Time.deltaTime;
                }
                PlayerInstance = (GameObject)Instantiate(Player, spawnPoint, Quaternion.Euler(spawnRotation));
                firstStart = false;
            }
            else
            {
                PlayerInstance = (GameObject)Instantiate(Player, spawnPoint, Quaternion.Euler(spawnRotation));
            }
        }
        else if (PlayerInstance != null)
        {
            PlayerInstance.transform.position = spawnPoint;
        }

        CameraController.Instance.ResetCameraPosition();
    }
}
