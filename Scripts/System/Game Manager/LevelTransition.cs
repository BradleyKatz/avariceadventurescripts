﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Loads and unloads a level when you are within a trigger and provide the correct input.
/// Also controls the button prompt to display the correct input
/// </summary>
public class LevelTransition : AvariceObject
{
    public GameObject buttonPrompt;
    public string promptInput;
    public SceneReference NextLevel;

    private bool levelLoaded;

    private void Awake()
    {
        levelLoaded = false;
        buttonPrompt.SetActive(false);
    }

    protected override void TriggerEnter(Collider2D other)
    {
        base.TriggerEnter(other);

        buttonPrompt.SetActive(true);
    }

    protected override void TriggerStay(Collider2D other)
    {
        base.TriggerStay(other);

        if (!levelLoaded && InputHandler.GetAxis(promptInput) > 0.8f)
        {
            if (Path.GetFileNameWithoutExtension(NextLevel.ScenePath) == PlayerManager.Instance.prevScene)
            {
                PlayerManager.Instance.spawnPoint = PlayerManager.Instance.returnPoint;
                SceneLoader.Instance.LoadLevel(NextLevel, true);
                SceneLoader.Instance.UnLoadCurrentScene(false);
            }
            else
            {
                PlayerManager.Instance.prevScene = SceneLoader.Instance.getCurrentLoadedScene();
                PlayerManager.Instance.returnPoint = transform.position;
                SceneLoader.Instance.LoadLevel(NextLevel, true);
                SceneLoader.Instance.UnLoadCurrentScene(false);
            }
            levelLoaded = true;
        }
    }

    protected override void TriggerExit(Collider2D other)
    {
        base.TriggerExit(other);

        buttonPrompt.SetActive(false);
    }
}
