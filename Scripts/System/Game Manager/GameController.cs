﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Original Author: Caleb Greer
 * Modified by: Kevin Newbury
 */

/// <summary>
/// Handles the transitioning between the Menus and starting the game
/// Pauses the game when the correct input is pressed
/// Calls saving and loading when the buttons are pressed
/// 
/// Kevin: Added deathMenu and endGameMenu menu logic.
/// </summary>
public class GameController : Singleton<GameController>
{
    [SerializeField] private SceneReference GameScene;
    [SerializeField] private SceneReference titleScene;
    [SerializeField] private MenuClassifier mainMenu;
    [SerializeField] private MenuClassifier HUD;
    [SerializeField] private MenuClassifier pauseMenu;
    [SerializeField] private MenuClassifier areYouSureMenu;
    [SerializeField] private MenuClassifier controlsMenu;
    [SerializeField] private MenuClassifier settingsMenu;
    [SerializeField] private MenuClassifier deathScreen;
    [SerializeField] private MenuClassifier endGameMenu;

    private Scene _scene;
    public static bool GameIsPaused;

    [HideInInspector] public bool allowStart;

    private Player player;

    private void Start()
    {
        _scene = SceneManager.GetSceneByName(GameScene);
        GameIsPaused = false;
        allowStart = true;

        SceneLoader.Instance.LoadLevel(titleScene, false);
    }

    private void Update()
    {
        if (InputHandler.GetButtonDown("Start") && !allowStart)
        {
            PauseGame();
        }
    }

    public void Death()
    {
        MenuManager.Instance.showMenu(deathScreen);
    }

    public void Respawn()
    {
        MenuManager.Instance.hideMenu(deathScreen);
    }

    public void PlayGame()
    {
        if (allowStart)
        {
            if (_scene.isLoaded == false)
            {
                Debug.Log("Loading Game Scene: " + GameScene);
                SceneLoader.Instance.LoadLevel(GameScene, true, "Title");
            }

            MenuManager.Instance.showMenu(HUD);
            MenuManager.Instance.hideMenu(mainMenu);
            GameIsPaused = false;
            allowStart = false;
        }

    }

    public void DebugStartGame()
    {
        if (allowStart)
        {
            MenuManager.Instance.showMenu(HUD);
            MenuManager.Instance.hideMenu(mainMenu);
            GameIsPaused = false;
            allowStart = false;
        }
    }

    public void RestartGame()
    {
        allowStart = false;
        SceneLoader.Instance.UnLoadCurrentScene(true);
        PauseGame();
        PlayGame();
    }

    public void PauseGame()
    {
        GameIsPaused = !GameIsPaused;

        if (GameIsPaused)
        {
            MenuManager.Instance.showMenu(pauseMenu);
            Time.timeScale = 0;
        }
        else
        {
            MenuManager.Instance.hideMenu(pauseMenu);
            MenuManager.Instance.hideMenu(controlsMenu);
            MenuManager.Instance.hideMenu(settingsMenu);
            Time.timeScale = 1;
        }
    }

    public void ShowAreYouSureMenu()
    {
        MenuManager.Instance.hideMenu(pauseMenu);
        MenuManager.Instance.showMenu(areYouSureMenu);
    }

    public void HideAreYouSureMenu()
    {
        MenuManager.Instance.hideMenu(areYouSureMenu);
        MenuManager.Instance.showMenu(pauseMenu);
    }

    public void ShowControlsMenu()
    {
        MenuManager.Instance.hideMenu(pauseMenu);
        MenuManager.Instance.showMenu(controlsMenu);
    }

    public void HideControlsMenu()
    {
        MenuManager.Instance.hideMenu(controlsMenu);
        MenuManager.Instance.showMenu(pauseMenu);
    }

    public void ShowSettingsMenu()
    {
        MenuManager.Instance.hideMenu(pauseMenu);
        MenuManager.Instance.showMenu(settingsMenu);
    }

    public void HideSettingsMenu()
    {
        MenuManager.Instance.hideMenu(settingsMenu);
        MenuManager.Instance.showMenu(pauseMenu);
    }

    public void ReturnToTitle()
    {
        PauseGame();

        MenuManager.Instance.hideMenu(HUD);
        MenuManager.Instance.hideMenu(areYouSureMenu);
        HUDManager.Instance.ResetLetters();
        SpawnedGameObjectManager.Instance.DestroyObjects();

        Debug.Log("Unloading Game Scene: " + GameScene);

        GameObject cameraGO = GameObject.Find("Camera 1");
        if (cameraGO)
        {
            for (int i = 0; i < cameraGO.transform.childCount; ++i)
            {
                GameObject childGO = cameraGO.transform.GetChild(i)?.gameObject;
                if (childGO)
                {
                    childGO.SetActive(false);
                }
            }

            cameraGO.SetActive(false);
        }

        SceneLoader.Instance.UnLoadCurrentScene(true);
        SceneLoader.Instance.LoadLevel("Title", false);

        Destroy(PlayerManager.Instance.GetPlayer());
        PlayerManager.Instance.prevScene = "";

        MenuManager.Instance.showMenu(mainMenu);
        allowStart = true;
    }

    //Delete this later
    public void ReturnToTitleEndGame()
    {
        Time.timeScale = 1.0f;
        MenuManager.Instance.hideMenu(HUD);
        MenuManager.Instance.hideMenu(endGameMenu);
        HUDManager.Instance.ResetLetters();

        Debug.Log("Unloading Game Scene: " + GameScene);

        GameObject cameraGO = GameObject.Find("Camera 1");
        if (cameraGO)
        {
            for (int i = 0; i < cameraGO.transform.childCount; ++i)
            {
                GameObject childGO = cameraGO.transform.GetChild(i)?.gameObject;
                if (childGO)
                {
                    childGO.SetActive(false);
                }
            }

            cameraGO.SetActive(false);
        }

        SceneLoader.Instance.UnLoadCurrentScene(true);
        SceneLoader.Instance.LoadLevel("Title", false);

        Destroy(PlayerManager.Instance.GetPlayer());
        PlayerManager.Instance.prevScene = "";

        MenuManager.Instance.showMenu(mainMenu);
        allowStart = true;
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    public void SavePlayer()
    {
        if (player == null)
        {
            player = FindObjectOfType<Player>();
        }
        SaveSystem.SavePlayer(player);
    }

    public void LoadPlayer()
    {
        if (player == null)
        {
            player = PlayerManager.Instance.GetPlayer().GetComponent<Player>();
        }

        PlayerData data = SaveSystem.LoadPlayer();

        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        player.transform.position = position;
    }
}
