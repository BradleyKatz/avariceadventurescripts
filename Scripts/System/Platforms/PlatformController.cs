﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * Based off of Sebastian Lague's 2D Character Controller tutorial, this script models everything related to moving platforms in our game such as:
 * - Setting a series of waypoints for the platform to follow.
 * - Smoothing between the platform's waypoints.
 * - Carrying other game entities along the surface of the platform as it is moving.
 * 
 * We've also augmented this class to support being able to pass through the platform from below.
 */

public class PlatformController : RaycastController
{
    #region Helper Structs
    protected struct PassengerMovementInfo
    {
        public Transform passengerTransform;
        public Vector3 desiredVelocity;
        public bool isOnPlatform;
        public bool moveBeforePlatform;

        public PassengerMovementInfo(Transform _transform, Vector3 _velocity, bool _isOnPlatform, bool _moveBeforePlatform)
        {
            passengerTransform = _transform;
            desiredVelocity = _velocity;
            isOnPlatform = _isOnPlatform;
            moveBeforePlatform = _moveBeforePlatform;
        }
    }
    #endregion

    [HideInInspector] public Vector3 velocity = Vector3.zero;

    public LayerMask passengerMask;
    [Range(0.0f, 2.0f)]
    //public float easingAmount = 1.0f;
    private float easingAmount = 0.5f;
    public List<Vector3> localWaypoints = new List<Vector3>();
    public bool isPassableFromBelow = false;
    public bool movePassengers = false;
    public bool activated = true;

    private int fromWaypointIndex = 0; // Index of the waypoint we're currently moving away from
    private float normalizedWaypointDistance = 0.0f; // Percentage (between 0 and 1) that we've moved from waypoint to waypoint
    private List<Vector3> worldWaypoints = new List<Vector3>();

    private float moveSpeed = 4.0f;
    private List<PassengerMovementInfo> pendingMovements = new List<PassengerMovementInfo>();
    private Dictionary<Transform, AvariceCC> passengerDictionary = new Dictionary<Transform, AvariceCC>();

    public void RemovePassenger(AvariceCC passengerCC)
    {
        if (passengerDictionary.ContainsKey(passengerCC.transform))
        {
            passengerDictionary.Remove(passengerCC.transform);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        float size = 0.3f;

        for (int i = 0; i < localWaypoints.Count; ++i)
        {
            Vector3 waypointWorldPos = (Application.isPlaying) ? worldWaypoints[i] : localWaypoints[i] + transform.position;
            Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
            Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
        }
    }

    protected override void Start()
    {
        base.Start();
        moveSpeed = 5.0f;

        foreach (Vector3 localWaypointPos in localWaypoints)
        {
            worldWaypoints.Add(localWaypointPos + transform.position);
        }
    }

    protected void FixedUpdate()
    {
        if (GameController.GameIsPaused) { return; } 

        UpdateRaycastOrigins();

        if (localWaypoints.Count > 0)
        {
            velocity = CalculatePlatformMovement();

            if (movePassengers)
            {
                CalculatePassengerPositions(velocity);
            }

            if (pendingMovements.Count > 0)
            {
                activated = true;
            }

            MovePassengers(true);
            transform.Translate(velocity, Space.World);
            MovePassengers(false);
        }
    }

    private float ApplyEasing(float x)
    {
        float a = easingAmount + 1.0f;
        return Mathf.Pow(x, a) / (Mathf.Pow(x, a) + (Mathf.Pow(1.0f - x, a)));
    }

    private Vector3 CalculatePlatformMovement()
    {
        if (!activated) { return Vector3.zero; }

        int toWaypointIndex = fromWaypointIndex + 1;
        float distanceBetweenWaypoints = Vector3.Distance(worldWaypoints[fromWaypointIndex], worldWaypoints[toWaypointIndex]);
        normalizedWaypointDistance += Time.deltaTime * moveSpeed / distanceBetweenWaypoints;
        normalizedWaypointDistance = Mathf.Clamp01(normalizedWaypointDistance);

        Vector3 newPos = Vector3.Lerp(worldWaypoints[fromWaypointIndex], worldWaypoints[toWaypointIndex], ApplyEasing(normalizedWaypointDistance));

        if (normalizedWaypointDistance >= 1.0f)
        {
            normalizedWaypointDistance = 0.0f;
            if (++fromWaypointIndex >= worldWaypoints.Count - 1)
            {
                fromWaypointIndex = 0;
                worldWaypoints.Reverse();
            }
        }

        return newPos - transform.position;
    }

    private void MovePassengers(bool beforeMovePlatform)
    {
        foreach (PassengerMovementInfo moveInfo in pendingMovements)
        {
            if (!passengerDictionary.ContainsKey(moveInfo.passengerTransform))
            {
                AvariceCC passengerCC = moveInfo.passengerTransform.GetComponent<AvariceCC>();
                if (passengerCC != null && !passengerCC.isIgnoringPlatformMovement)
                {
                    passengerDictionary.Add(moveInfo.passengerTransform, passengerCC);
                    passengerCC.currentRidingPlatform = this;
                }
            }

            if (moveInfo.moveBeforePlatform == beforeMovePlatform)
            {
                AvariceCC passengerCC;
                if (passengerDictionary.TryGetValue(moveInfo.passengerTransform, out passengerCC))
                {
                    // Only apply the platform x-velocity to the entity when they aren't moving or running with the platform.
                    // Doing so fixes the tradmill effect from running against the platform
                    AvariceEntity avariceEntity = passengerCC.GetComponent<AvariceEntity>();
                    if (avariceEntity.moveAxis.x == 0.0f || Mathf.Sign(avariceEntity.velocity.x) == Mathf.Sign(velocity.x))
                    {
                        passengerCC.Move(moveInfo.desiredVelocity, moveInfo.isOnPlatform);
                    }
                    else
                    {
                        passengerCC.Move(new Vector3(0.0f, moveInfo.desiredVelocity.y, moveInfo.desiredVelocity.z), moveInfo.isOnPlatform);
                    }
                }
            }
        }
    }

    private void CalculatePassengerPositions(Vector3 velocity)
    {
        HashSet<Transform> passengersMovedThisFrame = new HashSet<Transform>();
        pendingMovements = new List<PassengerMovementInfo>();

        float xDirection = Mathf.Sign(velocity.x);
        float yDirection = Mathf.Sign(velocity.y);

        if (velocity.y == 0.0f && velocity.x == 0.0f && !activated)
        {
            // One skinWidth to get to the platform surface, and another to get a ray just above the surface.
            float rayLength = skinWidth * 2.0f;
            for (int i = 0; i < verticalRayCount; ++i)
            {
                Vector2 rayOrigin = raycastOrigins.topLeft + Vector2.right * (verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                if (hit.transform != null && !passengersMovedThisFrame.Contains(hit.transform))
                {
                    float pushX = velocity.x;
                    float pushY = velocity.y;

                    pendingMovements.Add(new PassengerMovementInfo(hit.transform, new Vector3(pushX, pushY), true, true));
                    passengersMovedThisFrame.Add(hit.transform);
                }
            }
        }

        //Vertically moving platform
        if (velocity.y != 0.0f && activated)
        {
            float rayLength = Mathf.Abs(velocity.y) + skinWidth * 2.0f;

            for (int i = 0; i < verticalRayCount; ++i)
            {
                Vector2 rayOrigin = (yDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * yDirection, rayLength, passengerMask);

                // Our system casts multiple rays, which may hit the same target multiple times.
                // This prevents moving the same passenger multiple times.
                if (hit.transform != null && !passengersMovedThisFrame.Contains(hit.transform))
                {
                    float pushX = (yDirection == 1) ? velocity.x : 0.0f;
                    float pushY = velocity.y - (hit.distance - skinWidth) * yDirection;

                    pendingMovements.Add(new PassengerMovementInfo(hit.transform, new Vector3(pushX, pushY), true, yDirection == 1.0f));
                    passengersMovedThisFrame.Add(hit.transform);
                }
            }
        }

        // Horizontally moving platform
        if (velocity.x != 0.0f && activated)
        {
            float rayLength = Mathf.Abs(velocity.x) + skinWidth;

            for (int i = 0; i < horizontalRayCount; ++i)
            {
                Vector2 rayOrigin = (xDirection == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * xDirection, rayLength, passengerMask);

                if (hit.transform != null && !passengersMovedThisFrame.Contains(hit.transform))
                {
                    float pushX = velocity.x - (hit.distance - skinWidth) * xDirection;
                    float pushY = -skinWidth;

                    // In this case, the passenger isn't standing on the platform, they're being pushed from the side.
                    pendingMovements.Add(new PassengerMovementInfo(hit.transform, new Vector3(pushX, pushY), false, true));
                    passengersMovedThisFrame.Add(hit.transform);
                }
            }
        }

        // There's a passenger on top of a horiztonally or downward moving platform, so cast a small ray upwards.
        if (yDirection == -1 || (velocity.y == 0.0f && velocity.x != 0.0f) && activated)
        {
            // One skinWidth to get to the platform surface, and another to get a ray just above the surface.
            float rayLength = skinWidth * 2.0f;

            for (int i = 0; i < verticalRayCount; ++i)
            {
                Vector2 rayOrigin = raycastOrigins.topLeft + Vector2.right * (verticalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                if (hit.transform != null && !passengersMovedThisFrame.Contains(hit.transform))
                {
                    float pushX = velocity.x;
                    float pushY = velocity.y;

                    pendingMovements.Add(new PassengerMovementInfo(hit.transform, new Vector3(pushX, pushY), true, true));
                    passengersMovedThisFrame.Add(hit.transform);
                }
            }
        }


    }
}
