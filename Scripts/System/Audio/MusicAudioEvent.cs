﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Audio Events/Music")]
public class MusicAudioEvent : AudioEvent
{
    public AudioClip clip;

    public float fadeInTime = 0.5f;
    public float fadeOutTime = 2.0f;
    public bool loopMusic;

    [Range(0, 1)]
    public float volume = 0.7f;
    [Range(0.5f, 1.5f)]
    public float pitch = 1f;

    public override void ChangeVolume()
    {
        source.volume = volume * AudioManager.Instance.GetMusicVolume();
    }

    //[Range(0, 0.5f), Tooltip("Set the randomness that the volume can differentiate")]
    //public float randomVolume = 0.1f;
    //[Range(0, 0.5f), Tooltip("Set the randomness that the pitch can differentiate")]
    //public float randomPitch = 0.1f;

    public override void Play()
    {
        source.clip = clip;
        source.volume = volume * AudioManager.Instance.GetMusicVolume(); //* (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
        source.pitch = pitch; //* (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));

        if (loopMusic)
        {
            source.loop = true;
        }

        AudioManager.Instance.AudioFadeIn(source, fadeInTime, source.volume);
    }

    public override void Stop()
    {
        AudioManager.Instance.AudioFadeOut(source, fadeOutTime);
    }

}
