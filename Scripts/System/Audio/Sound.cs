﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Data class for the sound
/// </summary>
[System.Serializable]
public class Sound
{
    public string name;
    public AudioEvent audioEvent;
}

