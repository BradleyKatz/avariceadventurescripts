﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AudioEvent : ScriptableObject
{
    protected AudioSource source;
    public abstract void Play();

    public abstract void ChangeVolume();

    public abstract void Stop();

    public void SetSource(AudioSource _source)
    {
        source = _source;
    }

    public AudioSource GetSource()
    {
        return source;
    }
}
