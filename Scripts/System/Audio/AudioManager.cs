﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Creates gameObjects of sounds and then can be called to play the sound
/// </summary>
public class AudioManager : Singleton<AudioManager>
{
    [Header("Sound Objects")]
    [SerializeField] private List<Sound> soundEffects;
    [SerializeField] private List<Sound> music;
    private MusicAudioEvent levelTheme;

    private float musicVolume = 1.0f;
    private float sfxVolume = 1.0f;

    private void Start()
    {
        for (int i = 0; i < soundEffects.Count; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + soundEffects[i].name);
            _go.transform.SetParent(this.transform);
            soundEffects[i].audioEvent.SetSource(_go.AddComponent<AudioSource>());
        }

        for (int i = 0; i < music.Count; i++)
        {
            GameObject _go = new GameObject("Song" + i + "_" + music[i].name);
            _go.transform.SetParent(this.transform);
            music[i].audioEvent.SetSource(_go.AddComponent<AudioSource>());
        }

        PlaySong(music[0].name);
    }

    public float GetMusicVolume()
    {
        return musicVolume;
    }

    public void SetMusicVolume(float _volume)
    {
        if (musicVolume == _volume) { return; }

        musicVolume = _volume;
        AdjustMusicVolume();
    }

    private void AdjustMusicVolume()
    {
        for (int i = 0; i < music.Count; i++)
        {
            if (music[i].audioEvent.GetSource().isPlaying)
            {
                music[i].audioEvent.ChangeVolume();
                return;
            }
        }
    }

    public void RegisterSFX(Sound _sound)
    {
        if (_sound.name == "" || _sound.audioEvent == null)
        {
            return;
        }

        for (int i = 0; i < soundEffects.Count; i++)
        {
            if (soundEffects[i].name == _sound.name)
            {
                return;
            }
        }

        GameObject _go = new GameObject("Sound_" + soundEffects.Count + "_" + _sound.name);
        _go.transform.SetParent(this.transform);
        _sound.audioEvent.SetSource(_go.AddComponent<AudioSource>());
        soundEffects.Add(_sound);
    }

    public float GetSFXVolume()
    {
        return sfxVolume;
    }

    public void SetSFXVolume(float _volume)
    {
        if (sfxVolume == _volume) { return; }

        sfxVolume = _volume;
        AdjustSFXVolume();
    }

    private void AdjustSFXVolume()
    {
        for (int i = 0; i < soundEffects.Count; i++)
        {
            if (soundEffects[i].audioEvent.GetSource().isPlaying)
            {
                soundEffects[i].audioEvent.ChangeVolume();
                return;
            }
        }
    }

    public void PlaySoundEffect(string _name)
    {
        for (int i = 0; i < soundEffects.Count; i++)
        {
            if (soundEffects[i].name == _name)
            {
                soundEffects[i].audioEvent.Play();
                return;
            }
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: Sound not found in list: " + _name);
    }

    public void StopSoundEffect(string _name)
    {
        for (int i = 0; i < soundEffects.Count; i++)
        {
            if (soundEffects[i].name == _name)
            {
                soundEffects[i].audioEvent.Stop();
                return;
            }
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: Sound not found in list: " + _name);
    }

    public void PlaySong(string _name)
    {
        for (int i = 0; i < music.Count; i++)
        {
            if (music[i].name == _name)
            {
                if (levelTheme != null && levelTheme.GetSource().isPlaying && music[i].audioEvent.GetType() == typeof(MusicAudioEvent))
                {
                    AudioFadeOut(levelTheme.GetSource(), levelTheme.fadeInTime, false);
                }
                music[i].audioEvent.Play();
                return;
            }
        }

        // no sound with _name
        Debug.LogWarning("AudioManager: Song not found in list: " + _name);
    }

    public void StopSong(string _name)
    {
        for (int i = 0; i < music.Count; i++)
        {
            if (music[i].name == _name)
            {
                if (levelTheme != null && !levelTheme.GetSource().isPlaying && music[i].audioEvent.GetType() == typeof(MusicAudioEvent))
                {
                    AudioFadeIn(levelTheme.GetSource(), levelTheme.fadeOutTime, levelTheme.GetSource().volume, false);
                }
                music[i].audioEvent.Stop();
                return;
            }
        }

        Debug.LogWarning("AudioManager: Song not found in list: " + _name);
    }

    public void StopCurrentSong()
    {
        for (int i = 0; i < music.Count; i++)
        {
            if (music[i].audioEvent.GetSource().isPlaying)
            {
                if (levelTheme != null && !levelTheme.GetSource().isPlaying && music[i].audioEvent.GetType() == typeof(MusicAudioEvent))
                {
                    AudioFadeOut(levelTheme.GetSource(), levelTheme.fadeOutTime, false);
                }
                music[i].audioEvent.Stop();
                return;
            }
        }
    }

    public void SetLevelTheme(string _name)
    {
        for (int i = 0; i < music.Count; i++)
        {
            if (music[i].name == _name)
            {
                levelTheme = (MusicAudioEvent)music[i].audioEvent;
                return;
            }
        }
    }

    public void AudioFadeOut(AudioSource audio, float fadeTime, bool stop = true)
    {
        StartCoroutine(FadeOut(audio, fadeTime, stop));
    }

    public void AudioFadeIn(AudioSource audio, float fadeTime, float maxVolume, bool play = true)
    {
        StartCoroutine(FadeIn(audio, fadeTime, maxVolume, play));
    }

    private IEnumerator FadeOut(AudioSource audioSource, float FadeTime, bool Stop)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        if (Stop)
        {
            audioSource.Stop();
        }
        else
        {
            audioSource.Pause();
        }
        audioSource.volume = startVolume;
    }

    private IEnumerator FadeIn(AudioSource audioSource, float FadeTime, float MaxVolume, bool Play)
    {
        if (Play)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.UnPause();
        }

        audioSource.volume = 0f;
        while (audioSource.volume < MaxVolume)
        {
            audioSource.volume += Time.deltaTime / FadeTime;
            yield return null;
        }
    }
}

