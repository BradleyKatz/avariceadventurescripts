﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

[System.Serializable]
[CreateAssetMenu(menuName = "Audio Events/SFX")]
public class SFXAudioEvent : AudioEvent
{
    public AudioClip clip;

    [Range(0, 1)]
    public float volume = 0.7f;
    [Range(0.5f, 1.5f)]
    public float pitch = 1f;

    [Range(0, 0.5f), Tooltip("Set the randomness that the volume can differentiate")]
    public float randomVolume = 0.1f;
    [Range(0, 0.5f), Tooltip("Set the randomness that the pitch can differentiate")]
    public float randomPitch = 0.1f;

    private float newVolume;

    public override void ChangeVolume()
    {
        source.volume = newVolume * AudioManager.Instance.GetSFXVolume();
    }

    public override void Play()
    {
        source.clip = clip;
        newVolume = (volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f))) * AudioManager.Instance.GetSFXVolume();
        source.volume = newVolume;
        source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));
        source.Play();
    }

    public override void Stop()
    {
        source.Stop();
    }
}