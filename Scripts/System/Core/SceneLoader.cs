﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;

/*
 * Original Author: William Barry
 * Modified by: Caleb Greer
 */

/// <summary>
/// Used for loading scenes
/// Modification: Added loading screen to unloading scenes and tracking of scenes loaded
/// </summary>
public class SceneLoader : Singleton<SceneLoader>
{
    public class SceneLoadedEvent : EventManager.Event
    {
        public Scene scene;
    }

    public void OnSceneLoadedCallBack(SceneLoadedEvent _e)
    {

    }

    private void Start()
    {
        EventManager.Instance.AddListener<SceneLoadedEvent>(OnSceneLoadedCallBack);
    }

    public MenuClassifier loadingMenuClasifier;
    private float delayTimer = 0.3f;
    private string currentLoadedScene = "";
    private string currentLoadedSceneName = "";

    public string getCurrentLoadedScene()
    {
        return currentLoadedSceneName;
    }

    public void UnLoadCurrentScene(bool _showLoadingScreen)
    {
        StartCoroutine(UnLoadScene(currentLoadedScene, _showLoadingScreen));

        if (_showLoadingScreen)
        {
            InputHandler.bInputFrozen = true;
            AudioManager.Instance.StopCurrentSong();
        }
    }

    IEnumerator UnLoadScene(string _unloadScene, bool _showLoadingScreen)
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        if (_showLoadingScreen == true && loadingMenuClasifier != null)
        {
            MenuManager.Instance.showMenu(loadingMenuClasifier);
        }

        yield return new WaitForSeconds(delayTimer);
        AsyncOperation _sync = null;

        _sync = SceneManager.UnloadSceneAsync(_unloadScene);
        while (_sync.isDone == false) { yield return null; }
        _sync = Resources.UnloadUnusedAssets();
        while (_sync.isDone == false) { yield return null; }

        yield return new WaitForSeconds(delayTimer);

        if (_showLoadingScreen == true && loadingMenuClasifier != null)
        {
            MenuManager.Instance.hideMenu(loadingMenuClasifier);
        }

        Application.backgroundLoadingPriority = ThreadPriority.Normal;
    }

    public void LoadLevel(string _loadScene, bool _showLoadingScreen = true, string _unloadScene = "")
    {
        StartCoroutine(LoadScene(_loadScene, _showLoadingScreen, _unloadScene));

        if (_showLoadingScreen)
        {
            InputHandler.bInputFrozen = true;
            AudioManager.Instance.StopCurrentSong();
        }
    }

    IEnumerator LoadScene(string _scene, bool _showLoadingScreen, string _unloadScene)
    {
        Application.backgroundLoadingPriority = ThreadPriority.Low;

        if (_showLoadingScreen == true && loadingMenuClasifier != null)
        {
            MenuManager.Instance.showMenu(loadingMenuClasifier);
        }

        yield return new WaitForSeconds(delayTimer);
        AsyncOperation _sync = null;

        if (_unloadScene != "")
        {
            _sync = SceneManager.UnloadSceneAsync(_unloadScene);
            while (_sync.isDone == false) { yield return null; }
            _sync = Resources.UnloadUnusedAssets();
            while (_sync.isDone == false) { yield return null; }
        }

        if (currentLoadedScene != "" && SceneManager.GetSceneByName(currentLoadedScene).isLoaded)
        {
            _sync = SceneManager.UnloadSceneAsync(currentLoadedScene);
            while (_sync.isDone == false) { yield return null; }
            _sync = Resources.UnloadUnusedAssets();
            while (_sync.isDone == false) { yield return null; }
        }

        yield return new WaitForSeconds(delayTimer);

        _sync = SceneManager.LoadSceneAsync(_scene, LoadSceneMode.Additive);
        while (_sync.isDone == false) { yield return null; }

        yield return new WaitForSeconds(delayTimer);

        string sceneName = Path.GetFileNameWithoutExtension(_scene);
        Scene loadedScene = SceneManager.GetSceneByName(sceneName);
        if (loadedScene != null && loadedScene.buildIndex != -1)
        {
            //UnityEngine.SceneManagement.SceneManager.SetActiveScene(loadedScene);
        }
        //else
        //{
        //    throw new System.Exception("Scene not found in Scene Manager: " + _scene);
        //}

        currentLoadedScene = _scene;
        currentLoadedSceneName = sceneName;

        SceneLoadedEvent _event = new SceneLoadedEvent();
        _event.scene = loadedScene;
        EventManager.Instance.Raise(_event);

        if (_showLoadingScreen == true && loadingMenuClasifier != null)
        {
            MenuManager.Instance.hideMenu(loadingMenuClasifier);
        }

        Application.backgroundLoadingPriority = ThreadPriority.Normal;
    }
}