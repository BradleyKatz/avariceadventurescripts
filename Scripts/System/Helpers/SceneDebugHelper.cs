﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneDebugHelper : MonoBehaviour
{
    public Dropdown dropdown;
    public Button loadButton;

    private int dropdownSelection;

    private string sceneName;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        dropdown.gameObject.SetActive(true);
        loadButton.gameObject.SetActive(true);
#endif
    }

    // Update is called once per frame
    void Update()
    {
        dropdownSelection = dropdown.value;
        sceneName = dropdown.options[dropdownSelection].text;
    }

    public void DebugLoadScene()
    {
        SceneLoader.Instance.LoadLevel(sceneName, true);
        SceneLoader.Instance.UnLoadCurrentScene(false);
        GameController.Instance.DebugStartGame();
    }
}
