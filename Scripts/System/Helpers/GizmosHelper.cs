﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Used as a middle man to use drawGizmos for scriptable objects.
/// Currently just shows jump height, propel height, and jump + propel height
/// </summary>

public class GizmosHelper : MonoBehaviour
{
    public JumpAbility jump;
    public PropelAbility propel;
    public Player player;
    private float jumpHeight;
    private float propelHeight;

    // Start is called before the first frame update
    void Start()
    {  
   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        UpdateJumpTrajectory(player.transform.position, player.velocity, new Vector3(0, player.gravity, 0));
        UpdatePropelTrajectory(player.transform.position);
    }

    void UpdateJumpTrajectory(Vector3 initialPosition, Vector3 initialVelocity, Vector3 gravity)
    {
        jumpHeight = jump.jumpHeight;

        Gizmos.color = Color.blue;
        int numSteps = 50;
        float timeDelta = 0.3f / initialVelocity.magnitude;

        Vector3 position = initialPosition;
        Vector3 velocity = initialVelocity;
        //velocity.y = jump.jumpVelocity;

        // # Jump arc math needs work for our jump #
        //for (int i = 0; i < numSteps; ++i)
        //{
        //    Gizmos.DrawWireSphere(position, 0.05f);

        //    position += velocity * timeDelta + 0.5f * gravity * timeDelta * timeDelta;
        //    velocity += gravity * timeDelta;
        //}

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(initialPosition + new Vector3(0, jumpHeight), 0.1f);
        Gizmos.DrawLine(initialPosition, initialPosition + new Vector3(0, jumpHeight));
    }

    void UpdatePropelTrajectory(Vector3 initialPosition)
    {
        propelHeight = propel.propelHeight;

        //Gizmos.color = Color.green;
        //Gizmos.DrawWireSphere(initialPosition + new Vector3(0, propelHeight), 0.1f);
        //Gizmos.DrawLine(initialPosition, initialPosition + new Vector3(0, propelHeight));

        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(initialPosition + new Vector3(0, propelHeight + jumpHeight), 0.1f);
        Gizmos.DrawLine(initialPosition, initialPosition + new Vector3(0, propelHeight + jumpHeight));
    }
}
