﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : AvariceObject
{
    protected override void CollisionEnter(Collider2D other)
    {
        Destroy(transform.parent.gameObject);
    }
}
