﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraZoomZone : AvariceObject
{
    protected CinemachineVirtualCamera vcamOverride = null;

    protected override void TriggerEnter(Collider2D other)
    {
        vcamOverride = GetComponent<CinemachineVirtualCamera>();
        StartCoroutine(DelayDisableCameraFollow());
    }

    protected override void TriggerExit(Collider2D other)
    {
        CameraController.Instance.ResetLookAtCamera();
    }

    private IEnumerator DelayDisableCameraFollow()
    {
        yield return new WaitForEndOfFrame();
        CameraController.Instance.LookAtCamera(vcamOverride);
    }
}
