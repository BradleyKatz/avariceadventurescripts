﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSettings : MonoBehaviour
{
    [Tooltip("The master field of view applied to the scene as a whole")]
    public float SceneFieldOfView = 0.0f;
    [Range(0.0f, 300.0f), Tooltip("The master camera z-position applied to the scene as a whole")]
    public float SceneCameraDistance = 0.0f;
}
