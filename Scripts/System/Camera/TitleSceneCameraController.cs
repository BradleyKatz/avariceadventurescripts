﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AnimationListener))]
public class TitleSceneCameraController : MonoBehaviour
{
    private GameObject uiCanvasRoot = null;
    private GameObject startButton = null;
    private GameObject quitButton = null;
    private GameObject selectorBox = null;

    private void Start()
    {
        uiCanvasRoot = GameObject.Find("Canvas");
        uiCanvasRoot?.SetActive(false);
        uiCanvasRoot.GetComponent<CanvasGroup>().alpha = 0;

        AnimationListener animationListener = GetComponent<AnimationListener>();
        animationListener.AddAnimationPartwayListener(Animator.StringToHash("UI_camera"), x =>
        {
            uiCanvasRoot?.SetActive(true);
            StartCoroutine(LerpTitleUIAlpha());
        });
    }

    private void Update()
    {
        if (!startButton)
        {
            startButton = GameObject.Find("Start Button");
            startButton?.SetActive(false);
        }

        if (!quitButton)
        {
            quitButton = GameObject.Find("Quit Button");
            quitButton?.SetActive(false);
        }
        
        if (!selectorBox)
        {
            selectorBox = GameObject.Find("SelectorBox");
            selectorBox?.SetActive(false);
        }
        
    }

    private IEnumerator LerpTitleUIAlpha()
    {
        CanvasGroup canvasGroup = uiCanvasRoot.GetComponent<CanvasGroup>();
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        startButton.SetActive(true);
        startButton.GetComponent<Button>().interactable = true;

        quitButton.SetActive(true);
        quitButton.GetComponent<Button>().interactable = true;

        selectorBox.SetActive(true);
    }
}
