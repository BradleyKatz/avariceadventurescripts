﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraController : Singleton<CameraController>
{
    [Range(0.0f, 100.0f), Tooltip("The speed at which the camera zooms in or out")]
    public float cameraZoomSpeed = 5.0f;

    private enum eCameraShakeState
    {
        Inactive,
        Shaking
    }
    private eCameraShakeState currentShakeState = eCameraShakeState.Inactive;

    private float shakeTimeElapsed = 0.0f;
    private CinemachineVirtualCamera vCameraOriginal = null;
    private CinemachineVirtualCamera vCamera = null;
    private CinemachineBasicMultiChannelPerlin cameraNoiseChannel = null;
    private CinemachineConfiner cameraConfiner = null;

    private void Awake()
    {
        DontDestroyOnLoad(transform.root.gameObject);

        vCamera = GetComponent<CinemachineVirtualCamera>();
        vCameraOriginal = vCamera;

        cameraNoiseChannel = vCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
        cameraConfiner = GetComponent<CinemachineConfiner>();

        SceneManager.sceneLoaded += SetCameraBounds;
        SceneManager.sceneLoaded += SetCameraFOVAndPosition;
    }

    public void ActivateCameraShake(float duration = 0.3f, float amplitude = 1.2f, float frequency = 2.0f)
    {
        if (currentShakeState == eCameraShakeState.Inactive && cameraNoiseChannel)
        {
            shakeTimeElapsed = duration;
            cameraNoiseChannel.m_AmplitudeGain = amplitude;
            cameraNoiseChannel.m_FrequencyGain = frequency;
            currentShakeState = eCameraShakeState.Shaking;
        }
    }

    public void EnableCameraFollow()
    {
        vCamera.Follow = PlayerManager.Instance?.GetPlayer().transform;
    }

    public void LookAtCamera(CinemachineVirtualCamera vcamOverride)
    { 
        vCamera = vcamOverride;
        vCamera.enabled = true;

        vCameraOriginal.enabled = false;
    }

    public void ResetLookAtCamera()
    {
        if (vCamera != vCameraOriginal)
        {
            vCamera.enabled = false;
            vCamera = vCameraOriginal;
            vCamera.enabled = true;
        }
    }

    public void ResetCameraPosition()
    {
        ResetLookAtCamera();
        StartCoroutine(ToggleCamera());
    }

    private IEnumerator ToggleCamera()
    {
        vCamera.enabled = false;
        yield return new WaitForEndOfFrame();
        vCamera.enabled = true;
        EnableCameraFollow();
    }

    private void SetCameraBounds(Scene scene, LoadSceneMode mode)
    {
        CameraBounds cameraBounds = FindObjectOfType<CameraBounds>();
        if (cameraBounds && cameraConfiner)
        {
            cameraConfiner.m_BoundingShape2D = cameraBounds.GetBoundsCollider();
        }
    }

    private void SetCameraFOVAndPosition(Scene scene, LoadSceneMode mode)
    {
        CameraSettings cameraSettings = FindObjectOfType<CameraSettings>();
        if (cameraSettings)
        {
            if (cameraSettings.SceneCameraDistance != 0.0f)
            {
                CinemachineFramingTransposer transposer = vCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
                if (transposer)
                {
                    transposer.m_CameraDistance = cameraSettings.SceneCameraDistance;
                }
            }
            
            if (cameraSettings.SceneFieldOfView > 0.0f)
            {
                vCamera.m_Lens.FieldOfView = cameraSettings.SceneFieldOfView;
            }
        }
    }

    private void Update()
    {
        if (currentShakeState == eCameraShakeState.Shaking)
        {
            if (shakeTimeElapsed > 0.0f)
            {
                shakeTimeElapsed -= Time.deltaTime;
            }
            else
            {
                currentShakeState = eCameraShakeState.Inactive;
                cameraNoiseChannel.m_AmplitudeGain = 0.0f;
                cameraNoiseChannel.m_FrequencyGain = 0.0f;
            }
        }
    }
}
