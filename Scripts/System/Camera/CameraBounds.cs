﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PolygonCollider2D))]
public class CameraBounds : MonoBehaviour
{
    private PolygonCollider2D boundsCollider = null;

    private void Awake()
    {
        boundsCollider = GetComponent<PolygonCollider2D>();
    }

    public PolygonCollider2D GetBoundsCollider()
    {
        return boundsCollider;
    }
}
