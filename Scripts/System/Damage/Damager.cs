﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script models all things related to damage calculations in our game, with configurations provided to specify if a Damager should be a one-off attack collision or a continuous stream of damage over time (level hazards)
 */

public class Damager : AvariceObject
{
    public enum eDamageType
    {
        Single,
        Continuous,
    }

    protected enum eDamageState
    {
        Unresolved,
        Finished
    }

    [Header("Camera Shake Properties")]
    [Range(0.0f, 3.0f), Tooltip("The length of camera shake to apply when damage occurs")]
    public float shakeDuration = 0.0f;
    [Range(0.0f, 30.0f), Tooltip("The amplitude of the camera shake when damage occurs")]
    public float shakeAmplitude = 0.0f;
    [Range(0.0f, 3.0f), Tooltip("The frequency of the camera shake when damage occurs")]
    public float shakeFrequency = 0.0f;

    [Header("Knockback Properties")]
    public Vector2 knockbackDirection = Vector2.zero;
    [Range(0.0f, 10.0f)]
    public float knockbackStrength = 0.0f;

    [Header("Damage Properties")]
    [Range(1, 100)]
    public int damage = 1;
    [Tooltip("Whether or not the damage should be accounted for once or several times over a certain timeout buffer")]
    public eDamageType damageType = eDamageType.Single;

    [Header("Continuous Damage Properties")]
    [Range(0.0f, 5.0f)]
    public float continuousDamageDelay = 1.0f;
    private float continuousDamageDelayElapsed = 0.0f;

    [Header("Destruction Properties")]
    [Tooltip("Whether or not this entity should be destroyed after damaging its target")]
    public bool destroyOnContact = false;

    [Header("Sound Properties")]
    public Sound soundEffect;

    [HideInInspector] public bool isDamageCollisionEnabled = false;
    protected eDamageState currentDamageState = eDamageState.Unresolved;

    // Using this allows for an AOE effect
    private HashSet<int> enemiesDamagedInCurrentAttack = new HashSet<int>();

    private Dictionary<string, BoxCollider2D> abilityNameToAttackColliderDict = new Dictionary<string, BoxCollider2D>();

    protected override void Start()
    {
        base.Start();

        AttackColliderReference[] abilityDamageColliders = GetComponentsInChildren<AttackColliderReference>();
        foreach (AttackColliderReference colliderReference in abilityDamageColliders)
        {
            abilityNameToAttackColliderDict.Add(colliderReference.combatAbility.abilityName, colliderReference.abilityCollider);
        }

        if (soundEffect.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(soundEffect);
        }
    }

    public void SetAttackCollider(string abilityName)
    {
        BoxCollider2D foundCollider = null;
        if (abilityNameToAttackColliderDict.TryGetValue(abilityName, out foundCollider))
        {
            box2D = foundCollider;
        }
    }

    public void ResetDamagerState()
    {
        currentDamageState = eDamageState.Unresolved;
        knockbackDirection = Vector2.zero;
        knockbackStrength = 0.0f;
        enemiesDamagedInCurrentAttack.Clear();
        isDamageCollisionEnabled = false;
    }

    protected virtual void DealDamageAndKnockback(Damageable damageableObj)
    {
        if (!damageableObj.IFramesActive)
        {
            AudioManager.Instance.PlaySoundEffect(soundEffect.name);
            damageableObj.OnDamageTaken(damage);

            if (damageType == eDamageType.Continuous)
            {
                currentDamageState = eDamageState.Finished;
            }

            if (knockbackStrength > 0.0f && !damageableObj.isImmuneToKnockback)
            {
                AvariceEntity otherEntity = damageableObj.GetComponent<AvariceEntity>();
                if (otherEntity)
                {
                    knockbackDirection.x = knockbackDirection.x * Mathf.Sign(transform.right.x);
                    otherEntity.ApplyKnockbackForce(knockbackDirection, knockbackStrength);
                }
            }

            if (shakeDuration > 0.0f)
            {
                CameraController.Instance.ActivateCameraShake(shakeDuration, shakeAmplitude, shakeFrequency);
            }
        }
    }

    protected void ProcessDamageCollision(Collider2D other)
    {
        if (isDamageCollisionEnabled)
        {
            Damageable damageableObj = other.GetComponent<Damageable>();
            if (damageableObj)
            {
                if (damageType == eDamageType.Continuous && currentDamageState == eDamageState.Unresolved)
                {
                    DealDamageAndKnockback(damageableObj);
                }
                else if (damageType == eDamageType.Single && !enemiesDamagedInCurrentAttack.Contains(other.gameObject.GetInstanceID()))
                {
                    enemiesDamagedInCurrentAttack.Add(other.gameObject.GetInstanceID());
                    DealDamageAndKnockback(damageableObj);
                }

                if (destroyOnContact)
                {
                    if (gameObject.transform.parent != null)
                    {
                        Destroy(gameObject.transform.parent);
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }
            }
        }
    }

    protected override void Update()
    {
        base.Update();

        if (damageType == eDamageType.Continuous && currentDamageState == eDamageState.Finished)
        {
            continuousDamageDelayElapsed += Time.deltaTime;
            if (continuousDamageDelayElapsed >= continuousDamageDelay)
            {
                continuousDamageDelayElapsed = 0.0f;
                currentDamageState = eDamageState.Unresolved;
            }
        }
    }

    #region Trigger Callbacks
    protected override void TriggerEnter(Collider2D other)
    {
        ProcessDamageCollision(other);
    }

    protected override void TriggerStay(Collider2D other)
    {
        ProcessDamageCollision(other);
    }

    protected override void TriggerExit(Collider2D other)
    {
        currentDamageState = eDamageState.Unresolved;
        continuousDamageDelayElapsed = 0.0f;
    }
    #endregion
}
