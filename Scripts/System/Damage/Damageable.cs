﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by: Caleb Greer
 * 
 * This script keeps track of the amount of health Avarice Adventure entities have, both in terms of how much they have remaining and their maximum health.
 * This script also contains flags for whether or not iframes or knockback should be applied, as well as a coroutine to create damage flashes when an entity takes damage.
 */

public class Damageable : MonoBehaviour
{
    [Header("Health Properties")]
    [Range(0, 30)]
    public int health;
    [Range(1, 30)]
    public int maxHealth;

    [Header("iFrame Properties")]
    [Tooltip("Whether or not this entity is presently invincible")]
    [SerializeField] protected bool iFramesActive = false;
    public bool IFramesActive
    {
        get
        {
            return iFramesActive;
        }
    }

    [Range(0.0f, 2.0f), Tooltip("The length of time that iframes will be active after taking damage")]
    public float iFramesLength = 0.0f;

    [Header("Knockback Properties")]
    [Tooltip("Whether or not this entity is immune to knockback forces")]
    public bool isImmuneToKnockback = false;

    [Header("Visual Feedback")]
    public int numberOfFlashes = 5;
    public float flashDuration = 0.2f;
    public Color damagedColor = Color.red;
    public GameObject hitParticleEffect;
    public GameObject deathParticleEffect;
    public float yOffsetForParticle = 0.7f;

    [Header("Sound Properties")]
    public Sound deathSound;

    protected virtual void Start()
    {
        health = maxHealth;
    }

    public virtual void OnDamageTaken(int damage)
    {
        if (!iFramesActive)
        {
            int deltaHealth = health - damage;
            health = (deltaHealth >= 0) ? deltaHealth : 0;

            if (iFramesLength > 0.0f)
            {
                ActivateIFrames(iFramesLength);
            }

            if (health == 0)
            {
                if (deathSound.audioEvent != null)
                {
                    AudioManager.Instance.RegisterSFX(deathSound);
                    AudioManager.Instance.PlaySoundEffect(deathSound.name);
                }

                if (deathParticleEffect != null)
                {
                    Instantiate(deathParticleEffect, new Vector3(transform.position.x, transform.position.y + yOffsetForParticle, transform.position.z), Quaternion.identity);
                }

                OnDeath();
                return;
            }
            StartCoroutine(HurtBlink(numberOfFlashes));
            if (hitParticleEffect != null)
            {
                Instantiate(hitParticleEffect, new Vector3(transform.position.x, transform.position.y + yOffsetForParticle, transform.position.z), Quaternion.identity);
            }
        }
    }

    public virtual void Heal(int healAmount)
    {
        if (healAmount <= 0) { return; }

        int deltaHealth = health + healAmount;
        health = (deltaHealth <= maxHealth) ? deltaHealth : maxHealth;
    }

    public void ActivateIFrames(float duration)
    {
        if (!iFramesActive)
        {
            iFramesActive = true;
            Invoke("DisableIFramesOnTimeout", duration);
        }
    }

    public void ActivateIFrames()
    {
        if (!iFramesActive)
        {
            iFramesActive = true;
        }
    }

    public void DeactivateIFrames()
    {
        iFramesActive = false;
    }

    protected virtual void OnDeath()
    {
        //Destroy(this.gameObject);
    }

    IEnumerator HurtBlink(float duration)
    {
        for (int i = 0; i < duration; i++)
        {
            int z = 0;
            foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            {
                if (rend.GetType() == typeof(SpriteRenderer) || rend.GetType() == typeof(ParticleSystemRenderer))
                {
                    continue;
                }
                foreach(Material mat in rend.materials)
                {
                    mat.color = damagedColor;
                }
            }
            yield return new WaitForSeconds(flashDuration);
            int k = 0;
            foreach (Renderer rend in GetComponentsInChildren<Renderer>())
            {
                if (rend.GetType() == typeof(SpriteRenderer) || rend.GetType() == typeof(ParticleSystemRenderer))
                {
                    continue;
                }
                foreach (Material mat in rend.materials)
                {
                    mat.color = Color.white;
                }
            }
            yield return new WaitForSeconds(flashDuration);
            i++;
        }
    }

    private void DisableIFramesOnTimeout()
    {
        iFramesActive = false;
    }
}