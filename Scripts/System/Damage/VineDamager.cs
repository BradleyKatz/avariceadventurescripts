﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * Simple override of the Damager script just for Vine hazards that always ensures that the knockback given to the player pushes them opposite of the direction they collided with the vine in.
 * This is because vine hazards should only be passable via rolling.
 */

public class VineDamager : Damager
{
    protected override void Start()
    {
        base.Start();
        isDamageCollisionEnabled = true;
    }

    protected override void DealDamageAndKnockback(Damageable damageableObj)
    {
        if (!damageableObj.IFramesActive)
        {
            damageableObj.OnDamageTaken(damage);

            if (damageType == eDamageType.Continuous)
            {
                currentDamageState = eDamageState.Finished;
            }
        }
    }
}
