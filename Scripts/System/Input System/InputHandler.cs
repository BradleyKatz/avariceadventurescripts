﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class InputHandler : MonoBehaviour
{
    public static bool bInputFrozen = true;

    public static void EnableAbility(AbilityBase ability)
    {
        ability.isEnabled = true;
    }

    public static void DisableAbility(AbilityBase ability)
    {
        ability.isEnabled = false;
    }

    public static bool GetButton(string inputName)
    {
        if (Input.GetButton(inputName) && !bInputFrozen)
        {
            return true;
        }
        return false;
    }

    public static bool GetButtonDown(string inputName)
    {
        if (Input.GetButtonDown(inputName) && !bInputFrozen)
        {
            return true;
        }
        return false;
    }

    public static bool GetButtonUp(string inputName)
    {
        if (Input.GetButtonUp(inputName) && !bInputFrozen)
        {
            return true;
        }
        return false;
    }

    public static float GetAxis(string axisName)
    {
        if (!bInputFrozen)
        {
            return Input.GetAxis(axisName);
        }
        return 0;
    }

    public static float GetAxisRaw(string axisName)
    {
        if (!bInputFrozen)
        {
            return Input.GetAxisRaw(axisName);
        }
        return 0;
    }
}
