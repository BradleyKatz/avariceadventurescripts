﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 */

//public class InputSystem : Singleton<InputSystem>
//{
//    #region Helper Data Structures
//    public enum eInputContext
//    {
//        None,
//        Gameplay,
//        MainMenu
//    }

//    public enum eInputType
//    {
//        Pressed,
//        Held,
//        Released
//    }

//    public class InputBinding
//    {
//        public eInputContext inputContext = eInputContext.None;
//        [Range(1, 100)]
//        public int priority = 1;
//        public string inputName = string.Empty;
//        public string buttonName = string.Empty;
//        public eInputType inputEventType = eInputType.Pressed;
//        [Range(-1.0f, 1.0f)]
//        public float horizontalAxisOffset = 0.0f;
//        [Range(-1.0f, 1.0f)]
//        public float verticalAxisOffset = 0.0f;

//        #region Operators & Methods
//        public static bool operator <(InputBinding lhs, InputBinding rhs)
//        {
//            return lhs.priority < rhs.priority;
//        }

//        public static bool operator >(InputBinding lhs, InputBinding rhs)
//        {
//            return lhs.priority < rhs.priority;
//        }

//        public static bool operator ==(InputBinding lhs, InputBinding rhs)
//        {
//            return lhs.inputContext == rhs.inputContext && lhs.inputName == rhs.inputName && lhs.buttonName == rhs.buttonName &&  lhs.inputEventType == rhs.inputEventType && lhs.horizontalAxisOffset == rhs.horizontalAxisOffset && lhs.verticalAxisOffset == rhs.verticalAxisOffset && lhs.priority == rhs.priority;
//        }

//        public static bool operator !=(InputBinding lhs, InputBinding rhs)
//        {
//            return lhs.inputContext != rhs.inputContext || lhs.inputName != rhs.inputName || lhs.buttonName != rhs.buttonName || lhs.inputEventType != rhs.inputEventType || lhs.horizontalAxisOffset != rhs.horizontalAxisOffset || lhs.verticalAxisOffset != rhs.verticalAxisOffset || lhs.priority != rhs.priority;
//        }

//        public override bool Equals(object obj)
//        {
//            var binding = obj as InputBinding;
//            return binding != null &&
//                   inputContext == binding.inputContext &&
//                   priority == binding.priority &&
//                   inputName == binding.inputName &&
//                   buttonName == binding.buttonName &&
//                   inputEventType == binding.inputEventType &&
//                   horizontalAxisOffset == binding.horizontalAxisOffset &&
//                   verticalAxisOffset == binding.verticalAxisOffset;
//        }

//        public override int GetHashCode()
//        {
//            var hashCode = 1947541650;
//            hashCode = hashCode * -1521134295 + inputContext.GetHashCode();
//            hashCode = hashCode * -1521134295 + priority.GetHashCode();
//            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(inputName);
//            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(buttonName);
//            hashCode = hashCode * -1521134295 + inputEventType.GetHashCode();
//            hashCode = hashCode * -1521134295 + horizontalAxisOffset.GetHashCode();
//            hashCode = hashCode * -1521134295 + verticalAxisOffset.GetHashCode();
//            return hashCode;
//        }
//        #endregion
//    }
//    #endregion

//    protected eInputContext currentInputContext = eInputContext.None;

//    protected List<string> registeredButtonNames = new List<string>();
//    protected Dictionary<eInputContext, Dictionary<string, List<InputBinding>>> contextToButtonBindingDict = new Dictionary<eInputContext, Dictionary<string, List<InputBinding>>>();


//    public void RegisterInputBinding(InputBinding inputBinding)
//    {
//        if (inputBinding.inputContext != eInputContext.None)
//        {
//            if (!contextToButtonBindingDict[inputBinding.inputContext].ContainsKey(inputBinding.buttonName))
//            {
//                registeredButtonNames.Add(inputBinding.buttonName);
//                (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName] = new List<InputBinding>();
//            }

//            (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName].Add(inputBinding);

//            // Always leave the input binding list sorted in descending order
//            (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName].Sort();
//            (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName].Reverse();
//        }
//    }

//    public void RemoveInputBinding(InputBinding inputBinding)
//    {
//        if (inputBinding.inputContext != eInputContext.None)
//        {
//            if (contextToButtonBindingDict[inputBinding.inputContext].ContainsKey(inputBinding.buttonName))
//            {
//                for (int i = 0; i < (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName].Count; ++i)
//                {
//                    if ((contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName][i] == inputBinding)
//                    {
//                        (contextToButtonBindingDict[inputBinding.inputContext])[inputBinding.buttonName].RemoveAt(i);
//                        break;
//                    }
//                }
//            }
//        }
//    }

//    public void SetInputContext(eInputContext context)
//    {
//        if (context != eInputContext.None)
//        {
//            currentInputContext = context;
//        }
//    }

//    protected void Start()
//    {
//        contextToButtonBindingDict[eInputContext.Gameplay] = new Dictionary<string, List<InputBinding>>();
//        contextToButtonBindingDict[eInputContext.MainMenu] = new Dictionary<string, List<InputBinding>>();
//    }

//    protected void Update()
//    {
//        UpdateInputEvents();
//    }

//    protected void UpdateInputEvents()
//    {
//        foreach (string buttonName in registeredButtonNames)
//        {
//            List<InputBinding> buttonBindings = contextToButtonBindingDict[currentInputContext][buttonName];
//            foreach (InputBinding binding in buttonBindings)
//            {
//                bool isButtonDown = Input.GetButtonDown(buttonName);
//                Vector2 joyAxisOffset = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

//                if (isButtonDown)
//                {
//                    if (binding.horizontalAxisOffset != 0.0f && binding.verticalAxisOffset != 0.0f)
//                    {
//                        if (Mathf.Approximately(binding.horizontalAxisOffset, joyAxisOffset.x) && Mathf.Approximately(binding.verticalAxisOffset, joyAxisOffset.y))
//                        {

//                        }
//                    }
//                    else if (binding.verticalAxisOffset != 0.0f)
//                    {
//                        if (Mathf.Approximately(binding.verticalAxisOffset, joyAxisOffset.y))
//                        {

//                        }
//                    }
//                    else if (binding.horizontalAxisOffset != 0.0f)
//                    {
//                        if (Mathf.Approximately(binding.horizontalAxisOffset, joyAxisOffset.x))
//                        {

//                        }
//                    }
//                }
//            }
//        }
//    }
//}
