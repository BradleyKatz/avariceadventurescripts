﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 */

public class DummyPickup : PickupController
{
    public override void OnPickupGrabbed(Player player)
    {
        Destroy(this.gameObject);
    }
}
