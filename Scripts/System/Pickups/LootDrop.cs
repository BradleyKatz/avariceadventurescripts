﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Loot Drop system for determining what loot something will drop with a set drop chance for everything
/// </summary>

// TODO: Improve this to have a set drop chance for each item
public class LootDrop : Damageable
{
    //public Sound onDeathSound;

    [Header("Loot Properties")]
    public List<Item> items = new List<Item>();

    protected override void Start()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        //if (onDeathSound.audioEvent != null)
        //{
            //AudioManager.Instance.RegisterSFX(onDeathSound);
        //}
    }

    protected override void OnDeath()
    {
        foreach (Item item in items)
        {
            int i = Random.Range(0, 100);
            if (i <= item.dropChance)
            {
                SpawnedGameObjectManager.Instance.AddObject(Instantiate(item.item, transform.position + new Vector3(0.0f, 0.5f, 0.0f), Quaternion.identity));
            }
        }

        //AudioManager.Instance.PlaySoundEffect(onDeathSound.name);
        Destroy(this.gameObject);
    }
}