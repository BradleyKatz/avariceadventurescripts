﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script serves as a base for any pickup objects in the game. The reason that it derives from AvariceEntity is because we thought it would be interesting if physics properties could still be applied to pickups.
 * To be more specific, this allows for a pickup spawned from defeating an enemy riding on a moving platform to land on said platform and ride it like any other entity in the game.
 */

public abstract class PickupController : AvariceEntity
{
    public Sound onPickupSound = null;

    public float despawnTime = 0.0f;
    protected float currentTimeActive = 0.0f;

    public abstract void OnPickupGrabbed(Player player);

    protected override void Start()
    {
        base.Start();

        if (onPickupSound.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(onPickupSound);
        }
    }

    protected override void Update()
    {
        if (despawnTime > 0.0f)
        {
            currentTimeActive += Time.deltaTime;
            if (currentTimeActive > despawnTime)
            {
                Destroy(this.gameObject);
            }
        }

        if (gravity > 0.0f && controller.IsGrounded)
        {
            controller.Move(new Vector2(0.0f, -gravity * Time.deltaTime));
        }
    }

    protected override void TriggerEnter(Collider2D other)
    {
        Player player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            OnPickupGrabbed(player);
        }
    }
}
