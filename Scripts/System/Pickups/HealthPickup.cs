﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// An object that can be picked up to heal the player
/// </summary>
public class HealthPickup : PickupController
{
    public int healAmount = 1;

    public override void OnPickupGrabbed(Player player)
    {
        AudioManager.Instance.PlaySoundEffect(onPickupSound.name);
        player.playerHealth.Heal(healAmount);
        Destroy(this.gameObject);
    }
}
