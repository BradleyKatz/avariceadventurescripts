﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Is used to check for when the player lands on the ground and spawns the dustCloud particle effect
/// </summary>
public class GroundCheck : AvariceObject
{
    [SerializeField]
    GameObject dustCloud;

    protected override void TriggerEnter(Collider2D other)
    {
        base.TriggerEnter(other);

        Instantiate(dustCloud, transform.position, dustCloud.transform.rotation);
    }
}
