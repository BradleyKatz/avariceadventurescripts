﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */


/// <summary>
/// Destroys any object with this script after a set amount of time
/// </summary>
public class TimedDestroy : MonoBehaviour
{
    public float timer = 1f;

    void Start()
    {
        Destroy(gameObject, timer);
    }
}
