﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script serves as the second half of the capture system as it models which capture abilities are attached to which enemies.
 * Additionally, this script also controls when an enemy is vulnerable to capture and how many uses of the ability the player will obtain upon capture.
 * 
 */

[RequireComponent(typeof(Damageable))]
public class Captureable : MonoBehaviour
{
    [System.Serializable]
    public class CaptureAbilityStats
    {
        [Tooltip("The type of enemy captured.")]
        public eEnemyType captureEnemyType;

        [Tooltip("The ability given to the player upon successful capture of this enemy")]
        public AbilityBase captureAbility;

        [Range(1, 3), Tooltip("The number of uses available before this captured ability is lost")]
        public int uses;

        public CaptureAbilityStats(CaptureAbilityStats otherStats)
        {
            captureAbility = otherStats.captureAbility;
            uses = otherStats.uses;
            captureEnemyType = otherStats.captureEnemyType;
        }
    }

    private enum eCaptureState
    {
        Immune,
        Vulnerable,
        Captured
    }

    [Header("Capture Properties")]
    [Range(0.0f, 1.0f), Tooltip("When the enemy has this percentage of health remaining, they will become vulnerable to capture")]
    public float vulnerableHealthPercentage = 0.25f;
    [Tooltip("The button sprite that will be displayed above the enemy's head when they are vulnerable to capture")]
    public Sprite capturePromptSprite = null;
    [Tooltip("The position relative to the enemy that the capture prompt will appear")]
    public Vector3 capturePromptPosition = Vector2.zero;
    [Tooltip("The color value to use to show that the capture is out of the player's range")]
    public Color capturePromptOutOfRangeColor = Color.gray;
    [Tooltip("Properties related to the capture ability given to the player upon successful capture")]
    public CaptureAbilityStats captureAbilityStats;

    private eCaptureState currentCaptureState = eCaptureState.Immune;
    public bool IsVulnerable
    {
        get
        {
            return currentCaptureState == eCaptureState.Vulnerable;
        }
    }

    private bool captureSpriteShown = false;

    private Damageable entityDamageable;
    private SpriteRenderer promptRenderer;

    protected void Start()
    {
        entityDamageable = GetComponent<Damageable>();
        promptRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        float size = 0.3f;

        Vector3 promptWorldPos = transform.position + capturePromptPosition;
        Gizmos.DrawLine(promptWorldPos - Vector3.up * size,  promptWorldPos + Vector3.up * size);
        Gizmos.DrawLine(promptWorldPos - Vector3.left * size, promptWorldPos + Vector3.left * size);
    }

    public void OnEnemyCaptured(Player player)
    {
        currentCaptureState = eCaptureState.Captured;
        StartCoroutine(CaptureEnemy(player));
    }

    private IEnumerator CaptureEnemy(Player player)
    {
        // Assuming the scale for all three dimensions will be uniform
        Vector3 targetScale = new Vector3(transform.localScale.x * 0.25f, transform.localScale.y * 0.25f, transform.localScale.z);
        float shrinkSpeed = 15.0f;

        promptRenderer.enabled = false;

        // Disable everything that can hurt the player
        {
            // ...Which just so happens to be every AvariceObject attached to the enemy
            AvariceObject[] avariceObjects = GetComponentsInChildren<AvariceObject>();
            foreach (AvariceObject avariceObject in avariceObjects)
            {
                avariceObject.enabled = false;
            }
        }

        yield return new WaitForSeconds(0.3f);

        float shrinkTimeElapsed = 0.25f;
        while (shrinkTimeElapsed > 0.0f)
        {
            shrinkTimeElapsed -= Time.deltaTime;

            transform.localScale = Vector3.Lerp(transform.localScale, targetScale, Time.deltaTime * shrinkSpeed);
            transform.position = new Vector3(transform.position.x, transform.position.y - Time.deltaTime, transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        Destroy(this.gameObject);
    }

    protected void Update()
    {
        if (currentCaptureState == eCaptureState.Immune)
        {
            if (entityDamageable.health <= Mathf.Ceil(entityDamageable.maxHealth * vulnerableHealthPercentage))
            {
                currentCaptureState = eCaptureState.Vulnerable;
            }
        }
        else if (currentCaptureState == eCaptureState.Vulnerable && !captureSpriteShown)
        {
            captureSpriteShown = true;
            promptRenderer.sprite = capturePromptSprite;
            promptRenderer.transform.localPosition = capturePromptPosition;
        }
    }

    public void OnCaptureInRange()
    {
        promptRenderer.color = Color.white;
    }

    public void OnCaptureOutOfRange()
    {
        promptRenderer.color = capturePromptOutOfRangeColor;
    }
}
