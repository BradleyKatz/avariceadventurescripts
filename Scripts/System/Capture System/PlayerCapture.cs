﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * This script simply handles the logic behind capturing an enemy in the bag. If the player is close enough to an enemy that is vulnerable to capture, the player can then
 * trigger the capture process by pressing the appropriate button.
 * 
 */

public class PlayerCapture : AvariceObject
{
    public string captureInputAxisName = "Capture";
    public string captureAnimName = "Ground Capture";
    public GameObject captureAnimHalfwayParticles;

    [Header("Sound Properties")]
    [Tooltip("The sound effect that plays when an enemy is successfully captured")]
    public Sound onCaptureSucessSound = null;

    private bool isCaptureAnimActive = false;
    public bool IsCaptureAnimActive
    {
        get
        {
            return isCaptureAnimActive;
        }
    }

    private Player player;
    private Captureable currentEnemyCapturable = null;

    protected override void Start()
    {
        base.Start();
        player = GetComponentInParent<Player>();
        currentEnemyCapturable = null;

        if (onCaptureSucessSound.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(onCaptureSucessSound);
        }

        player.animationListener.AddAnimationStartedListener(Animator.StringToHash(captureAnimName), x => 
        {
            player.isMovementFrozen = true;
            isCaptureAnimActive = true;
        });

        player.animationListener.AddAnimationPartwayListener(Animator.StringToHash(captureAnimName), x =>
        {
            if (captureAnimHalfwayParticles != null && currentEnemyCapturable != null)
            {
                Instantiate(captureAnimHalfwayParticles, player.blendshapeReference.transform.position + new Vector3(0.0f, 0.5f, 0.0f), Quaternion.identity);
                AudioManager.Instance.PlaySoundEffect(onCaptureSucessSound.name);
            }
        });

        player.animationListener.AddAnimationCompletedListener(Animator.StringToHash(captureAnimName), x => 
        {
            if (currentEnemyCapturable?.captureAbilityStats != null)
            {
                player.currentCapture = new Captureable.CaptureAbilityStats(currentEnemyCapturable.captureAbilityStats);
                player.currentCapture.captureAbility.Initialize(player.gameObject);
            }

            Reset();
        });
    }

    protected override void Update()
    {
        base.Update();

        if (player.controller.IsGrounded && !isCaptureAnimActive)
        {
            if (InputHandler.GetButtonDown(captureInputAxisName))
            {
                player.blendshapeReference?.ResetAll();
                player.animator.SetTrigger("Capture");
                player.bagAnimator.SetTrigger("Capture");
            }
        }
    }

    public void Reset()
    {
        player.playerHealth.DeactivateIFrames();
        currentEnemyCapturable = null;
        player.isMovementFrozen = false;
        isCaptureAnimActive = false;
    }

    private void OnCapturePressed(Collider2D other)
    {
        Captureable enemyCaptureable = other.gameObject.GetComponent<Captureable>();
        if (enemyCaptureable && enemyCaptureable.IsVulnerable)
        {
            currentEnemyCapturable = enemyCaptureable;
            player.playerHealth.ActivateIFrames();
            currentEnemyCapturable.OnEnemyCaptured(player);
        }
    }

    private void OnCaptureInRange(Collider2D other)
    {
        Captureable enemyCapturable = other.gameObject.GetComponent<Captureable>();
        if (enemyCapturable && enemyCapturable.IsVulnerable)
        {
            enemyCapturable.OnCaptureInRange();
        }
    }

    private void OnCaptureOutOfRange(Collider2D other)
    {
        if (other)
        {
            Captureable enemyCapturable = other.gameObject.GetComponent<Captureable>();
            if (enemyCapturable && enemyCapturable.IsVulnerable)
            {
                enemyCapturable.OnCaptureOutOfRange();
            }
        }
    }

    #region Trigger Callback Overrides
    protected override void TriggerEnter(Collider2D other)
    {
        OnCaptureInRange(other);

        if (isCaptureAnimActive)
        {
            OnCapturePressed(other);
        }
    }

    protected override void TriggerStay(Collider2D other)
    {
        OnCaptureInRange(other);

        if (isCaptureAnimActive)
        {
            OnCapturePressed(other);
        }
    }

    protected override void TriggerExit(Collider2D other)
    {
        base.TriggerExit(other);
        OnCaptureOutOfRange(other);
    }
    #endregion
}
