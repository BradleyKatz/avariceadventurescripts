﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendShapeReference : MonoBehaviour
{
    [HideInInspector] public List<Tuple<int, string, SkinnedMeshRenderer>> blendshapes;
    [HideInInspector] public Vector3 originalLocalPosition = Vector3.zero;
    [HideInInspector] public Quaternion originalLocalRotation = Quaternion.identity;

    private void Awake()
    {
        blendshapes = new List<Tuple<int, string, SkinnedMeshRenderer>>();
        originalLocalPosition = transform.localPosition;
        originalLocalRotation = transform.localRotation;

        SkinnedMeshRenderer[] skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers)
        {
            Mesh sharedMesh = skinnedMeshRenderer.sharedMesh;
            for (int i = 0; i < sharedMesh.blendShapeCount; ++i)
            {
                Tuple<int, string, SkinnedMeshRenderer> indexNameRendererTriplet = new Tuple<int, string, SkinnedMeshRenderer>(i, sharedMesh.GetBlendShapeName(i), skinnedMeshRenderer);
                blendshapes.Add(indexNameRendererTriplet);
            }
        }
    }

    public void ResetAll()
    {
        transform.localPosition = originalLocalPosition;
        transform.localRotation = originalLocalRotation;

        foreach (var reference in blendshapes)
        {
            for (int i = 0; i < reference.Item3.sharedMesh.blendShapeCount; ++i)
            {
                reference.Item3.SetBlendShapeWeight(i, 0.0f);
            }
        }
    }
}
