﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Controls the melee attack, shockwave ability and walking animation of the brawler enemy
/// </summary>
public class BrawlerLogic : EnemyLogic
{
    [Header("Brawler Properties")]
    public bool arenaMode = false;
    [Range(1, 10), Tooltip("How far from the player can the brawler be to begin his attack")]
    public float distanceFromPlayer = 1;
    [Range(1,5)]
    public int meleeDamage = 1;

    [Header("Shockwave Properties")]
    public GameObject shockwaveProjectile;
    public float projectileSpeed = 20;
    public float projectileDuration = 3;
    [Range(1, 5)]
    public int projectileDamage = 1;
    public float _delay = 2f;
    public float yOffset = 1.0f;

    private bool useShockwave;
    private Damager damager;
    private Vector3 dir;

    public bool Shockwave { get { return useShockwave; } set { useShockwave = value; } }

    protected override void Start()
    {
        base.Start();

        damager = GetComponentInChildren<Damager>();
        damager.damage = meleeDamage;
        targetDestination = transform.position;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (arenaMode && PlayerManager.Instance.GetPlayer() != null)
        {
            targetDestination = PlayerManager.Instance.GetPlayer().transform.position;
        }

        targetDestination = new Vector3(targetDestination.x, transform.position.y, transform.position.z);
        
        if (bAttacking)
        {         
            dir = targetDestination - transform.position;
            float moveDir = Mathf.Sign(dir.x);

            if (Mathf.Abs(dir.x) > distanceFromPlayer)
            {
                targetDestination.x = targetDestination.x - (distanceFromPlayer * moveDir);
            }
            else
            {
                targetDestination.x = transform.position.x;
            }

            bAttacking = false;
        }

        if (useShockwave)
        {
            GroundSlam(dir);
            useShockwave = false;
        }

        animator.SetFloat("Speed", Mathf.Abs(velocity.x));
    }

    public void GroundSlam(Vector3 dir)
    {
        Vector3 tempTrans = new Vector3(transform.position.x, transform.position.y + yOffset, 0);
        var proj = Instantiate(shockwaveProjectile, tempTrans, Quaternion.identity);

        var entity = proj.GetComponent<ProjectileLogic>();
        entity.moveSpeed = projectileSpeed;
        entity.duration = projectileDuration;
        entity.damage = (int)projectileDamage;

        float moveDir = Mathf.Sign(dir.x);
        entity.velocity.x = moveDir * projectileSpeed;
    }

    public void SetShockwave(int i)
    {
        if (i == 1)
        {
            useShockwave = true;
        }
    }
}
