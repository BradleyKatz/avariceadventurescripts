using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Handles the targeting, rotation, and casting of the wizard
/// Sets the fireball values once casted
/// </summary>
public class WizardLogic : EnemyLogic
{
    [Header("Wizard Properties")]
    public GameObject projectile;
    public GameObject spawnLocation;
    public float projectileSpeed = 20;
    public float projectileDuration = 3;
    [Range(1, 10)]
    public int projectileDamage = 3;
    public float _delay = 2f;

    [Header("Sound Properties")]
    public Sound spellCast;

    public float Delay { get { return _delay; } set { _delay = value; } }

    protected Transform modelTransform = null;

    protected override void Start()
    {
        base.Start();

        // Assumes the second child of the Wizard GO is the model
        modelTransform = transform.GetChild(1);

        if (spellCast.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(spellCast);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (PlayerManager.Instance.GetPlayer() != null)
        {
            targetDestination = PlayerManager.Instance.GetPlayer().transform.position;
        }

        Vector3 directionVector = targetDestination - transform.position;
        if (modelTransform != null)
        {
            if (Mathf.Sign(directionVector.x) == -1.0)
            {
                if (transform.rotation.y != -90.0f)
                {
                    modelTransform.eulerAngles = new Vector3(0, -90, 0);
                }
            }
            else if (Mathf.Sign(directionVector.x) == 1.0)
            {
                if (transform.rotation.y != 90.0f)
                {
                    modelTransform.eulerAngles = new Vector3(0, 90.0f, 0);
                }
            }

            if (bAttacking)
            {
                CastSpell(directionVector);
                bAttacking = false;
            }
        }
    }

    public void SetAttacking()
    {
        bAttacking = true;
    }

    public void CastSpell(Vector3 dir)
    {
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        var proj = Instantiate(projectile, spawnLocation.transform.position, Quaternion.AngleAxis(angle, Vector3.forward));
        var entity = proj.GetComponent<ProjectileLogic>();

        Vector3 toTarget = dir.normalized;

        entity.moveSpeed = projectileSpeed;
        entity.duration = projectileDuration;
        entity.damage = (int)projectileDamage;

        entity.velocity = toTarget * projectileSpeed;

        entity.leftRotation = Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles;
        entity.rightRotation = Quaternion.AngleAxis(angle, Vector3.forward).eulerAngles;

        AudioManager.Instance.PlaySoundEffect(spellCast.name);
    }
}