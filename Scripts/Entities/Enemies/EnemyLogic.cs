﻿using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// The parent class of all our enemies
/// Controls the movement and death logic
/// </summary>
public class EnemyLogic : AvariceEntity 
{
    [Header("Enemy Properties")]
    [HideInInspector] public Vector3 targetDestination;
    [HideInInspector] public float targetAngle;

    [Tooltip("The vectors in this list are added to the gameobjects position")]
    [SerializeField] private List<Vector3> localWaypoints = new List<Vector3>();
    [HideInInspector] public Vector3List worldWaypoints = new Vector3List();

    [HideInInspector] protected bool bAttacking = false;
    [HideInInspector] protected bool bStunned = false;

    [HideInInspector] public Animator animator;
    [HideInInspector] public AnimationListener animationListener;
    private Damageable enemyHealth;

    public GameObject deathEffect;

    BehaviorTree behaviorTree;

    #region Properties
    public bool Attacking { get { return bAttacking; } set { bAttacking = value; } }
    public Vector3List WayPoints { get { return worldWaypoints; } set { worldWaypoints = value; } }
    public Vector3 TargetDestination { get { return targetDestination; } set { targetDestination = value; } }
    #endregion

    protected void Awake()
    {
        animationListener = GetComponent<AnimationListener>();
    }

    protected override void Start()
    {
        base.Start();

        animator = GetComponent<Animator>();
        enemyHealth = GetComponent<Damageable>();

        ResetWaypoints();
    }

    protected override void Update()
    {
        if (PlayerManager.Instance.GetPlayer() != null && PlayerManager.Instance.GetPlayer().GetComponent<Damageable>().health <= 0)
        {
            enemyHealth.health = enemyHealth.maxHealth;
        }

        targetDestination.z = transform.position.z;

        if (enemyHealth.health <= 0)
        {
            Instantiate(deathEffect, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }

        targetVelocityX = 0.0f;

        //if (controller.IsCeilingHit)
        //{
        //    velocity.y = 0.0f;
        //}

        if (!controller.IsGrounded && !isGravityFrozen)
        {
            velocity.y += gravity * Time.deltaTime;
        }

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.IsGrounded) ? accelerationTimeGround : accelerationTimeAir);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        controller.Move(velocity * Time.deltaTime);
    }

    private void ResetWaypoints()
    {
        worldWaypoints.vectorList.Clear();

        foreach (Vector3 localWaypointPos in localWaypoints)
        {
            worldWaypoints.vectorList.Add(localWaypointPos + transform.position);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        float size = 0.3f;

        for (int i = 0; i < localWaypoints.Count; ++i)
        {
            Vector3 waypointWorldPos = (Application.isPlaying) ? worldWaypoints.vectorList[i] : localWaypoints[i] + transform.position;
            Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
            Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
        }
    }
}
