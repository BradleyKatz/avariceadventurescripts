﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Handles the attacking logic behind the Scarab enemy
/// </summary>
public class ScarabLogic : EnemyLogic
{
    [Header("Scarab Properties")]
    [Range(1, 5)]
    public int chargeDamage = 1;
    public float chargeSpeed = 25f;
    [Tooltip("How far past the player will the Scarab charge")]
    public float overshootDistance = 8f;
    public Color attackColor;

    private Damager damager;

    [HideInInspector] public bool bCanDamage = false;
    public bool canDamage { get { return bCanDamage; } set { bCanDamage = value; } }

    protected override void Start()
    {
        base.Start();

        moveSpeed = chargeSpeed;
        damager = GetComponentInChildren<Damager>();
        damager.damage = chargeDamage;
        targetDestination = transform.position;
    }

    protected override void Update()
    {
        base.Update();

        damager.isDamageCollisionEnabled = bCanDamage;

        if (bAttacking)
        {
            Vector3 dir = targetDestination - transform.position;
            float moveDir = Mathf.Sign(dir.x);

            targetDestination = new Vector3(targetDestination.x, transform.position.y, transform.position.z);
            targetDestination.x = targetDestination.x + (overshootDistance * moveDir);

            bAttacking = false;
        }

        animator.SetFloat("Speed", Mathf.Abs(velocity.x));
    }
}
