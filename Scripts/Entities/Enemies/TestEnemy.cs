﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 */

public class TestEnemy : AvariceEntity
{
    protected override void Update()
    {
        base.Update();
        targetVelocityX = moveAxis.x * moveSpeed;

        if (controller.IsCeilingHit)
        {
            velocity.y = 0.0f;
        }

        if (!controller.IsGrounded && !isMovementFrozen)
        {
            velocity.y += gravity * Time.deltaTime;
        }

        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.IsGrounded) ? accelerationTimeGround : accelerationTimeAir);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        controller.Move(velocity);
    }
}
