﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Handles the movement, duration and damage of the projectile
/// </summary>
public class ProjectileLogic : AvariceEntity
{
    [Header("Projectile Properties")]
    public bool useTimer;
    [Tooltip("Duration is used only if you want to have a timer on the object")]
    public float duration;
    [HideInInspector]public int damage;
    public bool destroyOnContact;

    [Header("Sound Properties")]
    public Sound onSpawnSound;
    public Sound onContactSound;

    [HideInInspector] public Vector3 targetDestination;
    private float timer;
    private Damager damager;

    private Animator projectileAnimator = null;
    private AnimationListener projectileAnimationListener = null;

    protected override void Start()
    {
        base.Start();

        projectileAnimator = GetComponentInChildren<Animator>();
        projectileAnimationListener = GetComponentInChildren<AnimationListener>();

        damager = GetComponent<Damager>();
        damager.isDamageCollisionEnabled = true;
        damager.damage = damage;
        timer = duration;

        if (onSpawnSound.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(onSpawnSound);
            AudioManager.Instance.PlaySoundEffect(onSpawnSound.name);
        }

        if (onContactSound.audioEvent != null)
        {
            AudioManager.Instance.RegisterSFX(onContactSound);
        }
    }

    protected override void Update()
    {
        base.Update();

        timer -= Time.deltaTime;
        if(timer <= 0 && useTimer)
        {
            Destroy(this.gameObject);
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        velocity.z = 0;
        controller.Move(velocity * Time.deltaTime);
    }

    protected override void TriggerEnter(Collider2D other)
    {
        if (projectileAnimator && projectileAnimationListener && destroyOnContact)
        {
            velocity = Vector3.zero;
            projectileAnimator.SetTrigger("Death");
            projectileAnimationListener.AddAnimationCompletedListener(Animator.StringToHash("Shockwave Death"), DestroyProj);
        }
        else
        {
            if (destroyOnContact)
            {
                DestroyProj();
            }
        }
    }

    public void DestroyProj(int shortHashName = 0)
    {
        if (projectileAnimationListener)
        {
            projectileAnimationListener.RemoveAnimationCompletedListener(Animator.StringToHash("Shockwave Death"), DestroyProj);
        }

        AudioManager.Instance.PlaySoundEffect(onContactSound.name);
        Destroy(this.gameObject);
    }
}
