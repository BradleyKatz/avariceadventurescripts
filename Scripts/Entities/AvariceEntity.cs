﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Bradley Katz
 * Modified by:
 * 
 * An AvariceEntity, simply put, is any entity in our game that requires the use of our character controller. This base class eases the implementation of our game's entities by providing a strong base containing movement smoothing, knockback calculations, and character model flipping.
 */

[RequireComponent(typeof(AvariceCC))]
public class AvariceEntity : AvariceObject
{
    [Header("Movement Properties")]
    public float gravity = 0.0f;
    public float moveSpeed = 6.0f;
    [Range(0, 1)]
    public float accelerationTimeGround = 0.1f;
    [Range(0, 1)]
    public float accelerationTimeAir = 0.2f;

    [HideInInspector] public bool isFallingThroughPlatform = false;
    [HideInInspector] public bool isMovementFrozen = false;
    [HideInInspector] public bool isGravityFrozen = false;

    /*[HideInInspector]*/
    public Vector3 velocity;
    [HideInInspector] public float velocityXSmoothing;
    [HideInInspector] public float velocityYSmoothing;
    [HideInInspector] public AvariceCC controller;
    [HideInInspector] public Vector2 moveAxis;
    [HideInInspector] public float targetVelocityX;
    [HideInInspector] public Vector3 rightRotation = new Vector3(0.0f, 0.0f, 0.0f);
    [HideInInspector] public Vector3 leftRotation = new Vector3(0.0f, 180.0f, 0.0f);
    [HideInInspector] public BlendShapeReference blendshapeReference = null;

    #region Knockback Functions
    public void ApplyKnockbackForce(Vector3 otherEntityPosition, float knockbackStrength)
    {
        Vector3 knockbackDirection = new Vector3(GetHorizontalFacingDirection() * -1.0f, Mathf.Sign(transform.position.y - otherEntityPosition.y));

        velocity = new Vector3(knockbackDirection.x * knockbackStrength, knockbackDirection.y * 4.0f);
        velocity.y *= (knockbackDirection.y == 1.0f) ? 2.0f : 1.0f;
        controller.IsGrounded = false;
    }

    // Affords full control over the knockback direction, useful for abilities such as Propel
    public void ApplyKnockbackForce(Vector2 knockbackDirection, float knockbackStrength)
    {
        
        velocity = new Vector3(knockbackDirection.x * knockbackStrength, knockbackDirection.y * knockbackStrength);
        controller.IsGrounded = false;
    }
    #endregion

    public float GetHorizontalFacingDirection()
    {
        return Mathf.Sign(transform.right.x);
    }

    protected override void Start()
    {
        base.Start();
        controller = GetComponent<AvariceCC>();
        blendshapeReference = GetComponentInChildren<BlendShapeReference>();
    }

    protected override void Update()
    {
        base.Update();

        if (velocity.x != 0.0f)
        {
            transform.localRotation = (Mathf.Sign(velocity.x) == -1.0f) ? Quaternion.Euler(leftRotation) : Quaternion.Euler(rightRotation);
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }
}
