﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Closes a door and then will open once the enemy waves have been defeated
/// </summary>
[RequireComponent(typeof(Animator))]
public class DoorTrap : AvariceObject
{
    public string audioClipName = "BattleMusic";

    [SerializeField] private GameObject door;
    [SerializeField] private GameObject spawner;

    private Animator anim;
    private bool doorActive;
    private bool trapTriggered;

    protected override void Start()
    {
        base.Start();
        anim = GetComponentInChildren<Animator>();
    }

    protected override void TriggerExit(Collider2D other)
    {
        base.TriggerExit(other);

        if (!trapTriggered)
        {
            AudioManager.Instance.PlaySong(audioClipName);
            trapTriggered = true;
            //door.SetActive(true);
            if (anim != null)
            {
                anim.SetTrigger("Close");
            }
            spawner.SetActive(true);
            doorActive = true;
        }
    }

    protected override void Update()
    {
        base.Update();

        if (doorActive)
        {
            if (!spawner.activeInHierarchy)
            {
                if (anim != null)
                {
                    anim.SetTrigger("Open");
                }
                //door.SetActive(false);
                doorActive = false;
                AudioManager.Instance.StopSong(audioClipName);
            }
        }
    }

    public void ResetEncounter()
    {
        trapTriggered = false;
        doorActive = false;
        if (anim != null)
        {
            anim.SetTrigger("Open");
        }
    }
}
