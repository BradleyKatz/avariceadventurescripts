﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Spawns an enemy and checks whether it is alive or not
/// Once it has been defeated it will spawn a new one after a set amount of time
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    [Tooltip("Enemy you wish to spawn")]
    public GameObject enemy;
    public float spawnDelay;

    private GameObject enemyRef;
    private float timer;

    private void Start()
    {
        enemyRef = Instantiate(enemy, transform.position, transform.rotation, this.transform);
        SpawnedGameObjectManager.Instance.AddObject(enemyRef);
        timer = 0;
    }

    private void Update()
    {
        if (enemyRef == null)
        {
            timer += Time.deltaTime;

            if (timer >= spawnDelay)
            {
                enemyRef = Instantiate(enemy, transform.position, transform.rotation, this.transform);
                timer = 0;
            }
        }
    }
}
