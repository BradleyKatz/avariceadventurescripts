﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This simple script destroys the gameobject
/// it's attached to when the player attacks it.
/// </summary>

public class Obstruction : Damageable
{
    protected override void OnDeath()
    {
        Destroy(this.gameObject);
    }
}