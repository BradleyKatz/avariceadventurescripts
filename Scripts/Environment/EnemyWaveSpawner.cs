﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Caleb Greer
 * Modified by:
 */

/// <summary>
/// Spawns waves of enemies which can be set to be randomly spawned or spawned in order
/// Waves are fully customizable and can have a set delay between the waves and a set delay between the spawning of enemies
/// Draws waypoints based off the colours set to indicate which enemy will spawn at the set points
/// </summary>
public class EnemyWaveSpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemySpawnParticles;
    [SerializeField] private GameObject scarab;
    [SerializeField] private GameObject brawler;
    [SerializeField] private GameObject wizard;

    [Tooltip("If checked it will randomly spawn the enemies. If not it will only spawn them at the desired spot or repeat through a list of spawn points")]
    public bool randomSpawn = false;
    [Range(0, 2)]
    public float spawnDelay = 0.8f;
    [Range(0, 5)]
    public float waveDelay = 1.0f;

    public EnemyWave[] waves;

    [Header("Editor Properties")]
    public bool drawGizmos;
    public bool seeAllSpawnPoints;
    public int waveNumber;
    public Color scarabSpawnColor;
    public Color brawlerSpawnColor;
    public Color wizardSpawnColor;

    private int currentWave = 0;
    private bool waveSpawned = false;
    private List<GameObject> enemies = new List<GameObject>();
    private float timer = 0;

    // Start is called before the first frame update
    void OnEnable()
    {
        currentWave = 0;
        timer = 0;

        if (randomSpawn)
        {
            StartCoroutine(randomlySpawnEnemies(waves[currentWave]));
        }
        else
        {
            StartCoroutine(spawnEnemies(waves[currentWave]));
        }
    }

    // Update is called once per frame
    void Update()
    {
        WaveUpdate();

        if (PlayerManager.Instance.GetPlayer().GetComponent<Damageable>().health <= 0)
        {
            ResetEncounter();
        }
    }

    public void ResetEncounter()
    {
        foreach (GameObject go in enemies.ToArray())
        {
            Destroy(go);
        }

        GetComponentInParent<DoorTrap>().ResetEncounter();
        currentWave = waves.Length;
    }

    void WaveUpdate()
    {
        if (waveSpawned)
        {
            foreach (GameObject go in enemies.ToArray())
            {
                if (go == null)
                {
                    enemies.Remove(go);
                }
            }

            if (enemies == null || enemies.Count == 0)
            {
                if (timer < waveDelay)
                {
                    timer += Time.deltaTime;
                }
                else
                {
                    waveSpawned = false;
                    currentWave++;
                    if (currentWave < waves.Length)
                    {


                        if (randomSpawn)
                        {
                            StartCoroutine(randomlySpawnEnemies(waves[currentWave]));
                            timer = 0;
                        }
                        else
                        {
                            StartCoroutine(spawnEnemies(waves[currentWave]));
                            timer = 0;
                        }
                    }
                }
            }
        }

        if (currentWave >= waves.Length)
        {
            this.gameObject.SetActive(false);
        }
    }

    int GetRandomSpawnPoint(List<Vector3> _vectors)
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        int i = Random.Range(0, _vectors.Count);

        return i;
    }

    IEnumerator randomlySpawnEnemies(EnemyWave _wave)
    {
        int totalEnemies = _wave.numberOfBrawlers + _wave.numberOfScarabs + _wave.numberOfWizards;
        int enemiesSpawned = 0;

        int scarabs = 0;
        int brawlers = 0;
        int wizards = 0;

        while (enemiesSpawned != totalEnemies)
        {
            if (scarabs < _wave.numberOfScarabs)
            {
                enemies.Add(Instantiate(scarab, _wave.scarabSpawnPoints[GetRandomSpawnPoint(_wave.scarabSpawnPoints)] + transform.position, transform.rotation));
                scarabs++;

                yield return new WaitForSeconds(spawnDelay);
            }


            if (brawlers < _wave.numberOfBrawlers)
            {
                enemies.Add(Instantiate(brawler, _wave.brawlerSpawnPoints[GetRandomSpawnPoint(_wave.brawlerSpawnPoints)] + transform.position, transform.rotation));
                brawlers++;

                yield return new WaitForSeconds(spawnDelay);
            }


            if (wizards < _wave.numberOfWizards)
            {
                enemies.Add(Instantiate(wizard, _wave.wizardSpawnPoints[GetRandomSpawnPoint(_wave.wizardSpawnPoints)] + transform.position, transform.rotation));
                wizards++;

                yield return new WaitForSeconds(spawnDelay);
            }

            enemiesSpawned = scarabs + brawlers + wizards;
        }

        waveSpawned = true;
    }

    IEnumerator spawnEnemies(EnemyWave _wave)
    {
        int totalEnemies = _wave.numberOfBrawlers + _wave.numberOfScarabs + _wave.numberOfWizards;
        int enemiesSpawned = 0;

        int scarabs = 0;
        int brawlers = 0;
        int wizards = 0;

        int scarabCounter = 0;
        int brawlerCounter = 0;
        int wizardCounter = 0;

        waveSpawned = true;

        while (enemiesSpawned != totalEnemies)
        {
            if (scarabs < _wave.numberOfScarabs)
            {
                GameObject spawnParticles = Instantiate(enemySpawnParticles, _wave.scarabSpawnPoints[scarabCounter] + transform.position, transform.rotation);
                ParticleSystem spawnPS = spawnParticles.GetComponentInChildren<ParticleSystem>();

                StartCoroutine(WaitForSpawnParticleCompletion(spawnPS, scarab, _wave.scarabSpawnPoints[scarabCounter] + transform.position, transform.rotation));
                scarabs++;

                if (scarabCounter + 1 < _wave.scarabSpawnPoints.Count)
                {
                    scarabCounter++;
                }
                else
                {
                    scarabCounter = 0;
                }

                yield return new WaitForSeconds(spawnDelay);
            }


            if (brawlers < _wave.numberOfBrawlers)
            {
                GameObject spawnParticles = Instantiate(enemySpawnParticles, _wave.brawlerSpawnPoints[brawlerCounter] + transform.position, transform.rotation);
                ParticleSystem spawnPS = spawnParticles.GetComponentInChildren<ParticleSystem>();

                StartCoroutine(WaitForSpawnParticleCompletion(spawnPS, brawler, _wave.brawlerSpawnPoints[brawlerCounter] + transform.position, transform.rotation));
                brawlers++;

                if (brawlerCounter + 1 < _wave.brawlerSpawnPoints.Count)
                {
                    brawlerCounter++;
                }
                else
                {
                    brawlerCounter = 0;
                }

                yield return new WaitForSeconds(spawnDelay);
            }


            if (wizards < _wave.numberOfWizards)
            {
                GameObject spawnParticles = Instantiate(enemySpawnParticles, _wave.wizardSpawnPoints[wizardCounter] + transform.position, transform.rotation);
                ParticleSystem spawnPS = spawnParticles.GetComponentInChildren<ParticleSystem>();

                StartCoroutine(WaitForSpawnParticleCompletion(spawnPS, wizard, _wave.wizardSpawnPoints[wizardCounter] + transform.position, transform.rotation));
                wizards++;

                if (wizardCounter + 1 < _wave.wizardSpawnPoints.Count)
                {
                    wizardCounter++;
                }
                else
                {
                    wizardCounter = 0;
                }

                yield return new WaitForSeconds(spawnDelay);
            }

            enemiesSpawned = scarabs + brawlers + wizards;
        }

    }

    private IEnumerator WaitForSpawnParticleCompletion(ParticleSystem spawnPS, GameObject enemyPrefab, Vector3 spawnPosition, Quaternion spawnRotation)
    {
        GameObject enemy = Instantiate(enemyPrefab, spawnPosition, spawnRotation);
        enemies.Add(enemy);
        enemy.SetActive(false);

        while (spawnPS.isPlaying)
        {
            yield return new WaitForEndOfFrame();
        }

        enemy.SetActive(true);
    }

    private void OnValidate()
    {
        if (waveNumber > waves.Length - 1)
        {
            waveNumber = waves.Length - 1;
        }
        else if (waveNumber < 0)
        {
            waveNumber = 0;
        }
        scarabSpawnColor.a = 1;
        brawlerSpawnColor.a = 1;
        wizardSpawnColor.a = 1;
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            float size = 0.3f;

            if (seeAllSpawnPoints)
            {
                foreach (EnemyWave wave in waves)
                {
                    Gizmos.color = scarabSpawnColor;
                    foreach (Vector3 spawn in wave.scarabSpawnPoints)
                    {
                        Vector3 waypointWorldPos = spawn + transform.position;
                        Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                        Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                    }

                    Gizmos.color = brawlerSpawnColor;
                    foreach (Vector3 spawn in wave.brawlerSpawnPoints)
                    {
                        Vector3 waypointWorldPos = spawn + transform.position;
                        Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                        Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                    }

                    Gizmos.color = wizardSpawnColor;
                    foreach (Vector3 spawn in wave.wizardSpawnPoints)
                    {
                        Vector3 waypointWorldPos = spawn + transform.position;
                        Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                        Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                    }
                }
            }
            else
            {
                Gizmos.color = scarabSpawnColor;
                for (int i = 0; i < waves[waveNumber].scarabSpawnPoints.Count; ++i)
                {
                    Vector3 waypointWorldPos = waves[waveNumber].scarabSpawnPoints[i] + transform.position;
                    Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                    Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                }

                Gizmos.color = brawlerSpawnColor;
                for (int i = 0; i < waves[waveNumber].brawlerSpawnPoints.Count; ++i)
                {
                    Vector3 waypointWorldPos = waves[waveNumber].brawlerSpawnPoints[i] + transform.position;
                    Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                    Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                }

                Gizmos.color = wizardSpawnColor;
                for (int i = 0; i < waves[waveNumber].wizardSpawnPoints.Count; ++i)
                {
                    Vector3 waypointWorldPos = waves[waveNumber].wizardSpawnPoints[i] + transform.position;
                    Gizmos.DrawLine(waypointWorldPos - Vector3.up * size, waypointWorldPos + Vector3.up * size);
                    Gizmos.DrawLine(waypointWorldPos - Vector3.left * size, waypointWorldPos + Vector3.left * size);
                }
            }
        }
    }
}
