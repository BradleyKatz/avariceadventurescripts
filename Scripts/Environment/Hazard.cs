﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script allow for a gameobject to
/// damage the player when they walk on it.
/// </summary>

public class Hazard : Damager
{
    protected override void Start()
    {
        base.Start();
        isDamageCollisionEnabled = true;
    }
}