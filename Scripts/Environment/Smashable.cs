﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script instantiates a drop (ex. coin) after
/// the player attacks it.
/// </summary>

public class Smashable : Damageable
{
    public GameObject drop;

    protected override void OnDeath()
    {
        //Instantiate(drop, transform.position + new Vector3(-2.0f, 1.0f, 0.0f), transform.rotation);
        Destroy(this.gameObject);
    }
}