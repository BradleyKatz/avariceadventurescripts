﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Original Author: Kevin Newbury
 * Modified by: Brad Wuz Here
 */

/// <summary>
/// This script handles the fill
/// of each Heart in the HUD.
/// </summary>

public class HeartLogic : MonoBehaviour
{
    [HideInInspector]
    public bool isFilled;
    public GameObject fillImage;
    public float fillSpeed;
    private Image image;

    private void Start()
    {
        isFilled = false;
        image = fillImage.GetComponent<Image>();
    }

    private void Update()
    {
        if (isFilled)
        {
            image.fillAmount += fillSpeed * Time.deltaTime;
        }
        else
        {
            image.fillAmount -= fillSpeed * Time.deltaTime;
        }
    }
}