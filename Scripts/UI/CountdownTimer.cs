﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    public float timerSeconds;
    public Text timerText;

    public void Update()
    {
        CountdownText();
    }

    void CountdownText()
    {
        timerSeconds -= Time.deltaTime;
        int mins = (int)(timerSeconds / 60);
        int seconds = (int)(timerSeconds - (mins * 60));

        timerText.text = mins + ":" + seconds;
    }
}
