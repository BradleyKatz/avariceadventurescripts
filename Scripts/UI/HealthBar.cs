﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// Comments are upon you, whether you'd
/// risk it or not.
/// </summary>

public class HealthBar : MonoBehaviour
{
    [HideInInspector]
    public float fillAmount;
    private Image barImage;

    private void Start()
    {
        barImage = this.GetComponent<Image>();
        
    }

    private void Update()
    {
        barImage.fillAmount = fillAmount;
    }
}