﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeUILogic : MonoBehaviour
{
    public void ChangeMusicVolume(float _volume)
    {
        AudioManager.Instance.SetMusicVolume(_volume);
    }

    public void ChangeSFXVolume(float _volume)
    {
        AudioManager.Instance.SetSFXVolume(_volume);
    }
}
