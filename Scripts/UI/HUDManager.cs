﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script handles all aspects of the HUD including:
/// Health, capture, map, dialog, currency, and EndGame menu.
/// 
/// This script does not include the management of menus.
/// 
/// As the number of public and private variables is quite large,
/// they are organized by category for easy of understanding.
/// 
/// Then there are methods that manage each aspect of the HUD
/// which are all called in Update.
/// </summary>

public class HUDManager : Singleton<HUDManager>
{
    //Health
    [Header("Health")]
    public List<GameObject> hearts;

    private int maxHealth;
    private int currentHealth;

    //Capture Icon
    [Header("Capture")]
    public GameObject captureIconObject;
    public GameObject captureIconBorder;
    public Sprite emptyIcon;
    public Sprite scarabIcon;
    public Sprite mummyIcon;
    public Sprite wizardIcon;
    public Text captureChargesText;

    private Damageable playerDam;
    private eEnemyType enemyType;
    private Image captureImage;
    private Image captureCharges;
    private int uses = 0;

    //Dialog
    [Header("Dialog")]
    [Range(3.0f, 5.0f)]
    public float timer;
    public Text dialogText;
    public Text dialogCharacter;
    public Image dialogImage;
    public Image dialogBackground;

    private float timerReference;
    private bool isActive;
    private bool init;

    //Currency
    [Header("Currency")]
    public Text currencyText;
    public Image currencyIcon;

    private int currencyValue;

    //Letters
    [Header("Letters")]
    public Image letterAOne;
    public Image letterV;
    public Image letterATwo;
    public Image letterR;
    public Image letterI;
    public Image letterC;
    public Image letterE;

    public Image letterAOneGrey;
    public Image letterVGrey;
    public Image letterATwoGrey;
    public Image letterRGrey;
    public Image letterIGrey;
    public Image letterCGrey;
    public Image letterEGrey;

    private Animator letterAnimator;


    //EndGame
    [Header("End Game")]
    public GameObject endGameMenu;

    //Player
    private Player player;

    private void Start()
    {
        //Dialog
        isActive = false;
        timerReference = timer;

        //Currency
        currencyValue = 0;
    }

    private void Update()
    {
        if (player == null)
        {
            GameObject playerObj = PlayerManager.Instance.GetPlayer();
            if (playerObj)
            {
                player = playerObj.GetComponent<Player>();
            }
        }
        else if (!playerDam && player && player.playerHealth)
        {
            playerDam = player.playerHealth;
            maxHealth = playerDam.maxHealth;
            currentHealth = playerDam.health;
            uses = player.currentCapture.uses;
            enemyType = player.currentCapture.captureEnemyType;
            captureImage = captureIconObject.GetComponent<Image>();

            for (int i = 0; i < hearts.Count; i++)
            {
                if (i > maxHealth - 1)
                {
                    hearts[i].SetActive(false);
                }
            }
        }
        else
        {

            ManageHealth();
            ManageCaptureCharges();
            ManageCaptureIcon();
            ManageDialog();
            ManageCurrency();
        }
    }

    private void ManageHealth()
    {
        if (!player)
        {
            currentHealth = 0;
        }
        else
        {
            currentHealth = playerDam.health;
        }

        for (int i = 0; i < hearts.Count; i++)
        {
            if (i < currentHealth)
            {
                hearts[i].GetComponent<HeartLogic>().isFilled = true;
            }
            else
            {
                hearts[i].GetComponent<HeartLogic>().isFilled = false;
            }
        }
    }

    private void ManageCaptureCharges()
    {
        if (player)
        {
            if (player.currentCapture != null)
            {
                uses = player.currentCapture.uses;
            }
            else
            {
                uses = 0;
            }

            switch (uses)
            {
                case 1:
                    captureIconObject.SetActive(true);
                    captureIconBorder.SetActive(true);
                    captureChargesText.gameObject.SetActive(true);
                    captureChargesText.text = "1";
                    break;
                case 2:
                    captureIconObject.SetActive(true);
                    captureIconBorder.SetActive(true);
                    captureChargesText.gameObject.SetActive(true);
                    captureChargesText.text = "2";
                    break;
                case 3:
                    captureIconObject.SetActive(true);
                    captureIconBorder.SetActive(true);
                    captureChargesText.gameObject.SetActive(true);
                    captureChargesText.text = "3";
                    break;
                default:
                    captureIconObject.SetActive(false);
                    captureIconBorder.SetActive(false);
                    captureChargesText.gameObject.SetActive(false);
                    captureChargesText.text = "0";
                    break;
            }
        }
    }

    private void ManageCaptureIcon()
    {
        if (player.currentCapture == null)
        {
            enemyType = eEnemyType.Empty;
        }
        else
        {
            enemyType = player.currentCapture.captureEnemyType;
        }

        switch (enemyType)
        {
            case eEnemyType.Scarab:
                captureImage.sprite = scarabIcon;
                break;
            case eEnemyType.Mummy:
                captureImage.sprite = mummyIcon;
                break;
            case eEnemyType.Wizard:
                captureImage.sprite = wizardIcon;
                break;
            default:
                captureImage.sprite = emptyIcon;
                break;
        }
    }

    private void ManageDialog()
    {
        if (!init)
        {
            dialogImage.canvasRenderer.SetAlpha(0.0f);
            dialogText.canvasRenderer.SetAlpha(0.0f);
            dialogCharacter.canvasRenderer.SetAlpha(0.0f);
            dialogBackground.canvasRenderer.SetAlpha(0.0f);
            init = true;
        }

        if (isActive)
        {
            timer -= Time.deltaTime;

            if (timer <= 0.0f)
            {
                FadeDialog(0.0f, 0.5f);

                if (dialogImage.canvasRenderer.GetAlpha() == 0.0f)
                {
                    isActive = false;
                    timer = timerReference;
                }
            }
            else
            {
                FadeDialog(1.0f, 1.0f);
            }
        }
    }

    private void ManageCurrency()
    {
        currencyText.text = currencyValue.ToString();

        if (currencyValue < 0)
        {
            currencyValue = 0;
        }
    }

    public void ActivateLetter(string letterName)
    {
        switch (letterName)
        {
            case "A1":
                letterAOne.gameObject.SetActive(true);
                letterAOneGrey.gameObject.SetActive(false);
                WiggleLetter(letterAOne);
                break;
            case "V":
                letterV.gameObject.SetActive(true);
                letterVGrey.gameObject.SetActive(false);
                WiggleLetter(letterV);
                break;
            case "A2":
                letterATwo.gameObject.SetActive(true);
                letterATwoGrey.gameObject.SetActive(false);
                WiggleLetter(letterATwo);
                break;
            case "R":
                letterR.gameObject.SetActive(true);
                letterRGrey.gameObject.SetActive(false);
                WiggleLetter(letterR);
                break;
            case "I":
                letterI.gameObject.SetActive(true);
                letterIGrey.gameObject.SetActive(false);
                WiggleLetter(letterI);
                break;
            case "C":
                letterC.gameObject.SetActive(true);
                letterCGrey.gameObject.SetActive(false);
                WiggleLetter(letterC);
                break;
            case "E":
                letterE.gameObject.SetActive(true);
                letterEGrey.gameObject.SetActive(false);
                WiggleLetter(letterE);
                break;
            default:
                Debug.Log("Something went wrong in ActivateLetter().");
                break;
        }
    }

    public void ResetLetters()
    {
        letterAOne.gameObject.SetActive(false);
        letterAOneGrey.gameObject.SetActive(true);
        letterV.gameObject.SetActive(false);
        letterVGrey.gameObject.SetActive(true);
        letterATwo.gameObject.SetActive(false);
        letterATwoGrey.gameObject.SetActive(true);
        letterR.gameObject.SetActive(false);
        letterRGrey.gameObject.SetActive(true);
        letterI.gameObject.SetActive(false);
        letterIGrey.gameObject.SetActive(true);
        letterC.gameObject.SetActive(false);
        letterCGrey.gameObject.SetActive(true);
        letterE.gameObject.SetActive(false);
        letterEGrey.gameObject.SetActive(true);
    }

    public void WiggleLetter(Image letter)
    {
        letter.GetComponent<Animator>().SetBool("LetterCollected", true);
    }

    public void WiggleCoin()
    {
        currencyIcon.GetComponent<Animator>().SetBool("CoinCollected", true);
    }

    public void FadeDialog(float alpha, float timer)
    {
        dialogImage.CrossFadeAlpha(alpha, timer, false);
        dialogText.CrossFadeAlpha(alpha, timer, false);
        dialogBackground.CrossFadeAlpha(alpha, timer, false);
        dialogCharacter.CrossFadeAlpha(alpha, timer, false);
    }

    public void DisplayDialog(string message, string character, Sprite icon)
    {
        dialogText.text = message;
        dialogImage.sprite = icon;
        dialogCharacter.text = character;
        isActive = true;
    }
}