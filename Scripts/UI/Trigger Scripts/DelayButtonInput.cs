﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelayButtonInput : MonoBehaviour
{
    public float delay = 0.5f;
    private Button mainMenuButton;
    private float delayRef;
    private bool isDelayed = false;
    private void Start()
    {
        mainMenuButton = GetComponent<Button>();
        mainMenuButton.enabled = false;
        delayRef = delay;
    }
    private void Update()
    {
        if(isDelayed)
        {
            Debug.Log("Delay started!");
            delay -= Time.deltaTime;
            if (delay <= 0.0f)
            {
                mainMenuButton.enabled = true;
                delay = delayRef;
                isDelayed = false;
                Debug.Log("Delay finished!");
            }
        }
    }

    public void StartDelay()
    {
        isDelayed = true;
    }
}