﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script brings up the EndGame
/// menu once the player steps on the 
/// trigger (hopefully at the end of the game).
/// </summary>

public class EndGameTrigger : AvariceObject
{
    public MenuClassifier endGameMenu;

    protected override void TriggerEnter(Collider2D other)
    {
        if (other.GetComponent<Player>() != null)
        {
            MenuManager.Instance.showMenu(endGameMenu);
            MenuManager.Instance.getMenu<Menu>(endGameMenu).GetComponentInChildren<DelayButtonInput>().StartDelay();
        }
    }
}