﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Original Author: Kevin Newbury
 * Modified by:
 */

/// <summary>
/// This script display a dialog box
/// in the HUD when the player steps over
/// the trigger.
/// </summary>

public class DialogTrigger : AvariceObject
{
    //public string message;
    //public string character;
    //public Sprite sprite;
    //public bool showOnce;

    //private HUDManager manager;
    //private bool isShowing;

    //protected override void Start()
    //{
    //    base.Start();
    //    manager = HUDManager.Instance;
    //    isShowing = true;
    //}

    //protected override void TriggerEnter(Collider2D other)
    //{
    //    if(isShowing)
    //    {
    //        if (other.GetComponent<Player>() != null)
    //        {
    //            manager.DisplayDialog(message, character, sprite);

    //            if(showOnce)
    //            {
    //                isShowing = false;
    //            }
    //        }
    //    }
    //}
}