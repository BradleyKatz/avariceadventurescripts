﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageCrossFade : MonoBehaviour
{
    public float fadeDuration = 2.0f;

    private Image image;
    private bool bFade;

    // Start is called before the first frame update
    void Start()
    {
        bFade = false;
        image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        if (image != null)
        {
            bFade = false;
            image.CrossFadeAlpha(0, 0, false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!bFade)
        {
            image.CrossFadeAlpha(1, fadeDuration, false);
            bFade = true;
        }
    }

    private void OnDisable()
    {
        image.CrossFadeAlpha(0, 0, false);
    }
}
