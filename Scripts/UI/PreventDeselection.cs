﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
 * Original Author: Caleb Greer
 * Modified by: Kevin Newbury
 */

/// <summary>
/// Prevents the deselection of a button within the menus
/// 
/// Kevin: Added selectors for the menu for flavour.
/// </summary>
public class PreventDeselection : MonoBehaviour
{
    EventSystem eventSystem;

    public GameObject defaultObject;
    GameObject selectedObject;
    public GameObject selectionBox;

    private void OnEnable()
    {
        selectedObject = GetComponentInChildren<Selectable>().gameObject;
    }

    private void Start()
    {
        eventSystem = EventSystem.current;
    }

    private void Update()
    {
        if (eventSystem.currentSelectedGameObject != null && eventSystem.currentSelectedGameObject != selectedObject)
        {
            selectedObject = eventSystem.currentSelectedGameObject;
        }
        else if (selectedObject != null && eventSystem.currentSelectedGameObject == null)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
        }

        if(selectionBox != null)
        {
            SelectorPosition();
        }
    }

    public void ResetSelectedObject()
    {
        defaultObject.GetComponent<Selectable>().Select();
    }

    public void SelectorPosition()
    {
        selectionBox.transform.position = new Vector2(selectionBox.transform.position.x, selectedObject.transform.position.y);
    }
}